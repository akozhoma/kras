#!/bin/bash
echo Running pre-commit hook..

# Path to REV file
path='./VERSION'

# Get the revision
rev=$(git rev-list HEAD --count)
# Increment revision
rev=$((rev+1))

echo $rev > $path

# Add to existing commit
git add $path

echo Finished running pre-commit hook.
