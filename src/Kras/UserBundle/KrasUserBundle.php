<?php

namespace Kras\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class KrasUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
