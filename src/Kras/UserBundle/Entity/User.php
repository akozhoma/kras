<?php

namespace Kras\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 * @UniqueEntity("coordinating")
 * @ORM\HasLifecycleCallbacks
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $firstname;

    /**
     * @ORM\ManyToOne(targetEntity="Kras\KrasBundle\Entity\SupportCenter", inversedBy="employees", cascade="persist")
     * @ORM\JoinColumn(name="supportcenter_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $supportcenter;

    /**
     * @ORM\OneToOne(targetEntity="Kras\KrasBundle\Entity\SupportCenter", mappedBy="coordinator")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $coordinating;

    /**
     * @ORM\OneToMany(targetEntity="Kras\KrasBundle\Entity\LogEntry", mappedBy="employee", cascade="persist", orphanRemoval=true)
     */
    private $logentries;

    /**
     * @ORM\OneToMany(targetEntity="Kras\KrasBundle\Entity\TimesheetTemplate", mappedBy="employee", orphanRemoval=true)
     */
    private $timesheettemplates;

    /**
     * @ORM\OneToMany(targetEntity="Kras\KrasBundle\Entity\Timesheet", mappedBy="employee", orphanRemoval=true)
     */
    private $timesheets;

    /**
     * @ORM\ManyToMany(targetEntity="Kras\KrasBundle\Entity\Activity", mappedBy="employees")
     */
    private $activities;

    /**
     * @ORM\Column(name="started_working", type="date")
     */
    private $startedWorking;

    /**
     * @ORM\Column(type="string")
     */
    private $label;

    /**
     * @ORM\Column(name="last_sudo", type="datetime", nullable=true)
     */
    private $lastSudo;

    /**
     * @ORM\ManyToMany(targetEntity="Kras\KrasBundle\Entity\AllowedLeave", mappedBy="employees", orphanRemoval=true)
     */
    private $allowedleaves;

    /**
     * @ORM\ManyToMany(targetEntity="Kras\KrasBundle\Entity\UserLeave", mappedBy="employees", orphanRemoval=true)
     */
    private $leaves;

    /**
     * @ORM\OneToMany(targetEntity="Kras\KrasBundle\Entity\Training", mappedBy="employee", orphanRemoval=true)
     */
    private $trainings;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="float", precision=2, nullable=true)
     */
    private $overtime;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setLabelValue()
    {
        $label = !empty($this->firstname) || !empty($this->lastname)
            ? (!empty($this->firstname)?$this->firstname. ' ':'').(!empty($this->lastname)?$this->lastname:''):$this->username;

        $this->label = $label;
    }

    public function __construct()
    {
        parent::__construct();
        $this->activities = new ArrayCollection();
        $this->logentries = new ArrayCollection();
        $this->timesheettemplates = new ArrayCollection();
        $this->timesheets = new ArrayCollection();
        $this->enabled = true;
        $this->startedWorking = new \DateTime();
        $this->allowedleaves = new ArrayCollection();
        $this->leaves = new ArrayCollection();
        $this->trainings = new ArrayCollection();
        $this->overtime = 0;
    }

    public function __toString()
    {
        return $this->label;
    }

    public function getLastName()
    {
        return $this->lastname;
    }

    public function setLastName($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    public function getFirstName()
    {
        return $this->firstname;
    }

    public function setFirstName($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    public function getSupportCenter()
    {
        return $this->supportcenter;
    }

    public function setSupportCenter($supportcenter)
    {
        $this->supportcenter = $supportcenter;
        if($this->coordinating && $this->coordinating !== $this->supportcenter) {
            $this->coordinating->setCoordinator(null);
            $this->coordinating = null;
        }
        return $this;
    }

    public function getCoordinating()
    {
        return $this->coordinating;
    }

    public function setCoordinating($coordinating)
    {
        $this->coordinating = $coordinating;
        return $this;
    }

    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    public function getActivities()
    {
        return $this->activities;
    }

    public function setActivities($activities)
    {
        $this->activities = $activities;
        return $this;
    }

    public function addActivity($activity)
    {
        if (!$this->activities->contains($activity)) {
            $this->acitivities[] = $activity;
        }
        return $this;
    }

    public function removeActivity($activity)
    {
        $this->activities->remove($activity);
        return $this;
    }

    public function getLogEntries()
    {
        return $this->logentries;
    }

    public function setLogEntries($logentries)
    {
        $this->logentries = $logentries;
        return $this;
    }

    public function addLogEntry($logentry)
    {
        if (!$this->logentries->contains($logentry)) {
            $this->logentries[] = $logentry;
        }
        return $this;
    }

    public function removeLogEntry($logentry)
    {
        $this->logentries->remove($logentry);
        return $this;
    }

    public function getTimesheetTemplates()
    {
        return $this->timesheettemplates;
    }

    public function setTimesheetTemplates($timesheettemplates)
    {
        $this->timesheettemplates = $timesheettemplates;
        return $this;
    }

    public function addTimesheetTemplate($timesheettemplate)
    {
        if (!$this->timesheettemplates->contains($timesheettemplate)) {
            $this->timesheettemplates[] = $timesheettemplate;
        }
        return $this;
    }

    public function removeTimesheetTemplate($timesheettemplate)
    {
        $this->timesheettemplates->remove($timesheettemplate);
        return $this;
    }

    public function hasTimesheetTemplate($timesheettemplate)
    {
        return $this->timesheettemplates->contains($timesheettemplate);
    }

    public function getTimesheets()
    {
        return $this->timesheets;
    }

    public function setTimesheets($timesheets)
    {
        $this->timesheets = $timesheets;
        return $this;
    }

    public function addTimesheet($timesheet)
    {
        if (!$this->timesheets->contains($timesheet)) {
            $this->timesheets[] = $timesheet;
        }
        return $this;
    }

    public function removeTimesheet($timesheet)
    {
        $this->timesheets->remove($timesheet);
        return $this;
    }

    public function hasTimesheet($timesheet)
    {
        return $this->timesheets->contains($timesheet);
    }

    public function getStartedWorking()
    {
        return $this->startedWorking;
    }

    public function setStartedWorking($startedWorking)
    {
        $this->startedWorking = $startedWorking;
        return $this;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    public function getLastSudo()
    {
        return $this->lastSudo;
    }

    public function setLastSudo($lastSudo)
    {
        $this->lastSudo = $lastSudo;
        return $this;
    }

    public function hasSudoMode()
    {
        if (!$this->lastSudo) {
            return false;
        }

        $time = time();

        // The user is in sudo mode when the diff between $time and $lastSudo
        // is less than 30 minutes.
        return ($time - $this->lastSudo->getTimestamp()) < (30 * 60);
    }

    public function getAllowedLeaves()
    {
        return $this->allowedleaves;
    }

    public function setAllowedLeaves($allowedleaves)
    {
        $this->allowedleaves = $allowedleaves;
        return $this;
    }

    public function addAllowedLeave($allowedleave)
    {
        if (!$this->allowedleaves->contains($allowedleave)) {
            $this->allowedleaves[] = $allowedleave;
        }
        return $this;
    }

    public function removeAllowedLeave($allowedleave)
    {
        $this->allowedleaves->remove($allowedleave);
        return $this;
    }

    public function hasAllowedLeave($allowedleave)
    {
        return $this->allowedleaves->contains($allowedleave);
    }

    public function getLeaves()
    {
        return $this->leaves;
    }

    public function setLeaves($leaves)
    {
        $this->leaves = $leaves;
        return $this;
    }

    public function addLeave($leave)
    {
        if (!$this->leaves->contains($leave)) {
            $this->leaves[] = $leave;
        }
        return $this;
    }

    public function removeLeave($leave)
    {
        $this->leaves->remove($leave);
        return $this;
    }

    public function hasLeave($leave)
    {
        return $this->leaves->contains($leave);
    }

    public function getTrainings()
    {
        return $this->trainings;
    }

    public function setTrainings($trainings)
    {
        $this->trainings = $trainings;
        return $this;
    }

    public function addTraining($training)
    {
        if (!$this->trainings->contains($training)) {
            $this->trainings[] = $training;
        }
        return $this;
    }

    public function removeTraining($training)
    {
        $this->trainings->remove($training);
        return $this;
    }

    public function hasTraining($training)
    {
        return $this->trainings->contains($training);
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    public function getOvertime()
    {
        return $this->overtime;
    }

    public function setOvertime($overtime)
    {
        $this->overtime = $overtime;
        return $this;
    }
}
