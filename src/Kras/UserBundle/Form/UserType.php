<?php

namespace Kras\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Kras\UserBundle\Form\EventListener\PasswordRequirementSubscriber;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', null, array('label' => 'First name', 'required' => false))
            ->add('lastname', null, array('label' => 'Last name', 'required' => false))
            ->add('username', null, array('label' => 'Username'))
            ->add('gender', 'choice', array(
                    'choices' => array(
                        'GENDER_MALE'   => 'GENDER_MALE',
                        'GENDER_FEMALE' => 'GENDER_FEMALE'
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'required' => true,
                )
            )
            ->add('email', 'email', array('label' => 'Email address'))
            ->add('roles', 'choice', array(
                    'choices' => array(
                        'ROLE_INTERN'      => 'Intern',
                        'ROLE_EMPLOYEE'    => 'Employee',
                        'ROLE_COORDINATOR' => 'Coordinator',
                        'ROLE_DIRECTOR'    => 'Director',
                        'ROLE_HR_MANAGER'  => 'HR manager',
                        'ROLE_ADMIN'       => 'Administrator',
                        'ROLE_SUPER_ADMIN' => 'Super administrator',
                    ),
                    'multiple' => true,
                    'label' => 'Role',
                    'attr' => array(
                        'class' => 'chosen-select',
                    ),
                )
            )
            ->add('supportcenter', null, array(
                    'empty_value' => 'None',
                    'attr' => array(
                        'class' => 'chosen-select',
                    ),
                )
            )
            //->add('enabled', 'checkbox', array('required' => false, 'label' => 'Enable account?'))
            ->add('expiresat', 'date', array(
                    'label' => 'Expires at',
                    'widget' => 'single_text',
                    'attr' => array(
                        'class' => 'datepicker',
                    ),
                    'required' => false,
                )
            )
            ->add('startedWorking', 'date', array(
                    'widget' => 'single_text',
                    'attr' => array(
                        'class' => 'datepicker',
                    ),
                    'required' => true,
                )
            )
            ->addEventSubscriber(new PasswordRequirementSubscriber());
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Kras\UserBundle\Entity\User'
            )
        );
    }

    public function getName()
    {
        return 'kras_userbundle_usertype';
    }
}
