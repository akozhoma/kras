<?php

namespace Kras\KrasBundle\Twig;

class UtilExtension extends \Twig_Extension
{
    public function getTests()
    {
        return array(
            new \Twig_SimpleTest('boolean', array($this, 'booleanTest')),
        );
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('fileExists', array($this, 'fileExistsFunction')),
        );
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('defaultFile', array($this, 'defaultFileFilter')),
            new \Twig_SimpleFilter('formatTime', array($this, 'formatTimeFilter')),
        );
    }

    public function booleanTest($mixed)
    {
        return is_bool($mixed);
    }

    public function fileExistsFunction($path)
    {
        return file_exists($path);
    }

    public function defaultFileFilter($path, $defaultPath)
    {
        return file_exists($path) ? $path : $defaultPath;
    }

    function formatTimeFilter($seconds)
    {
        return sprintf("%02dh %02dm %02ds", floor($seconds / 3600), ($seconds / 60) % 60, $seconds % 60);
    }

    public function getName()
    {
        return 'util_extension';
    }
}