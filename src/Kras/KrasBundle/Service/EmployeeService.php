<?php
namespace Kras\KrasBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAware;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Kras\KrasBundle\Entity\Timesheet;
use Kras\KrasBundle\Entity\ExtendedTimeFragment;
use Kras\KrasBundle\Entity\TimesheetTemplate;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints\Null;

class EmployeeService extends ContainerAware
{
    private $em;
    private $context;
    private $request;
    private $session;

    public function __construct(EntityManager $em, SecurityContext $context, Session $session)
    {
        $this->em = $em;
        $this->context = $context;
        $this->session = $session;
    }

    private function getUser()
    {
        return $this->context->getToken()->getUser();
    }

    public function recalculateTimesheetWorkedHours($timesheet)
    {
        $employee = $timesheet->getEmployee();
        $extendedTimeFragments = $timesheet->getExtendedTimeFragments();

        $start_date = clone $timesheet->getStartDate();
        $end_date = clone $timesheet->getStartDate();
        $end_date->setTime(23, 59, 59);
        $end_date->modify('+6 days');

        $diff = 0;

        foreach ($extendedTimeFragments as $extendedTimeFragment) {
            $diff += $extendedTimeFragment->getEnd()->format('U') - $extendedTimeFragment->getStart()->format('U');
        }

        $dql = 'SELECT ld, l, lt FROM
            KrasKrasBundle:LeaveDate ld
            LEFT JOIN ld.leave l
            LEFT JOIN l.leavetype lt
            LEFT JOIN l.employees e
            WHERE (e.id = ?1 OR e IS NULL)
            AND (ld.date BETWEEN ?2 AND ?3)
            AND l.approved = TRUE
            ORDER BY l.startTime ASC';
        $query = $this->em->createQuery($dql);
        $leave_dates = $query
            ->setParameters(
                array(
                    1 => $employee,
                    2 => $start_date,
                    3 => $end_date,
                )
            )
            ->getResult();

        $overtime_used = 0;

        foreach ($leave_dates as $leave_date) {
            $leave = $leave_date->getLeave();
            $leave_type = $leave->getLeaveType();

            if ($leave_type->getPublicHoliday() || $leave_type->getSickness()) {
                if ($timesheet->getTimesheetTemplate()) {
                    foreach ($timesheet->getTimesheetTemplate()->getExtendedTimeFragments() as $fragment) {
                        if ($leave_date->getDate()->format('N') == $fragment->getStart()->format('N')) {
                            $diff += $fragment->getEnd()->format('U') - $fragment->getStart()->format('U');
                        }
                    }
                }
            } else {
                $diff += $leave->getEndTime()->format('U') - $leave->getStartTime()->format('U');

                if ($leave_type->getOvertime()) {
                    $overtime_used += ($leave->getEndTime()->format('U') - $leave->getStartTime()->format('U')) / 3600;
                }
            }
        }

        //die();

        $hours = round($diff / 3600, 2);
        $timesheet->setWorkedHours($hours);

        $overtime_hours = $timesheet->getWorkedHours() - $timesheet->getRequiredHours();
        //$overtime_hours -= $overtime_used;
        $overtime_hours = $overtime_hours < 0 ? 0 : $overtime_hours;

        $timesheet->setOvertimeHours($overtime_hours);
        $timesheet->setRemainingOvertimeHours($timesheet->getOvertimeHours() - $timesheet->getUsedOvertimeHours());

        $this->em->flush();
        $this->em->refresh($timesheet);
    }

    public function recalculateTimesheetsWorkedHours($timesheets)
    {
        foreach ($timesheets as $timesheet) {
            $this->recalculateTimesheetWorkedHours($timesheet);
        }
    }

    public function getTimesheetsByLeave($leave)
    {
        $timesheets = array();

       foreach ($leave->getLeaveDates() as $leaveDate) {
            $start_date = clone $leaveDate->getDate();
            $end_date = clone $leaveDate->getDate();

            $day = $start_date->format('w');
            $start_date->modify('-' . ($day == 0 ? 6 : $day - 1) . ' days');
            $end_date->modify('+' . ($day == 0 ? 0 : 6 - $day) . ' days');

            $start_date->setTime(00, 00, 00);
            $end_date->setTime(23, 59, 59);

            $employee_ids = array();

            if ($leave->getEmployees()->isEmpty()) {
                $employee_ids = $this->em->createQuery('SELECT u.id FROM KrasUserBundle:User u WHERE u.enabled = 1')->getArrayResult();
                $employee_ids = array_column($employee_ids, 'id');
            } else {
                $employee_ids = $leave->getEmployees()->map(function($v) { return $v->getId(); })->toArray();
            }

            $employee_ids[] = 0;

            $dql = 'SELECT t FROM KrasKrasBundle:Timesheet t
                    JOIN t.employee e
                    WHERE e.id IN (?1)
                    AND (t.startDate BETWEEN ?2 AND ?3 OR t.startDate = ?2 OR t.startDate = ?3)';
            $query = $this->em->createQuery($dql);
            $query->setParameters(array(
                1 => $employee_ids,
                2 => $start_date,
                3 => $end_date,
            ));

            $timesheets = array_merge($timesheets, $query->getResult());
        }

        return $timesheets;
    }

    public function recalculateTimesheetWorkedHoursByLeave($leave)
    {
        $this->recalculateTimesheetsWorkedHours($this->getTimesheetsByLeave($leave));
    }

    public function getActiveTimesheetTemplate($employee = null)
    {
        if (!$employee) {
            $employee = $this->getUser();
        }

        $dql = 'SELECT tt FROM KrasKrasBundle:TimesheetTemplate tt
            WHERE tt.employee = ?1
            AND tt.approved = TRUE
            AND tt.start < ?2
            AND (tt.end > ?2 OR tt.end IS NULL)
            ORDER BY tt.start DESC';
        $query = $this->em->createQuery($dql);
        $query->setParameters(
            array(
                1 => $employee->getId(),
                2 => new \DateTime(),
            )
        );
        $query->setMaxResults(1);

        $timesheettemplate = $query->getOneOrNullResult();

        return $timesheettemplate;
    }

    public function getActiveOrDefaultTimesheetTemplate($employee = null)
    {
        if (!$employee) {
            $employee = $this->getUser();
        }

        $timesheettemplate = $this->getActiveTimesheetTemplate($employee);

        if (!$timesheettemplate) {
            $dql = 'SELECT tt FROM KrasKrasBundle:TimesheetTemplate tt WHERE tt.employee IS NULL ORDER BY tt.id DESC';
            $query = $this->em->createQuery($dql);
            $query->setMaxResults(1);
            $timesheettemplate = $query->getOneOrNullResult();
        }

        return $timesheettemplate;
    }

    public function getFirstNonExistantTimesheet($employee = null)
    {
        if (!$employee) {
            $employee = $this->getUser();
        }

        $dql = 'SELECT t.year, t.week FROM KrasKrasBundle:Timesheet t WHERE t.employee = ?1 ORDER BY t.id ASC';
        $query = $this->em->createQuery($dql);
        $query->setParameter(1, $employee->getId());
        $results = $query->getArrayResult();

        $dt = clone ($employee->getStartedWorking());

        if (!empty($results)) {
            while (in_array(array('year' => $dt->format('o'), 'week' => $dt->format('W')), $results)) {
                $dt->modify('+1 week');
            }
        }

        $now = new \DateTime();
        if ($dt->format('oW') > $now->format('oW')) {
            return null;
        }

        $timesheet = new Timesheet();
        $timesheet
            ->setYear($dt->format('o'))
            ->setWeek($dt->format('W'));
        $this->em->persist($timesheet);
        $this->em->flush();
        $this->em->refresh($timesheet);

        $template = $this->getActiveOrDefaultTimesheetTemplate($employee);
        $extendedTimeFragments = new ArrayCollection();

        foreach ($template->getExtendedTimeFragments() as $extendedTimeFragment) {
            $new_extendedTimeFragment = clone $extendedTimeFragment;
            $new_extendedTimeFragment->setTimesheet($timesheet);

            $start = clone ($timesheet->getStartDate());
            while ($start->format('N') != $new_extendedTimeFragment->getStart()->format('N')) {
                $start->modify('+1 day');
            }

            $start->setTime(
                $extendedTimeFragment->getStart()->format('H'),
                $extendedTimeFragment->getStart()->format('i'),
                $extendedTimeFragment->getStart()->format('s')
            );

            $end = clone $start;
            $end->setTime(
                $extendedTimeFragment->getEnd()->format('H'),
                $extendedTimeFragment->getEnd()->format('i'),
                $extendedTimeFragment->getEnd()->format('s')
            );

            $new_extendedTimeFragment
                ->setStart($start)
                ->setEnd($end);

            $extendedTimeFragments->add($new_extendedTimeFragment);
        }

        $start_date = clone $timesheet->getStartDate();
        $end_date = clone $timesheet->getStartDate();
        $end_date->setTime(23, 59, 59);
        $end_date->modify('+6 days');

        $required_hours = $template->getRequiredHours();

        $dql = 'SELECT ld.date FROM
            KrasKrasBundle:LeaveDate ld
            LEFT JOIN ld.leave l
            LEFT JOIN l.leavetype lt
            LEFT JOIN l.employees e
            WHERE (e.id = ?1 OR e IS NULL)
            AND l.approved = TRUE
            AND (ld.date BETWEEN ?2 AND ?3)';
        $query = $this->em->createQuery($dql);
        $leave_dates = $query
            ->setParameters(
                array(
                    1 => $employee,
                    2 => $start_date,
                    3 => $end_date,
                )
            )
            ->getResult();

        $leave_dates = array_map('current', $leave_dates);
        array_walk(
            $leave_dates,
            function (&$leave_date) {
                $leave_date = $leave_date->format('Y-m-d');
            }
        );
//
//        foreach ($extendedTimeFragments as $extendedTimeFragment) {
//            if (in_array($extendedTimeFragment->getStart()->format('Y-m-d'), $leave_dates)) {
//                $required_hours -= ($extendedTimeFragment->getEnd()->format('U') - $extendedTimeFragment->getStart()->format('U')) / 3600;
//                $extendedTimeFragments->removeElement($extendedTimeFragment);
//            }
//        }

        $dql = 'SELECT t, td FROM
            KrasKrasBundle:Training t
            LEFT JOIN t.trainingDates td WITH (td.date BETWEEN ?2 AND ?3)
            WHERE t.approved = TRUE
            AND t.employee = ?1
            AND (td.date BETWEEN ?2 AND ?3)';
        $query = $this->em->createQuery($dql);
        $trainings = $query
            ->setParameters(
                array(
                    1 => $employee,
                    2 => $start_date,
                    3 => $end_date,
                )
            )
            ->getResult();

        $training_type = $this->em->getRepository('KrasKrasBundle:TimeFragmentType')->findOneBy(
            array('linkTraining' => true)
        );
        $training_dates = array();
        $training_fragments = array();

        foreach ($trainings as $training) {
            foreach ($training->getTrainingDates() as $trainingDate) {
                $training_dates[] = $trainingDate->getDate()->format('Y-m-d');

                $start = clone ($trainingDate->getDate());
                $start->setTime(
                    $trainingDate->getStart()->format('H'),
                    $trainingDate->getStart()->format('i'),
                    $trainingDate->getStart()->format('s')
                );

                $end = clone ($trainingDate->getDate());
                if (($trainingDate->getEnd()->format('U') - $trainingDate->getStart()->format('U')) > 27360) {
                    $end = clone $start;
                    $end->modify('+27360 seconds');
                } else {
                    $end->setTime(
                        $trainingDate->getEnd()->format('H'),
                        $trainingDate->getEnd()->format('i'),
                        $trainingDate->getEnd()->format('s')
                    );
                }

                $fragment = new ExtendedTimeFragment();
                $fragment
                    ->setTimesheet($timesheet)
                    ->setTimeFragmentType($training_type)
                    ->setLinkedTraining($training)
                    ->setStart($start)
                    ->setEnd($end);
                $training_fragments[] = $fragment;
            }
        }

        $training_dates = array_unique($training_dates);

        foreach ($extendedTimeFragments as $extendedTimeFragment) {
            if (in_array($extendedTimeFragment->getStart()->format('Y-m-d'), $training_dates)) {
                $extendedTimeFragments->removeElement($extendedTimeFragment);
            }
        }

        $extendedTimeFragments = new ArrayCollection(
            array_merge($extendedTimeFragments->toArray(), $training_fragments)
        );

        $timesheet
            ->setEmployee($employee)
            ->setSupportCenter($employee->getSupportCenter())
            ->setRequiredHours($required_hours)
            ->setTimesheetTemplate($template)
            ->setExtendedTimeFragments($extendedTimeFragments);

        $this->em->persist($timesheet);
        $this->em->flush();

        $this->em->refresh($timesheet);
        $this->recalculateTimesheetWorkedHours($timesheet);

        $this->em->persist($timesheet);
        $this->em->flush();

        $this->em->refresh($timesheet);

        if (isset($leave_dates) and !empty($leave_dates)) {

            foreach ($leave_dates as $leave_date) {
                $dql = 'SELECT ld.id FROM KrasKrasBundle:LeaveDate ld
                    JOIN ld.leave l
                    JOIN l.leavetype lt
                    LEFT JOIN l.employees e
                    WHERE e.id = ?1
                    AND l.approved = TRUE
                    AND ld.date = ?2';
                $query = $this->em->createQuery($dql);
                $id = $query
                    ->setParameters(
                        array(
                            1 => $employee->getId(),
                            2 => $leave_date,
                        )
                    )
                    ->getSingleScalarResult();
                $leave_date_ob = $this->em->getRepository('KrasKrasBundle:LeaveDate')->findOneById($id);
                $leaves[] = $leave_date_ob->getLeave();
            }

            if (isset($leaves) and !empty($leaves)) {
                foreach ($leaves as $leave) {
                    $this->deleteDefaultFragments($leave);
                }
            }
        }

        $this->recalculateTimesheetWorkedHours($timesheet);

        return $timesheet;
    }

    public function hasLeaveScheduled($leave_date_array, $start_time, $end_time, $employee = 0, $ignore_id = null)
    {
        $date_array = array();
        foreach ($leave_date_array as $leave_date) {
            $date_array[] = $leave_date->getDate()->format('Y-m-d');
        }

        $dql = 'SELECT count(l.id) FROM KrasKrasBundle:UserLeave l
            JOIN l.leavedates ld
            LEFT JOIN l.employees e
            WHERE (e IS NULL or e.id = ?1)
            AND ld.date IN (?2)
            AND (l.startTime < ?4 AND l.endTime > ?3)';

        if ($ignore_id) {
            $dql .= ' AND l.id <> ?5';
        }

        $query = $this->em->createQuery($dql);
        $query->setParameters(
            array(
                1 => $employee,
                2 => $date_array,
                3 => $start_time->format('H:i:s'),
                4 => $end_time->format('H:i:s'),
            )
        );

        if ($ignore_id) {
            $query->setParameter(5, $ignore_id);
        }

        $count = $query->getSingleScalarResult();

        return $count != 0;
    }

    public function unscheduleTraining($training)
    {
        if ($training->getExtendedTimeFragments()->isEmpty()) {
            return;
        }

        foreach ($training->getExtendedTimeFragments() as $fragment) {
            $timesheet = $fragment->getTimesheet();

            $this->em->remove($fragment);
            $this->em->flush();

            $this->recalculateTimesheetWorkedHours($timesheet);
        }

        $this->em->flush();
    }

    public function scheduleTraining($training)
    {
        if (!$training->getExtendedTimeFragments()->isEmpty()) {
            return;
        }

        // Grab the time fragment type for linking trainings
        $fragment_type = $this->em->getRepository('KrasKrasBundle:TimeFragmentType')->findOneBy(
            array(
                'linkTraining' => true,
            )
        );

        if (!$fragment_type) {
            return;
        }

        // Loop through dates for this training
        foreach ($training->getTrainingDates() as $date) {
            // Try to fetch a timesheet this training would be a part of
            $dql = 'SELECT t FROM KrasKrasBundle:Timesheet t
                    WHERE t.employee = ?1
                    AND t.year = ?2
                    AND t.week = ?3';
            $timesheet = $this->em->createQuery($dql)->setParameters(
                array(
                    1 => $training->getEmployee(),
                    2 => $date->getDate()->format('Y'),
                    3 => $date->getDate()->format('W'),
                )
            )->getOneOrNullResult();

            // If no timesheet was found, stop here
            if (!$timesheet) {
                continue;
            }

            $fragment = new ExtendedTimeFragment();
            $fragment->setTimeFragmentType($fragment_type);
            $fragment->setLinkedTraining($training);
            $fragment->setTimesheet($timesheet);

            $start = new \DateTime($date->getDate()->format('Y-m-d') . ' ' . $date->getStart()->format('H:i:s'));
            $fragment->setStart($start);

            $end = new \DateTime($date->getDate()->format('Y-m-d') . ' ' . $date->getEnd()->format('H:i:s'));
            $fragment->setEnd($end);

            $this->em->persist($fragment);
            $this->em->flush();

            $this->em->refresh($timesheet);
            $this->recalculateTimesheetWorkedHours($timesheet);

            $this->em->persist($timesheet);
            $this->em->flush();
        }
    }

    public function scheduleOvertime($employee, $seconds, $start_dates)
    {
        foreach ($start_dates as $start_date) {
            $start_date = clone $start_date->getDate();
            $start_date->modify('-6 months');

            $sec = $seconds;

            $dql = 'SELECT t FROM KrasKrasBundle:Timesheet t
                WHERE t.employee = ?1
                AND t.startDate > ?2
                AND t.overtimeHours > 0
                ORDER BY t.startDate ASC';
            $query = $this->em->createQuery($dql);
            $query->setParameters(
                array(
                    1 => $employee,
                    2 => $start_date,
                )
            );
            $timesheets = $query->getResult();

            foreach ($timesheets as $timesheet) {
                if (!$sec) {
                    break;
                }

                if ($timesheet->getRemainingOvertimeHours() >= ($sec / 3600)) {
                    $timesheet->setUsedOvertimeHours($timesheet->getUsedOvertimeHours() + ($sec / 3600));
                    $sec = 0;
                } else {
                    $timesheet->setUsedOvertimeHours(
                        $timesheet->getUsedOvertimeHours() + $timesheet->getRemainingOvertimeHours()
                    );
                    $sec -= ($timesheet->getRemainingOvertimeHours() * 3600);
                }

                $timesheet->setRemainingOvertimeHours(
                    $timesheet->getOvertimeHours() - $timesheet->getUsedOvertimeHours()
                );
                $this->em->persist($timesheet);
            }

            $this->em->flush();
        }

        $this->setEmployeeOvertime($employee);
    }

    public function unscheduleOvertime($employee, $seconds, $start_dates)
    {
        foreach ($start_dates as $start_date) {
            $start_date = clone $start_date->getDate();
            $start_date->modify('-6 months');

            $sec = $seconds;

            $dql = 'SELECT t FROM KrasKrasBundle:Timesheet t
                WHERE t.employee = ?1
                AND t.startDate > ?2
                AND t.overtimeHours > 0
                ORDER BY t.startDate DESC';
            $query = $this->em->createQuery($dql);
            $query->setParameters(
                array(
                    1 => $employee,
                    2 => $start_date,
                )
            );
            $timesheets = $query->getResult();

            foreach ($timesheets as $timesheet) {
                if (!$sec) {
                    break;
                }

                if ($sec <= ($timesheet->getUsedOvertimeHours() * 3600)) {
                    $timesheet->setUsedOvertimeHours($timesheet->getUsedOvertimeHours() - ($sec / 3600));
                    $sec = 0;
                } else {
                    $sec -= ($timesheet->getUsedOvertimeHours() * 3600);
                    $timesheet->setUsedOvertimeHours(0);
                }

                $timesheet->setRemainingOvertimeHours(
                    $timesheet->getOvertimeHours() - $timesheet->getUsedOvertimeHours()
                );
                $this->em->persist($timesheet);
            }

            $this->em->flush();
        }

        $this->setEmployeeOvertime($employee);
    }

    public function setEmployeeOvertime($employee)
    {
        $employee->setOvertime($this->getRemainingOvertime($employee));
        $this->em->persist($employee);
        $this->em->flush();
    }

    public function getRemainingLeave($leave_type, $employee = 0, $entity_id = null, $year = null)
    {
        if (!$year) {
            $year = gmdate('Y');
        }

        if ($leave_type->getOvertime()) {
            $dql = 'SELECT SUM(t.remainingOvertimeHours) FROM KrasKrasBundle:Timesheet t
                WHERE t.employee = ?1
                AND t.overtimeHours > 0
                AND t.startDate > ?2';
            $query = $this->em->createQuery($dql);
            $query->setParameters(
                array(
                    1 => $employee,
                    2 => new \DateTime('-6 months'),
                )
            );
            $remaining = $query->getSingleScalarResult();

            return $remaining;
        }

        $dql = 'SELECT al.amount FROM KrasKrasBundle:AllowedLeave al
            LEFT JOIN al.employees e
            WHERE (e IS NULL OR e.id = ?1)
            AND al.year = ?2
            AND al.leavetype = ?3';
        $query = $this->em->createQuery($dql);
        $query->setParameters(
            array(
                1 => $employee,
                2 => $year,
                3 => $leave_type,
            )
        );
        $allowed_leaves = $query->getArrayResult();
        $allowed_leaves = array_map('current', $allowed_leaves);

        $remaining = 0;
        foreach ($allowed_leaves as $allowed_leave) {
            if ($allowed_leave == -1) {
                return 9999;
            }

            $remaining += $allowed_leave;
        }

        $start = \DateTime::createFromFormat('Ymd His', $year.'0101 000000');
        $end = \DateTime::createFromFormat('Ymd His', $year.'1231 235959');

        // $dql = 'SELECT ld FROM KrasKrasBundle:LeaveDate ld
        //         LEFT JOIN ld.leave l
        //         LEFT JOIN l.employees e
        //         WHERE (e IS NULL OR e.id = ?1)
        //         AND (ld.date >= ?2 AND ld.date <= ?3)
        //         AND l.approved = TRUE
        //         AND l.leavetype = ?4';

        $dql = 'SELECT l FROM KrasKrasBundle:UserLeave l
                LEFT JOIN l.employees e
                LEFT JOIN l.leavedates ld
                WHERE (e IS NULL OR e.id = ?1)
                AND (ld.date >= ?2 AND ld.date <= ?3)
                AND l.approved = TRUE
                AND l.leavetype = ?4';

        $query = $this->em->createQuery($dql);
        $query->setParameters(
            array(
                1 => $employee,
                2 => $start,
                3 => $end,
                4 => $leave_type,
            )
        );
        $taken_leaves = $query->getResult();

        foreach ($taken_leaves as $taken_leave) {
            if ($entity_id && $taken_leave->getId() == $entity_id) {
                continue;
            }

            $remaining -= ($taken_leave->getDuration() / 3600);

            // if ($entity_id && $taken_leave->getLeave()->getId() == $entity_id) {
            //     continue;
            // }

            // $remaining -= ($taken_leave->)
        }

        return $remaining;
    }

    public function getRemainingOvertime($employee, $entity_id = null)
    {
        $dql = 'SELECT SUM(t.remainingOvertimeHours) FROM KrasKrasBundle:Timesheet t
            WHERE t.employee = ?1
            AND t.overtimeHours > 0
            AND t.startDate > ?2';

        $query = $this->em->createQuery($dql);
        $query->setParameters(
            array(
                1 => $employee,
                2 => new \DateTime('-6 months'),
            )
        );
        $remaining = $query->getSingleScalarResult();
        $remaining = $remaining == null ? 0 : $remaining;

        if ($entity_id) {
            $leave = $this->em->getRepository('KrasKrasBundle:UserLeave')->find($entity_id);
            if ($leave->getLeaveType()->getOvertime()) {
                $remaining += ($leave->getDuration() / 3600);
            }
        }

        return $remaining;
    }

    public function getOvertimeStatus($employee)
    {
        $dql = 'SELECT SUM(t.remainingOvertimeHours) AS remaining, SUM(t.overtimeHours) AS allowed, SUM(t.usedOvertimeHours) AS used FROM KrasKrasBundle:Timesheet t
            WHERE t.employee = ?1
            AND t.overtimeHours > 0
            AND t.startDate > ?2';
        $query = $this->em->createQuery($dql);
        $query->setParameters(
            array(
                1 => $employee,
                2 => new \DateTime('-6 months'),
            )
        );
        $status = $query->getArrayResult();
        $status = array_shift($status);

        array_walk(
            $status,
            function (&$val) {
                $val *= 3600;
            }
        );

        $dql = 'SELECT lt.name FROM KrasKrasBundle:LeaveType lt
            WHERE lt.overtime = TRUE';
        $query = $this->em->createQuery($dql);
        $result = $query->getOneOrNullResult();

        $status['name'] = $result['name'];

        return $status;
    }

    public function getLeaveStatus($employee)
    {
        $data = array();

        $dql = 'SELECT al FROM KrasKrasBundle:AllowedLeave al
            LEFT JOIN al.employees e
            INNER JOIN al.leavetype lt
            WHERE (e IS NULL OR e.id = ?1)
            AND lt.overtime = FALSE';
        $query = $this->em->createQuery($dql);
        $query->setParameter(1, $employee);
        $allowed_leaves = $query->getResult();

        foreach ($allowed_leaves as $allowed_leave) {
            $year = $allowed_leave->getYear();
            $type = (string) $allowed_leave->getLeaveType();
            $amount = $allowed_leave->getAmount();

            if (!array_key_exists($year, $data)) {
                $data[$year] = array();
            }

            if (!array_key_exists($type, $data[$year])) {
                $data[$year][$type] = array(
                    'allowed'   => 0,
                    'used'      => 0,
                    'remaining' => 0,
                );
            }

            if ($data[$year][$type]['allowed'] == -1) {
                continue;
            }

            if ($allowed_leave->getAmount() == -1) {
                $data[$year][$type]['allowed'] = -1;
            } else {
                $data[$year][$type]['allowed'] += ($allowed_leave->getAmount() * 3600);
            }
        }

        $dql = 'SELECT l FROM KrasKrasBundle:UserLeave l
            LEFT JOIN l.employees e
            INNER JOIN l.leavetype lt
            INNER JOIN l.leavedates ld
            WHERE (e IS NULL OR e.id = ?1)
            AND l.approved = TRUE
            AND lt.overtime = FALSE';
        $query = $this->em->createQuery($dql);
        $query->setParameter(1, $employee);
        $taken_leaves = $query->getResult();

        foreach ($taken_leaves as $taken_leave) {
            if (!$taken_leave->getLeaveDates()->count()) {
                continue;
            }

            $year = $taken_leave->getLeaveDates()->first()->getDate()->format('Y');
            $type = (string) $taken_leave->getLeaveType();

            if (!array_key_exists($year, $data)) {
                $data[$year] = array();
            }

            if (!array_key_exists($type, $data[$year])) {
                $data[$year][$type] = array(
                    'allowed'   => 0,
                    'used'      => 0,
                    'remaining' => 0,
                );
            }

            $data[$year][$type]['used'] += $taken_leave->getDuration();
        }

        foreach ($data as $year => $value) {
            foreach ($value as $type => $val) {
                $data[$year][$type]['remaining'] = $val['allowed'] == -1 ? -1 : (($val['allowed'] - $val['used']) < 0 ? 0 : ($val['allowed'] - $val['used']));
            }
        }

        krsort($data, SORT_NUMERIC);
        array_walk(
            $data,
            function (&$value) {
                ksort($value);
            }
        );

        return $data;
    }

    public function getEmployeesWorkedTimeByDates($employees, $data)
    {
        $result = array();

        $dql = 'SELECT etf, tft FROM KrasKrasBundle:ExtendedTimeFragment etf
            JOIN etf.timesheettemplate tt
            JOIN tt.timesheets t
            JOIN t.employee e
            LEFT JOIN etf.timefragmenttype tft
            WHERE e.id IN (?1)
            AND t.startDate = ?2';
        $query = $this->em->createQuery($dql);

        foreach ($data as $d) {
            $date = new \DateTime($d['date']);
            $start_date = clone $date;

            $start_date->modify('this week');
            $start_date->setTime(00, 00, 00);
            $query->setParameters(
                array(
                    1 => $employees,
                    2 => $start_date,
                )
            );

            $fragments = $query->getResult();

            if (empty($fragments)) {
                continue;
            }

            foreach ($fragments as $fragment) {
                if ($fragment->getStart()->format('N') == $date->format('N')) {
                    if (!array_key_exists($d['name'], $result)) {
                        $result[$d['name']] = array(
                            $d['name'],
                            0,
                            '0.00 hours',
                        );
                    }

                    $result[$d['name']][1] += $fragment->getEnd()->format('U') - $fragment->getStart()->format('U');
                }
            }
        }

        foreach ($result as $key => $res) {
            $result[$key][2] = number_format($res[1] / 3600, 2) . " hours";
        }

        return $result;
    }

    public function getSupportCentersWorkedTimeByDates($support_centers, $data)
    {
        $result = array();

        $dql = 'SELECT etf, tft FROM KrasKrasBundle:ExtendedTimeFragment etf
            JOIN etf.timesheettemplate tt
            JOIN tt.timesheets t
            JOIN t.supportcenter sc
            LEFT JOIN etf.timefragmenttype tft
            WHERE sc.id IN (?1)
            AND t.startDate = ?2';
        $query = $this->em->createQuery($dql);

        foreach ($data as $d) {
            $date = new \DateTime($d['date']);
            $start_date = clone $date;

            $start_date->modify('this week');
            $start_date->setTime(00, 00, 00);
            $query->setParameters(
                array(
                    1 => $support_centers,
                    2 => $start_date,
                )
            );

            $fragments = $query->getResult();

            if (empty($fragments)) {
                continue;
            }

            foreach ($fragments as $fragment) {
                if ($fragment->getStart()->format('N') == $date->format('N')) {
                    if (!array_key_exists($d['name'], $result)) {
                        $result[$d['name']] = array(
                            $d['name'],
                            0,
                            '0.00 hours',
                        );
                    }

                    $result[$d['name']][1] += $fragment->getEnd()->format('U') - $fragment->getStart()->format('U');
                }
            }
        }

        foreach ($result as $key => $res) {
            $result[$key][2] = number_format($res[1] / 3600, 2) . " hours";
        }

        return $result;
    }

    public function deleteDefaultFragments($leave)
    {
        if (!$leave->getApproved())  {
            return;
        }

        $em = $this->em;

        $employee_ids = array();

        if ($leave->getEmployees()->isEmpty()) {
            $employee_ids = $this->em->createQuery('SELECT u.id FROM KrasUserBundle:User u WHERE u.enabled = 1')->getArrayResult();
            $employee_ids = array_column($employee_ids, 'id');
        } else {
            $employee_ids = $leave->getEmployees()->map(function($v) { return $v->getId(); })->toArray();
        }

        $employee_ids[] = 0;

        foreach ($leave->getLeaveDates() as $leaveDate) {
            $date = $leaveDate->getDate();

            $start = \DateTime::createFromFormat('Ymd H:i:s', $date->format('Ymd').' 00:00:00');
            $end = \DateTime::createFromFormat('Ymd H:i:s', $date->format('Ymd').' 23:59:59');

            if ($leave->getStartTime() && $leave->getEndTime() && $leave->getStartTime() != $leave->getEndTime()) {
                $start = $leave->getStartTime();
                $end = $leave->getEndTime();

                $start->setDate($date->format("Y"), $date->format("m"), $date->format("d"));
                $end->setDate($date->format("Y"), $date->format("m"), $date->format("d"));
            }

            $dql = 'SELECT etf FROM KrasKrasBundle:ExtendedTimeFragment etf
                JOIN etf.timesheet ts
                LEFT JOIN ts.employee e
                WHERE e.id IN (?1)
                AND (etf.start < ?3 AND etf.end > ?2)';
            $query = $em->createQuery($dql)->setParameters(array(
                1 => $employee_ids,
                2 => $start->format('Y-m-d H:i:s'),
                3 => $end->format('Y-m-d H:i:s'),
            ));

            $TimeFragments = $query->getResult();

            foreach ($TimeFragments as $fragment) {
                $em->remove($fragment);
                $em->flush();
            }
        }
    }

    public function checkNotApprovedLeaves($employee, $leaveType, $durations = null, $year = null)
    {
        if (!$year) {
            $year = gmdate('Y');
        }

        $em = $this->em;

        if ($durations === null) {
            $durations = array();
        }

        $allowed_time = $this->getRemainingLeave($leaveType, $employee, null, $year);

        /*
         * select not approved and not rejected leaves
         *
         * l.approved is :
         *      null = not approved
         *      1 = approved
         *      0 = rejected
         *
         */
        $dql = 'SELECT l FROM KrasKrasBundle:UserLeave l
                    LEFT JOIN l.employees e
                    WHERE  e.id = ?1
                    AND (l.approved IS NULL )
                    AND l.leavetype = ?2 ';
        $query = $em->createQuery($dql);
        $query->setParameters(
            array(
                1 => $employee,
                2 => $leaveType
            )
        );
        $taken_leaves = $query->getResult();

        foreach ($taken_leaves as $taken_leave) {
            $durations[] = $taken_leave->getEndTime()->format('U') - $taken_leave->getStartTime()->format('U');
        }

        return $allowed_time < (array_sum($durations) / 3600);
    }

    public function getLeavesByTimesheetAndEmploe(Timesheet $timesheet)
    {

        $user = $timesheet->getEmployee();


        $em = $this->em;

        $startDate = $timesheet->getStartDate();

        $end_date = clone $startDate;
        $end_date->setTime(23, 59, 59);
        $end_date->modify('+6 days');

        /*
         * I do not understand why there is no check for approval
         */
        $dql = 'SELECT l, lt, ld FROM
            KrasKrasBundle:UserLeave l
            JOIN l.leavetype lt
            JOIN l.leavedates ld
            LEFT JOIN l.employees e
            WHERE (e.id = ?1 OR e IS NULL)
            AND (ld.date BETWEEN ?2 AND ?3)
            ORDER BY l.startTime ASC';
        $query = $em->createQuery($dql);
        $entities = $query
            ->setParameters(
                array(
                    1 => $user,
                    2 => $startDate,
                    3 => $end_date,
                )
            )
            ->getResult();

        return $entities;
    }

    public function removeExtraFragments(TimesheetTemplate $template, $id)
    {
        $fragments = $template->getExtendedTimeFragments();

        $timesheet = $this->em->getRepository('KrasKrasBundle:Timesheet')->findOneById($id);

        $leaves = $this->getLeavesByTimesheetAndEmploe($timesheet);


        foreach ($fragments as $fragment) {
            foreach ($leaves as $leave) {
                $leaveDays = $leave->getLeaveDates();
                foreach ($leaveDays as $leaveDay) {
                    $leaveDayOfWeek = date("w", $leaveDay->getDate()->getTimestamp());
                    $fragmentDayOfWeek = date("w", $fragment->getStart()->getTimestamp());

                    if ($leaveDayOfWeek === $fragmentDayOfWeek) {


                        $startDate2 = $fragment->getStart()->setDate(1, 1, 1)->getTimestamp();
                        $endDate2 = $fragment->getEnd()->setDate(1, 1, 1)->getTimestamp();

                        $startDate1 = $leave->getStartTime()->setDate(1, 1, 1)->getTimestamp();
                        $endDate1 = $leave->getEndTime()->setDate(1, 1, 1)->getTimestamp();

                        $overlap = max(0, min($endDate1, $endDate2) - max($startDate1, $startDate2));

                        if ($overlap > 0) {
                            $template->removeExtendedTimeFragment($fragment);
                        } else {
                            $this->em->refresh($fragment);
                        }
                    }
                }
            }
        }

        return $template;
    }


}
