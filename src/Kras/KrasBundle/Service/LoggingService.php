<?php
namespace Kras\KrasBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAware;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;
use Kras\KrasBundle\Entity\SystemLogEntry;
use Symfony\Component\HttpFoundation\Request;

class LoggingService extends ContainerAware {

	private $em;
	private $context;
    private $request;

    public function __construct(EntityManager $em, SecurityContext $context, Request $request)
    {
        $this->em = $em;
        $this->context = $context;
        $this->request = $request;
    }

    private function getUser()
    {
        return $this->context->getToken()->getUser();
    }

    public function log($object, $action)
    {
        $log = new SystemLogEntry();
        $log
            ->setUser((string) $this->getUser())
            ->setObject(get_class($object) . ' (' . $object->getId() . ') - ' . ((string) $object))
            ->setAction($action)
            ->setIp($this->request->getClientIp());

        // Persist
        $this->em->persist($log);
        $this->em->flush();
    }
}
