<?php

namespace Kras\KrasBundle\Service;

use Kras\KrasBundle\Entity\Member;

class MemberService {

    public function handleRequired(Member &$entity) {
        // If a few "required" fields are filled in, set completed to true
        if(count($entity->getPhoneNumbers()) > 0
            && $entity->getAddress() != null
            && $entity->getBirthDate() != null
            && $entity->getPostalCode() != null
            && $entity->getCity() != null) {
            $entity->setCompleted(true);
        }
    }

}