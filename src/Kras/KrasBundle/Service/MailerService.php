<?php
namespace Kras\KrasBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAware;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;

class MailerService extends ContainerAware {

	private $mailer;
	private $em;
	private $twig;
	private $context;
	private $address;

    public function __construct(\Swift_Mailer $mailer, EntityManager $em, \Twig_Environment $twig, SecurityContext $context, $address)
    {
        $this->mailer = $mailer;
        $this->em = $em;
        $this->twig = $twig;
        $this->context = $context;
        $this->address = $address;
    }

    public function sendFeedbackToSuperAdmins($message, $options = array())
    {
    	$recipients = $this->em->createQueryBuilder()
    		->select('u')
    		->from('KrasUserBundle:User', 'u')
    		->where('u.roles LIKE :role')
    		->setParameter('role', '%ROLE_SUPER_ADMIN%')
    		->getQuery()
    		->getResult();

    	$template = 'KrasKrasBundle:Emails:feedback.html.twig';

    	$subject = 'Kras Application - new feedback received';

    	$this->sendEmail(array(
    		'recipients'		=> $this->convertUsersToAddressees($recipients),
    		'subject'			=> $subject,
    		'template'			=> $template,
    		'message'			=> $message,
            'referer'           => $options['referer']
    	));
    }

    private function sendEmail($options)
    {
    	if (!array_key_exists('senders', $options)) {
    		$options['senders'] = $this->convertUserstoAddressees(array($this->getUser()));
    	}

    	$body = $this->twig->render(
            $options['template'],
            $options
        );

        $message = \Swift_Message::newInstance()
            ->setSubject($options['subject'])
            ->setFrom(array($this->address => 'Kras Service (no-reply)'))
            ->setTo($options['recipients'])
            ->setBody($body)
        ;

        $this->mailer->send($message);
    }

    private function convertUsersToAddressees($users)
    {
    	$addressees = array();
    	foreach ($users as $user){
    		$addressees[$user->getEmail()] = (string) $user;
    	}
    	return $addressees;
    }

    private function getUser()
    {
        return $this->context->getToken()->getUser();
    }
}