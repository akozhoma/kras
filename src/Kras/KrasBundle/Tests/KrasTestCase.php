<?php
namespace Kras\KrasBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Kras\UserBundle\Entity\User;

class KrasTestCase extends WebTestCase{
    protected $instances = array();

    protected $user_user;
    protected $user_pass;
    protected $user_mail;

    protected $client;

    public function __construct()
    {
        if (!empty($this->instances)) {
            return;
        }

        $this->user_user = 'test_unit_user_'.md5(uniqid());
        $this->user_pass = 'test_unit_pass_'.md5(uniqid());
        $this->user_mail = 'test_unit_mail_'.md5(uniqid()).'@example.com';

        $this->client = static::createClient();

        $user = new User();
        $user
            ->setUsername($this->user_user)
            ->setPlainPassword($this->user_pass)
            ->setEmail($this->user_mail)
            ->addRole('ROLE_ADMIN')
            ->addRole('ROLE_SUPER_ADMIN');

        $session = $this->client->getContainer()->get('session');

        $firewall = 'main';
        $token = new UsernamePasswordToken($user, $this->user_pass, $firewall, array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN'));
        $session->set('_security_'.$firewall, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }

    public function __destruct()
    {
        foreach ($this->instances as $key => $instance) {
            if ($this == $instance) {
                unset($this->instances[$key]);
            }
        }

        if (!empty($this->instances)) {
            return;
        }
    }

    protected function getClient()
    {
        return $this->client;
    }

    /**
     * Selects the first option of a choice form field.
     * @param \Symfony\Component\DomCrawler\Field\ChoiceFormField $field
     */
    public function selectFirstOption(\Symfony\Component\DomCrawler\Field\ChoiceFormField &$field) {
        $values = $field->availableOptionvalues();
        $field->select($values[0]);
    }

 }
