/**
 * This script is intended to make the usage of Doctrine Collections and 
 * Symfony2 form prototypes a lot easier. Simply adding the class
 * 'dynamic-collection' will attempt to bind an element with class 'dynamic-collection--item-add'
 * to duplicating a prototype, and will attempt to bind an element with class
 * 'dynamic-collection-item-delete' to deleting the cloned prototype with class 'dynamic-collection-item'
 * it is a part of.
 *
 * @author Kalman Olah <kalman@inuits.eu>
 */
(function($) {
    $(function() {
        // Loop through each of the dynamic collections
        $('.dynamic-collection').each(function() {
            // Store $(this);
            var $this = $(this);

            // Get the prototype
            var prototype = $this.data('prototype');

            // Set the initial index of this container
            $this.data('index', $this.find(':input').length);

            // When the delete link of any existing items is clicked, delete its prototype
            $this.find('.dynamic-collection-item-delete').click(function(e) {
                // Remove closest prototype
                $(this).closest('.dynamic-collection-item').remove();

                // Avoid following a link to the top of the page
                e.preventDefault(true);
                return false;
            });

            // When the add link is clicked, append a new prototype
            $this.find('.dynamic-collection-item-add').click(function(e) {
                // Grab the index
                var index = $this.data('index');

                // Clone the prototype
                var newForm = prototype.replace(/__name__/g, index);

                // Increment the index
                $this.data('index', index + 1);

                // Insert the prototype before the add button
                newForm = $(newForm).insertBefore($(this));

                // Reload chosen (see main.js)
                reloadChosen();

                // When the delete link is clicked, delete this prototype
                newForm.find('.dynamic-collection-item-delete').click(function(e) {
                    // Remove closest prototype
                    $(this).closest('.dynamic-collection-item').remove();

                    // Avoid following a link to the top of the page
                    e.preventDefault(true);
                    return false;
                });

                // Avoid following a link to the top of the page
                e.preventDefault(true);
                return false;
            });
        });
    });
})(jQuery);