/**
 * This script will take any element with class 'delete-button' and ask for confirmation in a modal dialog
 * before a POST request is sent to its 'data-delete-target=""' value.
 * Now optimized for Bootstrap 3 and POST requests.
 *
 * @author Kalman Olah <kalman@inuits.eu>
 */
(function($){
    $(function(){
        DeleteButton = {
            bindTo: function(elem) {
                // Cache our modal container (currently, it's present on all pages since it's in the base template)
                var $modal = $('#delete-dialog');
                var $button = $('#delete-target-submit');
                var $label = $('#delete-target-label');

                $(elem).unbind('click.deletebutton').on('click.deletebutton', function(e) {

                    // Cache the clicked element
                    var $this = $(this);

                    // Get the element's delete target
                    var target = $this.data('delete-target');

                    // Set the label of the delete target within the dialog
                    var label = $this.data('delete-target-label');
                    if (label) {
                        label = '<span class="label label-danger">' + label + '</span>';
                    } else {
                        label = $label.data('delete-target-label-default');
                    }
                    $label.html(label);

                    // Bind the action button to actually doing stuff
                    $button
                        .unbind('click')
                        .click(function(e) {
                            // Visit the target
                            window.location = target;
                        });

                    // Show our modal
                    $modal.modal('show');

                    // Make sure we don't accidentally do something we'll regret
                    e.preventDefault(true);
                    return false;
                });
            },

            rebind: function() {
                this.bindTo('.delete-button');
            }
        };

        DeleteButton.bindTo('.delete-button');
    });
})(jQuery);