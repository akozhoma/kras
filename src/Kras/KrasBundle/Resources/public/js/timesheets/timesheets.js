(function($) {
    function DatetimeToDate(dt) {
        var t = dt.split(/[- :T+]/);
        var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);

        return d;
    }

    function formatTime(secs, format) {
        if (!format) {
            format = '%hh %mm %ss';
        }

        var hours = ("0" + Math.floor(secs / 3600)).slice(-2),
        minutes = ("0" + Math.floor((secs % 3600) / 60)).slice(-2),
        seconds = ("0" + (secs % 60)).slice(-2);

        return format
            .replace('%h', hours)
            .replace('%m', minutes)
            .replace('%s', seconds);
    }

    function reloadTimesheetData() {
        $('.timesheet .events').html('');

        $.getJSON(app_timesheet_data_path, function(data) {
            _events = {};
            $('.overtime-warning').removeClass('hidden').hide();

            if (data.overtime_hours > 0) {
                $('.overtime-warning-worked').html(data.worked_hours);
                $('.overtime-warning-overtime').html(data.overtime_hours);
                $('.overtime-warning').show();
            }

            if (data.comments) {
                $('.timesheet-comments').val(data.comments);
            }

            if (data.extendedtimefragments) {
                $.each(data.extendedtimefragments, function(key, value) {
                    _events[value.id] = value;

                    var start = DatetimeToDate(value.start);
                    var end   = DatetimeToDate(value.end);

                    var start_secs = ((start.getHours() * 60) + start.getMinutes()) * 60;
                    var end_secs   = ((end.getHours() * 60) + end.getMinutes()) * 60;

                    var start_str = formatTime(start_secs, '%h:%m');
                    var end_str   = formatTime(end_secs, '%h:%m');

                    var diff_seconds = end_secs - start_secs;

                    var event_height = Math.round(diff_seconds / 100);

                    var day_of_week = start.getDay();
                    if (day_of_week < 1) day_of_week = day_of_week + 7;

                    var html = '<div data-start="' + start_str + '" data-end="' + end_str + '" class="event resizable" data-event-id="' + value.id + '" style="height:' + (event_height < 36 ? 36 : event_height) + 'px;background-color:' + (value.timefragmenttype ? value.timefragmenttype.color : '#cccccc') + ';">';

                        if (typeof app_timesheet_event_delete_path != 'undefined') {
                            html +='<button class="event-delete-btn btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>';
                        }

                        html += '<span class="event-start">' + start_str + '</span> \
                        <span class="event-end">' + end_str + '</span> \
                        <div class="padded">';

                        if (value.linkedactivity) {
                            html += '<div class="text-center h4 truncate">' + value.linkedactivity.name + '</div>';
                        }

                        if (value.linkedmember) {
                            html += '<div class="text-center h4 truncate">' + value.linkedmember.firstname + ' ' + value.linkedmember.lastname + '</div>';
                        }

                        if (value.linkedtraining) {
                            html += '<div class="text-center h4 truncate">' + value.linkedtraining.name + '</div>';
                        }

                        html += '<div class="text-center h5">' + (value.timefragmenttype ? value.timefragmenttype.name : app_timesheet_locale.no_type) + '</div>';
                        if (value.notes) {
                            html += '<h5 class="text-center">' +  value.notes + '</h5>';
                        }
                        html += '</div>';
                        html += '</div>';

                    $('.timesheet .events[data-dow="' + day_of_week + '"]').append(html);
                });
            }

            rebindEventDeletion();
            rebindEventView();

            if (typeof app_timesheet_duration_update_path != 'undefined') {
                rebindEventResizing();
            }

            if (typeof app_timesheet_template_event_list_path == 'undefined') {
                return;
            }

            $.getJSON(app_timesheet_template_event_list_path, function(data) {
                _template_events = {};

                $.each(data.extendedtimefragments, function(key, value) {
                    _template_events[value.id] = value;

                    var start = DatetimeToDate(value.start);
                    var end   = DatetimeToDate(value.end);

                    var start_secs = ((start.getHours() * 60) + start.getMinutes()) * 60;
                    var end_secs   = ((end.getHours() * 60) + end.getMinutes()) * 60;

                    var start_str = formatTime(start_secs, '%h:%m');
                    var end_str   = formatTime(end_secs, '%h:%m');

                    var diff_seconds = end_secs - start_secs;

                    var event_height = Math.round(diff_seconds / 100);

                    var day_of_week = start.getDay();
                    if (day_of_week < 1) day_of_week = day_of_week + 7;

                    if ($('.timesheet .events[data-dow="' + day_of_week + '"] .event[data-start="' + start_str + '"]').length
                        || $('.timesheet .events[data-dow="' + day_of_week + '"] .event[data-end="' + end_str + '"]').length) {
                        return;
                    }

                    var html = '<div class="template-event" data-template-event-id="' + value.id + '" style="height:' + event_height + 'px"> \
                        <span class="event-start">' + start_str + '</span> \
                        <span class="event-end">' + end_str + '</span> \
                        <div class="padded"> \
                        <h4 class="text-muted">' + (value.timefragmenttype ? value.timefragmenttype.name : '<i class="fa fa-ellipsis-h fa-lg"></i>') + '</h4> \
                        </div> \
                        </div>';

                    $('.timesheet .events[data-dow="' + day_of_week + '"]').append(html);
                });

                rebindTemplateEventForm();
            });
        });

        if (typeof app_timesheet_leave_path == 'undefined') {
            return;
        }

        $.getJSON(app_timesheet_leave_path, function(data) {
            _leaves = {};

            for (var i = 0; i < data.length; i++) {
                var dates = [];

                for (var j = 0; j < data[i].leavedates.length; j++) {
                    var date = data[i].leavedates[j].date.split('T')[0];

                    if (data[i].start_time && data[i].end_time) {
                        dates.push({
                            start: date + 'T' + data[i].start_time.split('T')[1],
                            end: date + 'T' + data[i].end_time.split('T')[1]
                        });
                    } else {
                        dates.push({
                            all_day: true,
                            start: date + 'T02:02:02',
                            end: date + 'T03:03:03'
                        });
                    }
                }

                for (var j = 0; j < dates.length; j++) {
                    var value = dates[j];

                    var start = DatetimeToDate(value.start);
                    var end   = DatetimeToDate(value.end);

                    var start_secs = ((start.getHours() * 60) + start.getMinutes()) * 60;
                    var end_secs   = ((end.getHours() * 60) + end.getMinutes()) * 60;

                    var start_str = formatTime(start_secs, '%h:%m');
                    var end_str   = formatTime(end_secs, '%h:%m');

                    var diff_seconds = end_secs - start_secs;

                    var event_height = Math.round(diff_seconds / 100);

                    var day_of_week = start.getDay();
                    if (day_of_week < 1) day_of_week = day_of_week + 7;

                    if (value.all_day) {
                        start_str = app_timesheet_locale.all_day;
                        end_str   = app_timesheet_locale.all_day;
                    }

                    // if ($('.timesheet .events[data-dow="' + day_of_week + '"] .event[data-start="' + start_str + '"]').length
                    //     || $('.timesheet .events[data-dow="' + day_of_week + '"] .event[data-end="' + end_str + '"]').length) {
                    //     return;
                    // }

                    var html = '<div class="leave" data-leave-id="' + data[i].id + '" style="height:' + event_height + 'px"> \
                        <span class="event-start">' + start_str + '</span> \
                        <span class="event-end">' + end_str + '</span> \
                        <div class="padded"> \
                        <h4><i class="fa fa-calendar"></i> ' + data[i].leavetype.name + '</h4> \
                        </div> \
                        </div>';

                    $('.timesheet .events[data-dow="' + day_of_week + '"]').append(html);
                }
            }
        });

        rebuildHours();
    }

    function rebindTemplateEventForm() {
        $('.timesheet .template-event').unbind('click').click(function(e) {
            $('.timesheet_day_of_week').prop('value', $(this).closest('.day').data('dow'));

            var template_event = _template_events[$(this).data('template-event-id')];

            var start = DatetimeToDate(template_event.start);
            var $start_inputs = $('#timesheet-modal .time-start-dropdown select');
            $start_inputs.first().val(start.getHours());
            $start_inputs.last().val(start.getMinutes());

            var end = DatetimeToDate(template_event.end);
            var $end_inputs = $('#timesheet-modal .time-end-dropdown select');
            $end_inputs.first().val(end.getHours());
            $end_inputs.last().val(end.getMinutes());

            $('#timesheet-modal').modal('show');
        });
    }

    function rebindEventView() {
        $('.timesheet .event').unbind('click').click(function(e) {
            if(!$(e.target).is('.ui-resizable-handle')){
                var event_id = $(this).data('event-id');
                _active_event_id = event_id;
                var event = _events[event_id];

                $('.timesheet_day_of_week').prop('value', $(this).closest('.day').data('dow'));

                var html = '<div> \
                    <table class="table table-bordered table-striped"> \
                    <tr><th>' + app_timesheet_locale.type + '</th><td>' + (event.timefragmenttype ? event.timefragmenttype.name : app_timesheet_locale.no_type) + '</td></tr> \
                    <tr><td colspan="2"></td></tr> \
                    <tr><th>' + app_timesheet_locale.start + '</th><td>' + DatetimeToDate(event.start).toString() + '</td></tr> \
                    <tr><th>' + app_timesheet_locale.end + '</th><td>' + DatetimeToDate(event.end).toString() + '</td></tr>';

                    if (event.linkedactivity || event.linkedmember || event.linkedtraining || event.notes) {
                        html += '<tr><td colspan="2"></td></tr>';
                    }

                    if (event.linkedactivity) {
                        html += '<tr><th>' + app_timesheet_locale.activity + '</th><td>' + event.linkedactivity.name + '</td></tr> \
                        <tr><th>' + app_timesheet_locale.link + '</th><td><a class="btn btn-info btn-xs" href="' + app_timesheet_locale.activity_link.replace('ID', event.linkedactivity.id) + '">' + app_timesheet_locale.click_here + '</a></td></tr>';
                    }

                    if (event.linkedmember) {
                        html += '<tr><th>' + app_timesheet_locale.member + '</th><td>' + event.linkedmember.firstname + ' ' + event.linkedmember.lastname + '</th></tr> \
                        <tr><th>' + app_timesheet_locale.link + '</th><td><a class="btn btn-info btn-xs" href="' + app_timesheet_locale.member_link.replace('ID', event.linkedmember.id) + '">' + app_timesheet_locale.click_here + '</a></td></tr>';
                    }

                    if (event.linkedtraining) {
                        html += '<tr><th>' + app_timesheet_locale.training + '</th><td>' + event.linkedtraining.name + '</td></tr> \
                        <tr><th>' + app_timesheet_locale.training_price + '</th><td>' + event.linkedtraining.price + '</td></tr> \
                        <tr><th>' + app_timesheet_locale.training_organizer + '</th><td>' + event.linkedtraining.organizer + '</td></tr>';
                    }

                    if ((event.linkedactivity || event.linkedmember || event.linkedtraining) && event.notes) {
                        html += '<tr><td colspan="2"></td></tr>';
                    }

                    if (event.notes) {
                        html += '<tr><th>' + app_timesheet_locale.notes + '</th><td>' + event.notes + '</td></tr>';
                    }

                    html += '</table> \
                    </div>';

                $('#timesheet-event-modal .modal-body').html(html);
                $('#timesheet-event-modal').modal('show');
            }
        });
    }

    function rebindEventDeletion() {
        $('.timesheet .event-delete-btn').unbind('click').click(function(e) {
            $.ajax(app_timesheet_event_delete_path.replace('EVENT_ID', $(this).closest('.event').data('event-id')), {
                type: 'POST',
                complete: function(response) {
                    reloadTimesheetData();
                }
            });

            e.preventDefault(true);
            return false;
        });
    }

    function formatNumberLength(num, length) {
        var r = "" + num;
        while (r.length < length) {
            r = "0" + r;
        }
        return r;
    }

    function rebindEventResizing() {
        $('.resizable').resizable({
            autoHide:    true,
            grid:        [9, 9],
            handles:     "n, s",
            minHeight:   36
        }).on('resize', function(e, ui) {
            var $this = $(this);

            var start = $this.data('start'),
                  end = $this.data('end');

            var start_seconds = (start.split(':')[0] * 3600) + (start.split(':')[1] * 60),
                  end_seconds = (end.split(':')[0] * 3600) + (end.split(':')[1] * 60);

            var duration = (($this.height() / 9) * 15 * 60);

            var axis = ui.position.top == 0 ? 's' : 'n';
            switch (axis) {
                case 's':
                    end_seconds = start_seconds + duration;
                    end = formatNumberLength(Math.floor(end_seconds / 3600), 2) +
                    ':' + formatNumberLength(Math.ceil((end_seconds % 3600) / 60), 2);

                    max_height = (((86400 - start_seconds) / 60) / 15) * 9;

                    break;

                case 'n':
                    start_seconds = end_seconds - duration;
                    start = formatNumberLength(Math.floor(start_seconds / 3600), 2) +
                      ':' + formatNumberLength(Math.ceil((start_seconds % 3600) / 60), 2);

                    max_height = (((86400 - (86400 - end_seconds)) / 60) / 15) * 9;

                    break;
            }

            $this
                .resizable("option", "maxHeight", max_height)
                .css('margin-top', ui.position.top < 0 ? 0 : ui.position.top)
                .css('top', 0)
                .attr('data-start', start)
                .attr('data-end', end)
                .data('start', start)
                .data('end', end);

            $this.find('.event-end').html(end);
            $this.find('.event-start').html(start);
        }).on('resizestop', function(e, ui) {
            var $this = $(this);

            $this
                .resizable('option', 'maxHeight', 99999)
                .css('margin-top', 0);

            $.ajax(app_timesheet_duration_update_path.replace('EVENT_ID', $this.data('event-id')), {
                type: 'POST',
                data: {
                    start:       $this.data('start'),
                    end:         $this.data('end'),
                    day_of_week: $this.closest('.events').data('dow')
                },
                dataType: 'html',
                success: function(data, xhr) {
                    if (data == 'INVALID') {
                        reloadTimesheetData();
                    }
                }
            });
        });
    }

    function reloadFragmentAddons() {
        if ($('#timesheet-event-update-modal .fragment_type').val() == '') {
            $('#timesheet-event-update-modal .link_activity, #timesheet-event-update-modal .link_member, #timesheet-event-update-modal .link_training').toggle(false);
        }
        
        if ($('#timesheet-modal .fragment_type').val() == '') {
            $('#timesheet-modal .link_activity, #timesheet-modal .link_member, #timesheet-modal .link_training').toggle(false);
        }
        
        $.each(app_fragment_types, function(key, fragment_type) {
            if (fragment_type.name == $('#timesheet-event-update-modal .fragment_type option:selected').html()) {
                $('#timesheet-event-update-modal .link_activity').toggle(fragment_type.link_activity);
                $('#timesheet-event-update-modal .link_member').toggle(fragment_type.link_member);
                $('#timesheet-event-update-modal .link_training').toggle(fragment_type.link_training);
            }

            if (fragment_type.name == $('#timesheet-modal .fragment_type option:selected').html()) {
                $('#timesheet-modal .link_activity').toggle(fragment_type.link_activity);
                $('#timesheet-modal .link_member').toggle(fragment_type.link_member);
                $('#timesheet-modal .link_training').toggle(fragment_type.link_training);
            }
        });
        
        $('#timesheet-modal .link_training').is(":visible")
            ? $('#timesheet-modal select.link_training').attr("required", "required")
            : $('#timesheet-modal select.link_training').removeAttr("required")
        ;
        $('#timesheet-event-update-modal .link_training').is(":visible")
            ? $('#timesheet-event-update-modal  select.link_training').attr("required", "required")
            : $('#timesheet-event-update-modal  select.link_training').removeAttr("required")
        ;
    }

    function initTimesheetEventUpdating() {
        $('.timesheet-event-edit-btn').click(function() {
            $('#timesheet-event-update-modal').modal('show');

            var event = _events[_active_event_id];

            if (event.timefragmenttype) {
                $('#timesheet-event-update-modal .fragment_type').val($('#timesheet-event-update-modal .fragment_type option:contains("' + event.timefragmenttype.name + '")').prop('value'));
                $('#timesheet-event-update-modal .link_activity').toggle(event.timefragmenttype.link_activity);
                $('#timesheet-event-update-modal .link_member').toggle(event.timefragmenttype.link_member);
                $('#timesheet-event-update-modal .link_training').toggle(event.timefragmenttype.link_training);
            } else {
                $('#timesheet-event-update-modal .fragment_type').val('');
                $('#timesheet-event-update-modal .link_activity, #timesheet-event-update-modal .link_member, #timesheet-event-update-modal .link_training').toggle(false);
            }

            var $start = $('#timesheet-event-update-modal .time-start-dropdown select');
            var $end   = $('#timesheet-event-update-modal .time-end-dropdown select');

            $start.first().val(event.start.split('T')[1].split(':')[0].replace(/^0/, ''));
            $start.last().val(event.start.split('T')[1].split(':')[1].replace(/^0/, ''));

            $end.first().val(event.end.split('T')[1].split(':')[0].replace(/^0/, ''));
            $end.last().val(event.end.split('T')[1].split(':')[1].replace(/^0/, ''));

            $('#timesheet-event-update-modal .event-notes').val(event.notes);

            $('#timesheet-event-update-modal .link_training').val(event.linkedtraining ? $('#timesheet-event-update-modal .link_training option:contains("' + event.linkedtraining.name + '")').prop('value') : '');
            $('#timesheet-event-update-modal .link_member').val(event.linkedmember ? $('#timesheet-event-update-modal .link_member option:contains("' + event.linkedmember.name + '")').prop('value') : '');
            $('#timesheet-event-update-modal .link_activity').val(event.linkedactivity ? $('#timesheet-event-update-modal .link_activity option:contains("' + event.linkedactivity.name + '")').prop('value') : '');
        });

        $('#timesheet-event-update-modal .modal-body').html($('#timesheet-modal .modal-body').html());

        $('.timesheet-event-update-form').submit(function(e) {
            $.ajax(app_timesheet_event_update_path.replace('EVENT_ID', _active_event_id), {
                type: 'POST',
                data: $('.timesheet-event-update-form').serialize(),
                complete: function(response) {
                    reloadTimesheetData();
                }
            });

            $('#timesheet-event-update-modal').modal('hide');
            e.preventDefault(true);
            return false;
        });
    }

    $(function() {
        if (!$('.timesheet').length) {
            return;
        }

        if (typeof app_timesheet_event_update_path != 'undefined') {
            initTimesheetEventUpdating();
        }

        $('.timesheet .day-add-btn').click(function(e) {
            $('.timesheet_day_of_week').prop('value', $(this).closest('.day').data('dow'));
        });

        if (typeof app_timesheet_comments_path != 'undefined') {
            $('.timesheet-comments').blur(function() {
                $('.timesheet-comments-status').removeClass('hidden').fadeIn();

                $.ajax(app_timesheet_comments_path, {
                    type: 'POST',
                    data: {
                        'comments': $('.timesheet-comments').val()
                    },
                    complete: function(response) {
                        $('.timesheet-comments-status').fadeOut();
                    }
                });
            });
        }

        if (typeof app_timesheet_req_hours_path != 'undefined') {
            $('.timesheet-req_hours').blur(function() {
                $('.timesheet-req_hours-status').removeClass('hidden').fadeIn();

                $.ajax(app_timesheet_req_hours_path, {
                    type: 'POST',
                    data: {
                        'hours': $('.timesheet-req_hours').val()
                    },
                    complete: function(response) {
                        $('.timesheet-req_hours-status').fadeOut();
                        rebuildHours()
                    }
                });
            });
        }

        reloadTimesheetData();

        $('.timesheet-event-form').submit(function(e) {
            $.ajax(app_timesheet_event_create_path, {
                type: 'POST',
                data: $('.timesheet-event-form').serialize(),
                complete: function(response) {
                    reloadTimesheetData();
                }
            });

            $('#timesheet-modal').modal('hide');
            e.preventDefault(true);
            return false;
        });

        if (typeof app_fragment_types != 'undefined') {
            $('.fragment-type').val($('.fragment-type option:first').val());
            reloadFragmentAddons();

            $('.fragment_type').change(function(e) {
                reloadFragmentAddons();
            });
        }
    });

    function rebuildHours(){
        if (typeof app_timesheet_get_hours_path != 'undefined') {
            $.getJSON(app_timesheet_get_hours_path, function(data) {
                $('.overtime-warning-worked').html(data.workedHours);
                if (data.overtime > 0){
                    $('.overtime-warning-overtime').html(data.overtime);
                    $('.overtime-part').show();
                } else {
                    $('.overtime-part').hide();
                }
            })
        }
    }
})(jQuery);
