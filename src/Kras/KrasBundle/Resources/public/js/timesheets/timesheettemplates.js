(function($) {
    function DatetimeToDate(dt) {
        var t = dt.split(/[- :T+]/);
        var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);

        return d;
    }

    function formatTime(secs, format) {
        if (!format) {
            format = '%hh %mm %ss';
        }

        var hours = ("0" + Math.floor(secs / 3600)).slice(-2),
        minutes = ("0" + Math.floor((secs % 3600) / 60)).slice(-2),
        seconds = ("0" + (secs % 60)).slice(-2);

        return format
            .replace('%h', hours)
            .replace('%m', minutes)
            .replace('%s', seconds);
    }

    function reloadTimesheetEvents() {
        $.getJSON(app_timesheettemplate_event_list_path, function(data) {
            $('.timesheettemplate .events').html('');

            $.each(data, function(key, value) {
                var start = DatetimeToDate(value.start);
                var end   = DatetimeToDate(value.end);

                var start_secs = ((start.getHours() * 60) + start.getMinutes()) * 60;
                var end_secs   = ((end.getHours() * 60) + end.getMinutes()) * 60;

                var start_str = formatTime(start_secs, '%h:%m');
                var end_str   = formatTime(end_secs, '%h:%m');

                var diff_seconds = end_secs - start_secs;

                var event_height = Math.round(diff_seconds / 100);

                var day_of_week = start.getDay();
                if (day_of_week < 1) day_of_week = day_of_week + 7;

                var html = '<div class="event" data-event-id="' + value.id + '" style="height:' + event_height + 'px">' +
                    (typeof app_timesheettemplate_event_delete_path != 'undefined' ? '<button class="event-delete-btn btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>' : '') +
                    '<span class="event-start">' + start_str + '</span> \
                    <span class="event-end">' + end_str + '</span> \
                    <div class="padded"> \
                    <h4>[' + (value.timefragmenttype ? value.timefragmenttype.name : 'XXXXXXXX') + ']</h4>'+
                    //(value.notes ? '<h4 class="text-left">' +  value.notes + '</h4>' : '') +
                   '</div> \
                   </div>';

                $('.timesheettemplate .events[data-dow="' + day_of_week + '"]').append(html);
            });

            if (typeof app_timesheettemplate_event_delete_path != 'undefined') {
                rebindEventDeletion();
            }
        });
    }

    function rebindEventDeletion() {
        $('.timesheettemplate .event-delete-btn').unbind('click').click(function(e) {
            $.ajax(app_timesheettemplate_event_delete_path.replace('EVENT_ID', $(this).closest('.event').data('event-id')), {
                type: 'POST',
                complete: function(response) {
                    reloadTimesheetEvents();
                }
            });

            e.preventDefault(true);
            return false;
        });
    }

    $(function() {
        if (!$('.timesheettemplate').length) {
            return;
        }

        $('.timesheettemplate .day-add-btn').click(function(e) {
            $('.timesheet_day_of_week').prop('value', $(this).closest('.day').data('dow'));
        });

        reloadTimesheetEvents();

        $('.timesheettemplate-event-form').submit(function(e) {
            $.ajax(app_timesheettemplate_event_create_path, {
                type: 'POST',
                data: $('.timesheettemplate-event-form').serialize(),
                complete: function(response) {
                    reloadTimesheetEvents();
                }
            });

            $('#timesheettemplate-modal').modal('hide');
            e.preventDefault(true);
            return false;
        });
    });
})(jQuery);
