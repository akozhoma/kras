(function($) {
    function drawChart() {
        _options = {
          title:  app.chart_title,
          is3D:   true,
          legend: 'left'
        };

        _chart = new google.visualization.PieChart(document.getElementById('activity_piechart'));
        refreshChartData();
    }

    function initChart() {
        google.load("visualization", "1.0", {packages:["corechart"], callback: drawChart});
    }

    function refreshChartData() {
        $.ajax(app.chart_data_path, {
            type:    'POST',
            data:    $('#chart-filter-form').serialize(),
            success: function(data) {
                var dataTable = new google.visualization.DataTable();
                dataTable.addColumn('string', 'Activity type');
                dataTable.addColumn('number', 'Seconds spent');
                dataTable.addColumn({type: 'string', role: 'tooltip'});
                dataTable.addRows(data);

                _chart.draw(dataTable, _options);
            }
        });
    }

    $(function() {
        initChart();

        $('#chart-filter-end-date, #chart-filter-start-date').on('changeDate', function(e) {
            refreshChartData();
        });

        $('#chart-filter-clear-btn').click(function(e) {
            $('#chart-filter-end-date, #chart-filter-start-date').removeAttr('value').val('');
            refreshChartData();

            e.preventDefault(true);
            return false;
        });
    });
})(jQuery);
