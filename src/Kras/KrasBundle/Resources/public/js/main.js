(function($){
    // Helper function: centers an element within the window
    function centerElement(el) {
        var $el = $(el);

        $el.css({
            position: 'absolute',
            margin: '0',
            top: (($(window).outerHeight() / 2) - ($el.outerHeight() / 2)) + 'px',
            left: (($(window).outerWidth() / 2) - ($el.outerWidth() / 2)) + 'px'
        });
    }

    function rebindDateTimePickers() {
        $('.datetimepicker').datetimepicker({
            format: 'yyyy/mm/dd hh:ii:00',
            maskInput: true,
            autoclose: true
        });

        $('.datepicker').datetimepicker({
            format: 'yyyy-mm-dd',
            maskInput: true,
            autoclose: true,
            minView:   2
        });

        $('.dateofbirthpicker').datetimepicker({
            format:    'yyyy-mm-dd',
            maskInput: true,
            minView:   2,
            startView: 4
        });

        $('.datetimepicker > .datetimepicker-years + .datetimepicker-inline').remove();
    }

    function rebindChosen() {
        $('.chosen-select').each(function() {
            if ($(this).css('display') != 'none') {
                $(this).chosen({
                    search_contains: true
                });
            }
        });
    }

    function initTimepickers() {
         $('.timepicker').timepicker({
            minuteStep:   15,
            showInputs:   false,
            disableFocus: true
        });
    }

    function initColorPickers() {
        $('.color-picker').colorpicker({
            format: 'hex'
        });
    }

    reloadChosen = rebindChosen;

    function initPostalCodeAutocomplete() {
        // Initialize autofill for postal codes and cities
        if ($('.postalcode-input').length) {
            $.getJSON(app.assetpath + 'kraskras/assets/postalcodes/json/codes.json', function(data) {
                $('.postalcode-input').keyup(function(e) {
                    var val = $(this).val();
                    if (data[val]) {
                        $('.city-input').val(data[val]);
                    }
                });
            });
        }
    }

    function activateSectionFilter() {
        var selected = $('.section-filter').closest('form').data('selected');

        if (typeof selected != 'undefined') {
            $('.section-filter').val(selected);
        }

        $('.section-filter').change(function(e) {
            $(this).closest('form').submit();
        });
    }

    // Initializes timeago
    function loadTimeago() {
        $.timeago.settings.allowFuture = true;
        $("abbr.timeago").timeago();
    }

    function initCollectionRebinds() {
        $('.dynamic-collection-item-add, .dynamic-collection-item-delete').click(function(e) {
            rebindDateTimePickers();
        });
    }

    function initTooltips() {
        $('[data-provide="tooltip"]').tooltip();
    }

    function parseDate(input) {
        var parts = input.split('-');
        // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
        return new Date(parts[0], parts[1]-1, parts[2]); // Note: months are 0-based
    }

    $(function(){
        // Determine what page we're on
        app.page = typeof app.page != 'undefined' ? app.page : 'any';

        initTooltips();
        rebindDateTimePickers();
        loadTimeago();
        rebindChosen();
        initPostalCodeAutocomplete();
        initTimepickers();
        initColorPickers();
        activateSectionFilter();
        initCollectionRebinds();

        if(app.page == 'members/show' || app.page == 'mymembers/show') {
            $('.logentry-create-btn').click(function(e) {
                if($(this).hasClass('disabled')) return;

                if($('.logentry-title').val() != '') {
                    $(this).addClass('disabled');

                    var data = $('.logentry-form').serialize();

                    if (app.page == 'mymembers/show') {
                        data += '&member_id=' + app.member_id;
                    }

                    $.ajax({
                        type: "POST",
                        url: $('.logentry-form').attr('action'),
                        data: data,
                        success: function(data) {
                            document.location.reload();
                        },
                        error: function() {
                            $('.logentry-success').fadeOut();
                            $('.logentry-error').fadeIn();
                        },
                        complete: function() {
                            $('.logentry-create-btn').removeClass('disabled');
                        }
                    });
                }else{
                    $('.logentry-success').fadeOut();
                    $('.logentry-error').fadeIn();
                }
            });
        }

        if(app.page == 'members/show' || app.page == 'mymembers/show' || app.page == 'logentries/new') {
            $.getJSON(app.title_list_path, function(data) {
                $('.logentry-title').typeahead({
                    name:  'logentry-title',
                    local: data
                });
            });
        }

        if(app.page == 'activities/new' || app.page == 'myactivities/new') {
                // Helper function for adding another input field
                function addDateInput(){
                    var $container_starts = $('.activity-starts');
                    var $container_ends = $('.activity-ends');
                    var $container_actions = $('.activity-actions');

                    // Get the template
                    var widget_start = $container_starts.attr('data-prototype');
                    var widget_end = $container_ends.attr('data-prototype');
                    var widget_actions = $container_actions.attr('data-prototype');

                    widget_start = widget_start.replace(/__name__/g, app.count_starts);
                    widget_end = widget_end.replace(/__name__/g, app.count_ends);

                    app.count_starts++;
                    app.count_ends++;

                    // Add the new input
                    $(widget_start).addClass('dt-start-'+app.count_starts).appendTo($container_starts);
                    $(widget_end).addClass('dt-end-'+app.count_ends).appendTo($container_ends);
                    $(widget_actions).appendTo($container_actions);

                    // Bind on date change
                    $('.dt-start-'+app.count_starts + ' input').on('changeDate', function(e) {
                        $('.dt-end-'+app.count_ends + ' input').val($(this).val());
                    });

                    // Rebind stuff
                    rebindDateInput();
                }

                function rebindDateInput() {
                    // Rebind the removing of inputs
                    rebindRemoveDateInput();

                    // Rebind the creating of inputs
                    rebindCreateDateInput();

                    // Rebind datetimepickers
                    rebindDateTimePickers();
                }

                function rebindRemoveDateInput() {
                    $('.activity-date-remove').unbind('click').click(function(e) {
                        var index = $(this).parent().parent().index() + 1;
                        $('.activity-starts > div:nth-child('+index+')').remove();
                        $('.activity-ends > div:nth-child('+index+')').remove();
                        $('.activity-actions > div:nth-child('+index+')').remove();
                    });
                }

                function rebindCreateDateInput() {
                    $('.activity-date-add').unbind('click').click(addDateInput);
                }

                // If there were no inputs yet, add one now
                if(app.count_starts == 0 || app.count_ends == 0) {
                    addDateInput();
                }else {
                    // Even if we don't need to add new inputs, perform bindings
                    rebindDateInput();
                }
        }

        // Member adding/editing page-specific
        if(app.page == 'members/new' || app.page == 'members/edit' || app.page == 'mymembers/new' || app.page == 'mymembers/edit') {

            var typeahead_cache = [];
            var typeahead_cache_reverse = [];
            var typeahead_cache_first = [];
            var typeahead_cache_last = [];

            // Initialize typeahead for firstnames and lastnames
            $.getJSON(app.name_list_path, function(data) {
                $.each(data, function(key, value) {
                    if($.inArray(value.firstname, typeahead_cache_first) == -1) {
                        typeahead_cache_first.push(value.firstname);
                    }
                    if($.inArray(value.lastname, typeahead_cache_last) == -1) {
                        typeahead_cache_last.push(value.lastname);
                    }
                    if($.inArray(value.lastname, typeahead_cache[value.firstname]) == -1) {
                        if(typeof typeahead_cache[value.firstname] == 'undefined') {
                            typeahead_cache[value.firstname] = [];
                        }
                        typeahead_cache[value.firstname].push(value.lastname);
                    }
                    if($.inArray(value.firstname, typeahead_cache_reverse[value.lastname]) == -1) {
                        if(typeof typeahead_cache_reverse[value.lastname] == 'undefined') {
                            typeahead_cache_reverse[value.lastname] = [];
                        }
                        typeahead_cache_reverse[value.lastname].push(value.firstname);
                    }
                });

                $('.member-firstname').typeahead({
                    name:  'firstname',
                    local: typeahead_cache_first
                });

                $('.member-lastname').typeahead({
                    name:  'lastname',
                    local: typeahead_cache_last
                });

                // Filter the lastname typeahead when the firstname input value is changed
                $(".member-firstname").bind("keyup paste", function() {
                    var value = $(this).val();
                    if(value == '') {
                        $('.member-lastname').typeahead('destroy');
                        $('.member-lastname').typeahead({
                            name:   'lastname',
                            source: typeahead_cache_last
                        });
                    }else if($.inArray(value, typeahead_cache_first) > -1) {
                        $('.member-lastname').typeahead('destroy');
                        $('.member-lastname').typeahead({
                            name:   'lastname',
                            source: typeahead_cache['value']
                        });
                    }else{
                        $('.member-lastname').typeahead('destroy');
                    }
                });

                $(".member-lastname").bind("keyup paste", function() {
                    var value = $(this).val();
                    if(value == '') {
                        $('.member-firstname').typeahead('destroy');
                        $('.member-firstname').typeahead({
                            name:   'firstname',
                            source: typeahead_cache_first
                        });
                    } else if($.inArray(value, typeahead_cache_last) > -1) {
                        $('.member-firstname').typeahead('destroy');
                        $('.member-firstname').typeahead({
                            name:   'firstname',
                            source: typeahead_cache_reverse['value']
                        });
                    }else{
                        $('.member-firstname').typeahead('destroy');
                    }
                });
            });
        }

        if (app.page == 'members/map' || app.page == 'mymembers/map' || app.page == 'activities/map' || app.page == 'myactivities/map') {
            function resizeMapCanvas() {
                // Remove 60 extra px (padding / margin)
                $("#map-canvas").height($(window).height() - $("#map-canvas").offset().top - 60);
            }

            function addMemberMarkerByAddress(obj) {
                if (obj.section_id) {
                    addMarker(obj, markers_members_sections[obj.section_id], section_icons[obj.section_id]);
                } else {
                    addMarker(obj, markers_members, null);
                }
            }

            function addSupportCenterMarkerByAddress(obj) {
                addMarker(obj, markers_supportcenters, altImage1);
            }

            function addActivityMarkerByAddress(obj) {
                addMarker(obj, markers_activities, altImage2);
            }

            function addMarker(obj, container, icon) {
                geocoder.geocode({
                    'address': obj.address + ', ' + obj.postalcode + ' ' + obj.city
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var loc = results[0].geometry.location;
                        if (container.length == 0) {
                            map.setCenter(loc);
                        }
                        var marker = new google.maps.Marker({
                            data: obj,
                            position: loc,
                            map: map,
                            icon: icon
                            /*,shadow: altShadow*/
                        });

                        google.maps.event.addListener(marker, 'click', function(e) {
                            map.setCenter(loc);
                            onMarkerClick(e, marker);
                        });

                        container.push(marker);
                    }
                    return null;
                });
            }

            function onMarkerClick(event, marker) {
                var content = '';
                if (marker.data) {
                    if (marker.data.name) {
                        content += '<strong>' + marker.data.name + '</strong><br>';
                    }
                    if (marker.data.firstname && marker.data.lastname) {
                        content += '<strong>' + marker.data.firstname + ' ' + marker.data.lastname + '</strong><br>';
                    }
                    if (marker.data.description) {
                        content += '<em>' + marker.data.description + '</em><br>';
                    }
                    content += marker.data.address + '<br>' + marker.data.postalcode + ' ' + marker.data.city;
                }

                // Replace our Info Window's content and position
                infowindow.setContent(content);
                infowindow.setPosition(marker.position);
                infowindow.open(map);
            }

            // Store section markers and images here
            var markers_members_sections = [];
            var section_icons = [];

            // Loop through sections and assign a color
            $('.section-checkbox').each(function() {
                // Check checkbox
                $(this).prop('checked', true);
                // Init container
                markers_members_sections[$(this).data('section')] = [];
                // Generate color
                var color = $(this).data('color');
                // Set color
                $(this).closest('span').css('background-color', '#' + color);
                // Generate a new icon with this color
                var icon = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + color,
                new google.maps.Size(21, 34),
                new google.maps.Point(0,0),
                new google.maps.Point(10, 34));
                section_icons[$(this).data('section')] = icon;
            })
            // Bind the change event to toggle markers of events for certain sections
            .change(function(e) {
                infowindow.close();
                $.each(markers_members_sections[$(this).data('section')], function(key, val) {
                    val.setVisible(!val.visible);
                });
            });

            var $canvas = $("#map-canvas");
            var markers_members = markers_supportcenters = markers_activities = [];

            // Size map container
            resizeMapCanvas();
            $(window).resize(resizeMapCanvas);

            // Load google maps
            var mapOptions = {
              zoom: 13,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map-canvas"),
                mapOptions);

            var geocoder = new google.maps.Geocoder();

            var infowindow = new google.maps.InfoWindow();

            // Create an alternate marker image for supportcenters
            var altColor1 = "1693A5";
            var altImage1 = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + altColor1,
                new google.maps.Size(21, 34),
                new google.maps.Point(0,0),
                new google.maps.Point(10, 34));
            /*var altShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
                new google.maps.Size(40, 37),
                new google.maps.Point(0, 0),
                new google.maps.Point(12, 35));*/
            var altColor2 = "FBB829";
            var altImage2 = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + altColor2,
                new google.maps.Size(21, 34),
                new google.maps.Point(0,0),
                new google.maps.Point(10, 34));

            $.getJSON(app.address_list_path, function(data) {
                if (data.members) {
                    $.each(data.members, function(key, val) {
                        addMemberMarkerByAddress(val);
                    });
                }
                if (data.supportcenters) {
                    $.each(data.supportcenters, function(key, val) {
                        addSupportCenterMarkerByAddress(val);
                    })
                }
                if (data.supportcenter) {
                    addSupportCenterMarkerByAddress(data.supportcenter);
                }
                if (data.activity) {
                    addActivityMarkerByAddress(data.activity);
                }
            });
        }

        if ($.inArray(app.page, ['myleave/new', 'myleave/edit', 'leave/new', 'leave/edit']) > -1) {
            function handleLeaveTypeChange() {
                var selected_value = $('.leave-type').find('option:selected').text();
                var selected_type = null;

                for (var i = 0; i < app.leavetypes.length; i++) {
                    if (app.leavetypes[i].name == selected_value) {
                        selected_type = app.leavetypes[i];
                        break;
                    }
                }

                $('.time-start-dropdown, .time-end-dropdown').closest('.form-group').toggle(!selected_type.sickness && !selected_type.public_holiday);
            }

            handleLeaveTypeChange();
            $('.leave-type').change(handleLeaveTypeChange);

            $('.dynamic-collection-item-add').click(function() {
                handleLeaveTypeChange();

                var old_selects = $('.dynamic-collection-item:eq(-2) .time-dropdown select'),
                    new_selects = $('.dynamic-collection-item:eq(-1) .time-dropdown select');

                for (var i = 0; i < old_selects.length; i++) {
                    new_selects[i].value = old_selects[i].value;
                }
            });
        }

        if (app.page == 'mydashboard') {
            var graph_data = [];

            function drawGraph(key) {
                nv.addGraph(function() {
                    var chart = nv.models.linePlusBarChart()
                        .margin({ top: 30, right: 30, bottom: 30, left: 30 })
                        .x(function(d,i) { return i; })
                        .y(function(d) { return d[1]; })
                        .color(d3.scale.category10().range());

                    chart.xAxis
                      .showMaxMin(false)
                      .tickFormat(function(d) {
                        return graph_data[key][0].values[d][0];
                    });

                    chart.bars.forceY([0]);

                   // nv.utils.windowResize(chart.update);

                    $(window).resize(function() {
                        d3.select('.section-' + key + ' svg').datum(graph_data[key]).transition().duration(500).call(chart);
                    });

                    d3.select('.section-' + key + ' svg').datum(graph_data[key]).transition().duration(500).call(chart);

                    return chart;
                });
            }

            $.getJSON(app.data_path, function(data) {
                $.each(data, function(key, val) {
                    graph_data[key] = [{
                        'key': 'Average attendees',
                        'bar': true,
                        'values': $.map(val, function(n, i) {
                            return [[ i, n.average_attendance ]];
                        })
                    },
                    {
                        'key': 'Activity count',
                        'values': $.map(val, function(n, i) {
                            return [[ i, n.activity_count ]];
                        })
                    }];
                });
            });

            $('.graph-block-toggle').click(function(e) {
                for (key in graph_data) {
                    drawGraph(key);
                }
            });
        }
    });
})(jQuery);
