<?php
namespace Kras\KrasBundle\DoctrineExtensions\Query;

use Doctrine\ORM\Query\Lexer;

/**
 * TimeStampDiffFunction ::= "TIMESTAMPDIFF" "(" ArithmeticPrimary "," ArithmeticPrimary ")"
 */
class TimeStampDiff extends \Doctrine\ORM\Query\AST\Functions\FunctionNode
{
    // (1)
    public $firstDateTimeExpression = null;
    public $secondDateTimeExpression = null;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER); // (2)
        $parser->match(Lexer::T_OPEN_PARENTHESIS); // (3)
        $this->firstDateTimeExpression = $parser->ArithmeticPrimary(); // (4)
        $parser->match(Lexer::T_COMMA); // (5)
        $this->secondDateTimeExpression = $parser->ArithmeticPrimary(); // (6)
        $parser->match(Lexer::T_CLOSE_PARENTHESIS); // (3)
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'TIMESTAMPDIFF(SECOND, ' .
            $this->firstDateTimeExpression->dispatch($sqlWalker) . ', ' .
            $this->secondDateTimeExpression->dispatch($sqlWalker) .
        ')'; // (7)
    }
}
