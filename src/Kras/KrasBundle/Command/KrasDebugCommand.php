<?php
namespace Kras\KrasBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Input\InputArgument;

class KrasDebugCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('kras:debug')
            ->setDescription('Perform various debugging-related tasks.')
            ->addArgument(
                'type',
                InputArgument::REQUIRED,
                'What sort of debugging task do you want to perform?'
            )
            ->addArgument(
                'id',
                InputArgument::OPTIONAL,
                'ID of the object you want to perform a debugging task on.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $employeeService = $this->getContainer()->get('kras.employee');

        $id = intval($input->getArgument('id'));

        switch ($input->getArgument('type')) {
            case 'calculate_overtime':
                $user = $em->getRepository('KrasUserBundle:User')->find($id);

                if (!$user) {
                    $output->writeln(sprintf('<error>No user found with id "%s"</error>', $id));
                    return;
                }

                $employeeService->setEmployeeOvertime($user);

                $output->writeln(sprintf('<info>ID: %s</info>', $user->getId()));
                $output->writeln(sprintf('<info>Remaining overtime hours: %s</info>', $user->getOvertime()));

                break;

            case 'process_timesheet':
                $timesheet = $em->getRepository('KrasKrasBundle:Timesheet')->find($id);

                if (!$timesheet) {
                    $output->writeln(sprintf('<error>No timesheet found with id "%s"</error>', $id));
                    return;
                }

                foreach ($timesheet->getTimesheetTemplate()->getExtendedTimeFragments() as $fragment) {
                    $fragment->setStart($fragment->getStart())->setEnd($fragment->getEnd());
                }

                $em->flush();

                $timesheet->getTimesheetTemplate()->calculateRequiredHours();
                $em->flush();

                $timesheet->setRequiredHours($timesheet->getTimesheetTemplate()->getRequiredHours());
                $em->flush();

                foreach ($timesheet->getExtendedTimeFragments() as $fragment) {
                    $fragment->setStart($fragment->getStart())->setEnd($fragment->getEnd());
                }

                $em->flush();
                $employeeService->recalculateTimesheetWorkedHours($timesheet);

                $output->writeln(sprintf('<info>ID: %s</info>', $timesheet->getId()));
                $output->writeln(sprintf('<info>Worked hours: %s</info>', $timesheet->getWorkedHours()));
                $output->writeln(sprintf('<info>Required hours: %s</info>', $timesheet->getRequiredHours()));

                break;

            default:
                $output->writeln('<error>Command not recognized.</error>');
        }
    }
}
