<?php
namespace Kras\KrasBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\StringInput;
use Kras\KrasBundle\Entity\Timesheet;

class CreateMissingTimesheetsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('missingtimesheets:create')
            ->setDescription('Creates missing timesheets for employees.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<comment>Creating missing timesheets..</comment>');
        $output->writeln('');

        $kernel = $this->getContainer()->get('kernel');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->employeeService = $this->getContainer()->get('kras.employee');

        $dql = 'SELECT u FROM KrasUserBundle:User u';
        $query = $em->createQuery($dql);
        $users = $query->getResult();

        //$begin = new \DateTime('-1 week');
        $begin = new \DateTime();

        foreach ($users as $user) {
            $user_begin = clone $begin;
            $user_end   = clone ($user->getStartedWorking());

            $week_count = floor($user_begin->diff($user_end)->days / 7);

            // If $user has less than $week_count timesheets, some are missing
            $dql = 'SELECT count(t.id) FROM KrasKrasBundle:Timesheet t
                WHERE t.employee = ?1';
            $query = $em->createQuery($dql);
            $query->setParameter(1, $user);

            $timesheet_count = $query->getSingleScalarResult();

            $output->writeln(sprintf(
                '<info>%s</info> timesheets found for user <info>%s</info>. (<info>%s</info> expected)',
                $timesheet_count,
                $user,
                $week_count
            ));

            if ($timesheet_count < $week_count) {
                $output->writeln('    <info>Creating missing timesheets..</info>');

                $this->createMissingTimesheets($user);
            }
        }

        $em->flush();

        $output->writeln('');
        $output->writeln('<comment>Done!</comment>');
    }

    private function createMissingTimesheets($user)
    {
        while ($this->employeeService->getFirstNonExistantTimesheet($user) != null) {}
    }
}
