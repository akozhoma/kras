<?php
namespace Kras\KrasBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Kras\KrasBundle\Entity\EntityMapping;
use Kras\UserBundle\Entity\User;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

class DefaultsEnsureCommand extends ContainerAwareCommand
{
    private $cfg;

    public function __construct()
    {
        parent::__construct();
        $this->cfg = array(
            'namespace_prefix' => 'Kras\KrasBundle\Entity\\',
            'bundle_prefix' => 'KrasKrasBundle:',
        );
    }

    protected function configure()
    {
        $this
          ->setName('defaults:ensure')
          ->setDescription('Ensures the existence of default data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $output->writeln('Ensuring existence of default data');

        // Add root user if needed
        $user = $this->em->createQueryBuilder()
            ->select('u')
            ->from('KrasUserBundle:User', 'u')
            ->where('u.roles LIKE :role')
            ->andWhere('u.enabled = 1')
            ->setParameter('role', '%ROLE_SUPER_ADMIN%')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
        if (!$user) {
            $username = 'admin';
            $password = 'password';
            $user = new User();
            $user
                ->setUsername($username)
                ->addRole('ROLE_SUPER_ADMIN')
                ->setEnabled(1)
                ->setEmail('example.admin@inuits.eu')
                ->setPlainPassword($password);
            $this->em->persist($user);
            $this->em->flush();
            $output->writeln(sprintf('Created a User with credentials <comment>%s</comment>:<comment>%s</comment>', $username, $password));
        }

        $content = array();

        // Add in Timesheet Templates
        $timesheetTemplates = array(
            array(0, 'Default'),
        );

        foreach ($timesheetTemplates as $timesheetTemplate) {
            $content["TimesheetTemplate"][$timesheetTemplate[0]] = array(
                "Name"          => $timesheetTemplate[1],
                "RequiredHours" => 40,
                "Approved"      => TRUE,
            );
        }

        $dt_1  = new \DateTime('2014-01-27 08:00:00');
        $dt_2  = new \DateTime('2014-01-27 12:00:00');
        $dt_3  = new \DateTime('2014-01-27 12:30:00');
        $dt_4  = new \DateTime('2014-01-27 16:30:00');
        $dt_5  = new \DateTime('2014-01-28 08:00:00');
        $dt_6  = new \DateTime('2014-01-28 12:00:00');
        $dt_7  = new \DateTime('2014-01-28 12:30:00');
        $dt_8  = new \DateTime('2014-01-28 16:30:00');
        $dt_9  = new \DateTime('2014-01-29 08:00:00');
        $dt_10 = new \DateTime('2014-01-29 12:00:00');
        $dt_11 = new \DateTime('2014-01-29 12:30:00');
        $dt_12 = new \DateTime('2014-01-29 16:30:00');
        $dt_13 = new \DateTime('2014-01-30 08:00:00');
        $dt_14 = new \DateTime('2014-01-30 12:00:00');
        $dt_15 = new \DateTime('2014-01-30 12:30:00');
        $dt_16 = new \DateTime('2014-01-30 16:30:00');
        $dt_17 = new \DateTime('2014-01-31 08:00:00');
        $dt_18 = new \DateTime('2014-01-31 12:00:00');
        $dt_19 = new \DateTime('2014-01-31 12:30:00');
        $dt_20 = new \DateTime('2014-01-31 16:30:00');

        // Add in Extended Time Fragments
        $extendedTimeFragments = array(
            array(0, 0, $dt_1, $dt_2),
            array(1, 0, $dt_3, $dt_4),
            array(2, 0, $dt_5, $dt_6),
            array(3, 0, $dt_7, $dt_8),
            array(4, 0, $dt_9, $dt_10),
            array(5, 0, $dt_11, $dt_12),
            array(6, 0, $dt_13, $dt_14),
            array(7, 0, $dt_15, $dt_16),
            array(8, 0, $dt_17, $dt_18),
            array(9, 0, $dt_19, $dt_20),
        );

        foreach ($extendedTimeFragments as $extendedTimeFragment) {
            $content["ExtendedTimeFragment"][$extendedTimeFragment[0]] = array(
                "TimesheetTemplate" => array(
                    "TimesheetTemplate" => array($extendedTimeFragment[1]),
                ),
                "Start"             => $extendedTimeFragment[2],
                "End"               => $extendedTimeFragment[3],
            );
        }

        // Load all mapper entities from the DB
        $dql     = 'SELECT em FROM KrasKrasBundle:EntityMapping em';
        $query   = $this->em->createQuery($dql);
        $mappers = $query->getArrayResult();

        // Loop through mapper entities and index them by entity name & source ID
        $mapped_entities = array();

        foreach ($mappers as $mapper) {
            $mapped_entities[$mapper['entity']][$mapper['source']] = $mapper['destination'];
        }

        // Clone the mapped entities index to keep track of what should be deleted in the end
        $deletable_entities = $mapped_entities;

        // Loop through the content we have
        foreach ($content as $entity_name => $entries) {

            // Get the full name of the class we're working with
            $class_name = $this->cfg['namespace_prefix'].$entity_name;

            // Get the full entity name of the class we're working with (Symfony style)
            $full_entity_name = $this->cfg['bundle_prefix'].$entity_name;

            // Loop through entries of the entity we'll need to add, grab the custom index we'll use as the mapper source ID
            foreach ($entries as $source_id => $entry) {

                $entity = null;

                // Try to find a live database entry for this entity, with the same source ID
                // If we can't find one, create a new instance of the entity
                if (array_key_exists($entity_name, $mapped_entities)
                    && array_key_exists($source_id, $mapped_entities[$entity_name])) {

                    // Grab the destination ID
                    $destination_id = $mapped_entities[$entity_name][$source_id];

                    // Try to find a live database entry with that destination ID
                    $entity = $this->em->getRepository($full_entity_name)->find($destination_id);

                }

                // If a live database entry wasn't found, create a new instance
                if (!$entity) {
                    $entity = new $class_name;
                }

                // Loop through field names and values for our object, and set them
                foreach ($entry as $field_name => $field_value) {

                    // If the value is an associative array, the field value should be set to the entities within its contents
                    // Array format: "<Master entity field name>" => array("<Slave entity name>" => array("<Source ID of the object"))
                    if (is_array($field_value) && $this->isAssocArray($field_value)) {

                        // If we filter out all null values and nothing remains, just skip this field
                        if (count(array_filter($field_value, function($var) {
                            return count(array_filter($var, function($v) {
                                return !is_null($v);
                            })) != 0;
                        })) == 0) {
                            continue;
                        }

                        $slave_entity_name = array_keys($field_value);
                        $slave_entity_name = $slave_entity_name[0];

                        // Get the full entity name of the slave class we're working with (Symfony style)
                        $full_slave_entity_name = $this->cfg['bundle_prefix'].$slave_entity_name;

                        $slave_entity_source_ids = reset($field_value);

                        // If we only have one source ID we'll use a setter, if we have multiple we'll use an adder
                        $slave_entity_function_prefix = count($slave_entity_source_ids) > 1 ? 'add' : 'set';

                        // Loop through the slave entity source IDs and attempt to actually fetch a live database entry
                        // If we find an entity, also immediately inject this value into its master
                        foreach($slave_entity_source_ids as $slave_entity_source_id){

                            // See if we can find a destination ID based on this source ID
                            $slave_entity_destination_id = $mapped_entities[$slave_entity_name][$slave_entity_source_id];

                            // If no destination ID was found, skip this entry
                            if (!$slave_entity_destination_id) {
                                continue;
                            }

                            // Try to fetch a live database entry based on the destination ID
                            $slave_entity = $this->em->getRepository($full_slave_entity_name)->find($slave_entity_destination_id);

                            // If no live database entry was found, ship this entry
                            if (!$slave_entity) {
                                continue;
                            }

                            // Call function on master entity (format: "set<PropertyName>")
                            call_user_func(array(&$entity, $slave_entity_function_prefix.$field_name), $slave_entity);

                        }

                    } else {
                        // If the field value is not an associative array, just set it
                        call_user_func(array(&$entity, 'set'.$field_name), $field_value);
                    }

                }

                // Check whether we're creating a new entity; we'll need this for later
                $entity_is_new = $entity->getId() == null;

                // Persist entity and flush immediately so we can store a destination ID in the mapper array if needed
                $this->em->persist($entity);
                $this->em->flush();

                // If this entity is new, we should add a mapper entry for it
                if ($entity_is_new) {

                    $destination_id = $entity->getId();

                    // Update the destination ID in the mapper array because we're cool like that
                    $mapped_entities[$entity_name][$source_id] = $destination_id;

                    $mapper = new EntityMapping();
                    $mapper
                        ->setEntity($entity_name)
                        ->setSource($source_id)
                        ->setDestination($destination_id);

                    $this->em->persist($mapper);
                    $this->em->flush();

                }

                // Remove this entity from the array of deletable entities
                unset($deletable_entities[$entity_name][$source_id]);

            }

        }

        // Loop through what's left in our array of deletable entities and.. delete them
        foreach ($deletable_entities as $entity_name => $entries) {

            // Grab the full entity name (Symfony style)
            $full_entity_name = $this->cfg['bundle_prefix'].$entity_name;

            foreach ($entries as $source_id => $destination_id) {

                // Try to find the entity we're to delete
                $entity = $this->em->getRepository($full_entity_name)->find($destination_id);

                // If the entity exists, delete it
                if ($entity) {
                    $this->em->remove($entity);
                }

                // Also try to find the mapper entity linked to this entity
                $entity = $this->em->getRepository('EDCSharedBundle:EntityMapping')->findOneBy(array(
                    'entity' => $entity_name,
                    'source' => $source_id,
                ));

                // If the mapper entity exists, delete it
                if ($entity) {
                    $this->em->remove($entity);
                }

            }

            // Flush changes to the database
            $this->em->flush();

        }


        // Extra post-install stuff
        $this->em->clear();

        $defaultTimesheetTemplates = $this->em->createQuery('SELECT tt FROM KrasKrasBundle:TimesheetTemplate tt WHERE tt.employee IS NULL')->getResult();

        foreach ($defaultTimesheetTemplates as $defaultTimesheetTemplate) {
            $defaultTimesheetTemplate->calculateRequiredHours();
            $this->em->persist($defaultTimesheetTemplate);
        }

        $this->em->flush();

        $output->writeln('Completed successfully.');
    }

    private function isAssocArray($array)
    {
        return array_keys($array) !== range(0, count($array) - 1);
    }
}
