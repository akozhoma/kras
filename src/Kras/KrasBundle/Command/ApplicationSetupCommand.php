<?php
namespace Kras\KrasBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\StringInput;

class ApplicationSetupCommand extends ContainerAwareCommand
{
    private $output;

    protected function configure()
    {
        $this
            ->setName('app:setup')
            ->setDescription('Configures the Application')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<comment>Setting up the application..</comment>');
        $output->writeln('');

        $this->output = $output;
        $kernel = $this->getContainer()->get('kernel');

        $output->writeln('<info>Updating database schema:</info>');
        $this->runCommand('doctrine:schema:update --force');

        $output->writeln('<info>Installing assets to \'web/\' using symlinks:</info>');
        $this->runCommand('assets:install --symlink web');

        $output->writeln(sprintf('<info>Clearing cache for the \'%s\' environment:</info>', $kernel->getEnvironment()));
        $this->runCommand('cache:clear');

        $output->writeln(sprintf('<info>Dumping assets for the \'%s\' environment:</info>', $kernel->getEnvironment()));
        $this->runCommand('assetic:dump');

        $output->writeln('<info>Ensuring existence of default data:</info>');
        $this->runCommand('defaults:ensure');

        $output->writeln('<comment>Done!</comment>');
    }

    private function runCommand($string)
    {
        // Split namespace and arguments
        $namespace = explode(' ', $string);
        $namespace = $namespace[0];

        // Set input
        $command = $this->getApplication()->find($namespace);
        $input = new StringInput($string);

        // Send all output to the console
        $returnCode = $command->run($input, $this->output);

        $this->output->writeln('');

        return $returnCode != 0;
    }
}
