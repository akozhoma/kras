<?php
namespace Kras\KrasBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\StringInput;
use Doctrine\Common\Collections\ArrayCollection;
use Kras\KrasBundle\Entity\Timesheet;
use Symfony\Component\Validator\Constraints\DateTime;

class FixTimesheetsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fixtimesheets:fix')
            ->setDescription('Fix timesheets for employees.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<comment>Fix timesheets..</comment>');
        $output->writeln('');


        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $timesheets = $em->getRepository('KrasKrasBundle:Timesheet')->findAll();

        foreach ($timesheets as $timesheet) {

            $date = $timesheet->getStartDate();

            $oldWeekNumber = $timesheet->getWeek();
            $timesheet->setWeek($date->format("W"));

            if($oldWeekNumber != $timesheet->getWeek()){
                $output->writeln($timesheet->getId() ."  ". $oldWeekNumber . " > " . $timesheet->getWeek() );

            }

            $fragments = $timesheet->getExtendedTimeFragments();

            foreach($fragments as $fragment){

                if($timesheet->getWeek() !== $fragment->getStart()->format("W")){

                    $fragment->setStart($fragment->getStart()->modify("-1 week"));
                    $fragment->setEnd($fragment->getEnd()->modify("-1 week"));
                    $q = $em->createQueryBuilder()
                        ->update('KrasKrasBundle:ExtendedTimeFragment', 'etf')
                        ->set('etf.start', "?1" )
                        ->set('etf.end', "?2")
                        ->where('etf.id = ?3')
                        ->setParameter(1, $fragment->getStart()->format('Y-m-d H:i:s'))
                        ->setParameter(2,  $fragment->getEnd()->format('Y-m-d H:i:s'))
                        ->setParameter(3, $fragment->getId())
                        ->getQuery();
                    $q->execute();

                    $output->writeln("upd time fragment");
                }


            }

            $em->persist($timesheet);

        }
        $em->flush();
        $output->writeln('Done!');

    }
}
