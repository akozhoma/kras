<?php
namespace Kras\KrasBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\StringInput;
use Doctrine\Common\Collections\ArrayCollection;
use Kras\KrasBundle\Entity\UserLeave;
use Symfony\Component\Validator\Constraints\DateTime;

class FixGenericCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fixgeneric:run')
            ->setDescription('Run some generic fixes (mostly db).');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<comment>Fixing..</comment>');
        $output->writeln('');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $employeeService = $this->getContainer()->get('kras.employee');

        $output->writeln('Fixing leaves..');
        $entities = $em->getRepository('KrasKrasBundle:UserLeave')->findAll();
        foreach ($entities as $leave) {
            $leave->setDurationValue();

            $em->persist($leave);
            $em->flush();
        }

        $output->writeln('Fixing timesheets..');
        $entities = $em->getRepository('KrasKrasBundle:Timesheet')->findAll();
        $employeeService->recalculateTimesheetsWorkedHours($entities);

        $output->writeln('Fixing user overtime counts..');
        $entities = $em->getRepository('KrasUserBundle:User')->findAll();
        foreach ($entities as $user) {
            $employeeService->setEmployeeOvertime($user);
        }

        $em->flush();
        $output->writeln('Done!');
    }
}
