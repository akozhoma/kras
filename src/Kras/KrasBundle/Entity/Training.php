<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Training
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Expose
     */
    private $name;

    /**
     * @ORM\Column(type="float", precision=2)
     * @Expose
     */
    private $price;

    /**
     * @ORM\Column(type="string")
     * @Expose
     */
    private $organizer;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $approved;

    /**
     * @ORM\OneToMany(targetEntity="ExtendedTimeFragment", mappedBy="linkedtraining", orphanRemoval=true)
     */
    private $extendedtimefragments;

    /**
     * @ORM\ManyToOne(targetEntity="Kras\UserBundle\Entity\User", inversedBy="trainings")
     */
    private $employee;

    /**
     * @ORM\OneToMany(targetEntity="TrainingDate", mappedBy="training", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $trainingDates;

    public function __construct()
    {
        $this->price = 0.00;
        $this->extendedtimefragments = new ArrayCollection();
        $this->approved = null;
        $this->trainingDates = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->organizer.' - '.$this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function getOrganizer()
    {
        return $this->organizer;
    }

    public function setOrganizer($organizer)
    {
        $this->organizer = $organizer;
        return $this;
    }

    public function getEmployee()
    {
        return $this->employee;
    }

    public function setEmployee($employee)
    {
        $this->employee = $employee;
        return $this;
    }

    public function getApproved()
    {
        return $this->approved;
    }

    public function setApproved($approved)
    {
        $this->approved = $approved;
        return $this;
    }

    public function getExtendedTimeFragments()
    {
        return $this->extendedtimefragments;
    }

    public function setExtendedTimeFragments($extendedtimefragments)
    {
        $this->extendedtimefragments = $extendedtimefragments;
        return $this;
    }

    public function addExtendedTimeFragment($extendedtimefragment)
    {
        if (!$this->extendedtimefragments->contains($extendedtimefragment)) {
            $this->extendedtimefragments[] = $extendedtimefragment;
        }
        return $this;
    }

    public function removeExtendedTimeFragment($extendedtimefragment)
    {
        $this->extendedtimefragments->remove($extendedtimefragment);
        return $this;
    }

    public function hasExtendedTimeFragment($extendedtimefragment)
    {
        return $this->extendedtimefragments->contains($extendedtimefragment);
    }

    public function getTrainingDates()
    {
        return $this->trainingDates;
    }

    public function setTrainingDates($trainingDates)
    {
        $this->trainingDates = $trainingDates;
        return $this;
    }

    public function addTrainingDate($trainingDate)
    {
        if (!$this->trainingDates->contains($trainingDate)) {
            $trainingDate->setTraining($this);
            $this->trainingDates[] = $trainingDate;
        }
        return $this;
    }

    public function removeTrainingDate($trainingDate)
    {
        $this->trainingDates->removeElement($trainingDate);
        return $this;
    }

    public function hasTrainingDate($trainingDate)
    {
        return $this->trainingDates->contains($trainingDate);
    }
}
