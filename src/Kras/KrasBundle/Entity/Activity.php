<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Activity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Expose
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="Kras\UserBundle\Entity\User", inversedBy="activities", cascade="persist")
     */
    private $employees;

    /**
     * @ORM\ManyToMany(targetEntity="Member", inversedBy="volunteered", cascade="persist")
     */
    private $volunteers;

    /**
     * @ORM\ManyToMany(targetEntity="Member", inversedBy="activities", cascade="persist")
     * @ORM\JoinTable(name="users_groups")
     */
    private $members;

    /**
     * @ORM\ManyToOne(targetEntity="SupportCenter", inversedBy="activities", cascade="persist")
     */
    private $supportcenter;

    /**
     * @ORM\ManyToMany(targetEntity="SupportCenterSection", inversedBy="activities", cascade="persist")
     */
    private $sections;

    /**
     * @ORM\ManyToOne(targetEntity="ActivityType", inversedBy="activities", cascade="persist")
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     * @Expose
     */
    private $start;

    /**
     * @ORM\Column(type="datetime")
     */
    private $end;

    /**
     * @ORM\OneToOne(targetEntity="AttendanceSheet", mappedBy="activity", cascade="persist", orphanRemoval=true)
     */
    private $attendancesheet;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $postalcode;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity="ExtendedTimeFragment", mappedBy="linkedactivity", orphanRemoval=true)
     */
    private $extendedtimefragments;

    public function __construct()
    {
        $this->employees = new ArrayCollection();
        $this->volunteers = new ArrayCollection();
        $this->members = new ArrayCollection();
        $this->sections = new ArrayCollection();
        $this->extendedtimefragments = new ArrayCollection();
    }

    public function __toString()
    {
        $date = $this->start->format('Y-m-d');
        return $date.' - '.$this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getSupportCenter()
    {
        return $this->supportcenter;
    }

    public function setSupportCenter($supportcenter)
    {
        $this->supportcenter = $supportcenter;
        return $this;
    }

    public function getEmployees()
    {
        return $this->employees;
    }

    public function setEmployees($employees)
    {
        $this->employees = $employees;
        return $this;
    }

    public function addEmployee($employee)
    {
        if (!$this->employees->contains($employee)) {
            $this->employees[] = $employee;
        }
        return $this;
    }

    public function removeEmployee($employee)
    {
        $this->employees->remove($employee);
        return $this;
    }

    public function getVolunteers()
    {
        return $this->volunteers;
    }

    public function setVolunteers($volunteers)
    {
        $this->volunteers = $volunteers;
        return $this;
    }

    public function addVolunteer($volunteer)
    {
        if (!$this->volunteers->contains($volunteer)) {
            $this->volunteers[] = $volunteer;
        }
        return $this;
    }

    public function removeVolunteer($volunteer)
    {
        $this->volunteers->remove($volunteer);
        return $this;
    }

    public function getMembers()
    {
        return $this->members;
    }

    public function setMembers($members)
    {
        $this->members = $members;
        return $this;
    }

    public function addMember($member)
    {
        if (!$this->members->contains($member)) {
            $this->members[] = $member;
        }
        return $this;
    }

    public function removeMember($member)
    {
        $this->members->remove($member);
        return $this;
    }

    public function getSections()
    {
        return $this->sections;
    }

    public function setSections($sections)
    {
        $this->sections = $sections;
        return $this;
    }

    public function addSection($section)
    {
        if (!$this->sections->contains($section)) {
            $this->sections[] = $section;
        }
        return $this;
    }

    public function removeSection($section)
    {
        $this->sections->remove($section);
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function setStart($start)
    {
        $this->start = $start;
        return $this;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function setEnd($end)
    {
        $this->end = $end;
    }

    public function getAttendanceSheet()
    {
        return $this->attendancesheet;
    }

    public function setAttendanceSheet($attendancesheet)
    {
        $this->attendancesheet = $attendancesheet;
        return $this;
    }

    public function isCompleted()
    {
        return $this->attendancesheet != null;
    }

    public function getNotes()
    {
        return $this->notes;
    }

    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    public function getPostalCode()
    {
        return $this->postalcode;
    }

    public function setPostalCode($postalcode)
    {
        $this->postalcode = $postalcode;
        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function getExtendedTimeFragments()
    {
        return $this->extendedtimefragments;
    }

    public function setExtendedTimeFragments($extendedtimefragments)
    {
        $this->extendedtimefragments = $extendedtimefragments;
        return $this;
    }

    public function addExtendedTimeFragment($extendedtimefragment)
    {
        if (!$this->extendedtimefragments->contains($extendedtimefragment)) {
            $this->extendedtimefragments[] = $extendedtimefragment;
        }
        return $this;
    }

    public function removeExtendedTimeFragment($extendedtimefragment)
    {
        $this->extendedtimefragments->remove($extendedtimefragment);
        return $this;
    }

    public function hasExtendedTimeFragment($extendedtimefragment)
    {
        return $this->extendedtimefragments->contains($extendedtimefragment);
    }
}
