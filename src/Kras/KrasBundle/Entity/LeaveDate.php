<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ExclusionPolicy("all")
 */
class LeaveDate
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @ORM\Column(type="date")
     * @Expose
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="UserLeave", inversedBy="leavedates")
     */
    private $leave;

    public function __construct()
    {
        $dt = new \DateTime();
        $dt->modify('+1 day');
        $this->date = $dt;
    }

    public function __toString()
    {
        return $this->date->format('Y-m-d');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    public function getLeave()
    {
        return $this->leave;
    }

    public function setLeave($leave)
    {
        $this->leave = $leave;
        return $this;
    }
}
