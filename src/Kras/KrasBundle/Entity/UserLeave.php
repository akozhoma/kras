<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks
 */
class UserLeave
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="Kras\UserBundle\Entity\User", inversedBy="leaves")
     */
    private $employees;

    /**
     * @ORM\ManyToOne(targetEntity="LeaveType", inversedBy="leaves")
     * @Expose
     */
    private $leavetype;

    /**
     * @ORM\Column(name="start_time", type="time", nullable=true)
     * @Expose
     */
    private $startTime;

    /**
     * @ORM\Column(name="end_time", type="time", nullable=true)
     * @Expose
     */
    private $endTime;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $duration;

    /**
     * @ORM\OneToMany(targetEntity="LeaveDate", mappedBy="leave", orphanRemoval=true, cascade={"persist"})
     * @Expose
     */
    private $leavedates;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $approved;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $identifier;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setDurationValue()
    {
        if (!$this->endTime || !$this->startTime) {
            $this->duration = null;
        } else {
            $this->duration = (($this->endTime->format('U') - $this->startTime->format('U')) * $this->leavedates->count());
        }
    }

    public function __construct()
    {
        $this->leavedates = new ArrayCollection();
        $this->startTime = new \DateTime();
        $this->endTime = new \DateTime();
        $this->employees = new ArrayCollection();
        $this->approved = null;
        $this->identifier = uniqid(null, true);
    }

    public function __toString()
    {
        $names = array();

        foreach ($this->employees as $employee) {
            $names[] = (string) $employee;
        }

        $names = implode(', ', $names);

        return ($this->employees->isEmpty() ? 'All' : $names).' - '.((string) $this->leavetype);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLeaveType()
    {
        return $this->leavetype;
    }

    public function setLeaveType($leavetype)
    {
        $this->leavetype = $leavetype;
        return $this;
    }

    public function getStartTime()
    {
        return $this->startTime;
    }

    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
        return $this;
    }

    public function getEndTime()
    {
        return $this->endTime;
    }

    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
        return $this;
    }

    public function getLeaveDates()
    {
        return $this->leavedates;
    }

    public function setLeaveDates($leavedates)
    {
        $this->leavedates = $leavedates;
        return $this;
    }

    public function addLeaveDate($leavedate)
    {
        if (!$this->leavedates->contains($leavedate)) {
            $this->leavedates[] = $leavedate;
            $leavedate->setLeave($this);
        }
        return $this;
    }

    public function removeLeaveDate($leavedate)
    {
        $this->leavedates->removeElement($leavedate);
        return $this;
    }

    public function hasLeaveDate($leavedate)
    {
        return $this->leavedates->contains($leavedate);
    }

    public function getDuration()
    {
        return $this->duration;
    }

    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    public function getApproved()
    {
        return $this->approved;
    }

    public function setApproved($approved)
    {
        $this->approved = $approved;
        return $this;
    }

    public function getEmployees()
    {
        return $this->employees;
    }

    public function setEmployees($employees)
    {
        $this->employees = $employees;
        return $this;
    }

    public function addEmployee($employee)
    {
        if (!$this->employees->contains($employee)) {
            $this->employees[] = $employee;
        }
        return $this;
    }

    public function removeEmployee($employee)
    {
        $this->employees->remove($employee);
        return $this;
    }

    public function hasEmployee($employee)
    {
        return $this->employees->contains($employee);
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
        return $this;
    }
}
