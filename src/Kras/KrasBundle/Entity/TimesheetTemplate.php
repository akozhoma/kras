<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity({"employee", "name"})
 * @ExclusionPolicy("all")
 */
class TimesheetTemplate
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Expose
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Kras\UserBundle\Entity\User", inversedBy="timesheettemplates")
     */
    private $employee;

    /**
     * @ORM\OneToMany(targetEntity="ExtendedTimeFragment", mappedBy="timesheettemplate", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"start" = "ASC"})
     * @Expose
     */
    private $extendedtimefragments;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $start;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $end;

    /**
     * @ORM\Column(name="required_hours", type="float", precision=2)
     * @Expose
     */
    private $requiredHours;

    /**
     * @ORM\Column(type="boolean")
     */
    private $approved;

    /**
     * @ORM\OneToMany(targetEntity="Timesheet", mappedBy="timesheettemplate")
     */
    private $timesheets;

    public function __construct()
    {
        $this->requiredHours = 0;
        $this->extendedtimefragments = new ArrayCollection();
        $this->timesheets = new ArrayCollection();
        $this->start = new \DateTime();
        $this->approved = false;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function calculateRequiredHours()
    {
        $diff = 0;

        foreach ($this->extendedtimefragments as $extendedTimeFragment) {
            $diff += $extendedTimeFragment->getEnd()->format('U') - $extendedTimeFragment->getStart()->format('U');
        }

        $hours = round($diff / 3600, 2);
        $this->requiredHours = $hours;
    }

    public function __toString()
    {
        return $this->id . ' - ' . $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getEmployee()
    {
        return $this->employee;
    }

    public function setEmployee($employee)
    {
        $this->employee = $employee;
        return $this;
    }

    public function getRequiredHours()
    {
        return $this->requiredHours;
    }

    public function setRequiredHours($requiredHours)
    {
        $this->requiredHours = $requiredHours;
        return $this;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function setStart($start)
    {
        $this->start = $start;
        return $this;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function setEnd($end)
    {
        $this->end = $end;
        return $this;
    }

    public function getApproved()
    {
        return $this->approved;
    }

    public function setApproved($approved)
    {
        $this->approved = $approved;
        return $this;
    }

    public function getExtendedTimeFragments()
    {
        return $this->extendedtimefragments;
    }

    public function setExtendedTimeFragments($extendedtimefragments)
    {
        $this->extendedtimefragments = $extendedtimefragments;
        return $this;
    }

    public function addExtendedTimeFragment($extendedtimefragment)
    {
        if (!$this->extendedtimefragments->contains($extendedtimefragment)) {
            $this->extendedtimefragments[] = $extendedtimefragment;
        }
        return $this;
    }

    public function removeExtendedTimeFragment($extendedtimefragment)
    {
        $this->extendedtimefragments->removeElement($extendedtimefragment);
        return $this;
    }

    public function hasExtendedTimeFragment($extendedtimefragment)
    {
        return $this->extendedtimefragments->contains($extendedtimefragment);
    }

    public function getTimesheets()
    {
        return $this->timesheets;
    }

    public function setTimesheets($timesheets)
    {
        $this->timesheets = $timesheets;
        return $this;
    }

    public function addTimesheet($timesheet)
    {
        if (!$this->timesheets->contains($timesheet)) {
            $this->timesheets[] = $timesheet;
        }
        return $this;
    }

    public function removeTimesheet($timesheet)
    {
        $this->timesheets->removeElement($timesheet);
        return $this;
    }

    public function hasTimesheet($timesheet)
    {
        return $this->timesheets->contains($timesheet);
    }
}
