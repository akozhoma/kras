<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @UniqueEntity("name")
 */
class SupportCenterSection
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minimumage;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $maximumage;

    /**
     * @ORM\ManyToOne(targetEntity="SupportCenter", inversedBy="sections")
     */
    private $supportcenter;

    /**
     * @ORM\ManyToMany(targetEntity="Member", mappedBy="sections", cascade="persist")
     */
    private $members;

    /**
     * @ORM\ManyToMany(targetEntity="Activity", mappedBy="sections", cascade="persist", orphanRemoval=true)
     */
    private $activities;

    /**
     * @ORM\Column(type="string", length=6);
     */
    private $color;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    public function __construct()
    {
        $this->members = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->enabled = true;
    }

    public function __toString()
    {
        return $this->supportcenter . ' - ' . $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getMinimumAge()
    {
        return $this->minimumage;
    }

    public function setMinimumAge($minimumage)
    {
        $this->minimumage = $minimumage;
        return $this;
    }

    public function getMaximumAge()
    {
        return $this->maximumage;
    }

    public function setMaximumAge($maximumage)
    {
        $this->maximumage = $maximumage;
        return $this;
    }

    public function getSupportCenter()
    {
        return $this->supportcenter;
    }

    public function setSupportCenter($supportcenter)
    {
        $this->supportcenter = $supportcenter;
        return $this;
    }

    public function getMembers()
    {
        return $this->members;
    }

    public function setMembers($members)
    {
        $this->members = $members;
        return $this;
    }

    public function addMember($member)
    {
        if (!$this->members->contains($member)) {
            $this->members[] = $member;
        }
        return $this;
    }

    public function removeMember($member)
    {
        $this->members->remove($member);
        return $this;
    }

    public function getActivities()
    {
        return $this->activities;
    }

    public function setActivities($activities)
    {
        $this->activities = $activities;
        return $this;
    }

    public function addActivity($activity)
    {
        if (!$this->activities->contains($activity)) {
            $this->acitivities[] = $activity;
        }
        return $this;
    }

    public function removeActivity($activity)
    {
        $this->activities->remove($activity);
        return $this;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }
}