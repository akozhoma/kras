<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @UniqueEntity("name")
 * @UniqueEntity("coordinator")
 */
class SupportCenter
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @ORM\Column(type="string")
     */
    private $postalcode;

    /**
     * @ORM\Column(type="string")
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity="Kras\UserBundle\Entity\User", mappedBy="supportcenter", cascade="persist")
     */
    private $employees;

    /**
     * @ORM\OneToOne(targetEntity="Kras\UserBundle\Entity\User", inversedBy="coordinating", cascade="persist")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $coordinator;

    /**
     * @ORM\OneToMany(targetEntity="SupportCenterSection", mappedBy="supportcenter", cascade="all", orphanRemoval=true)
     */
    private $sections;

    /**
     * @ORM\OneToMany(targetEntity="Member", mappedBy="supportcenter", cascade="persist", orphanRemoval=true)
     */
    private $members;

    /**
     * @ORM\OneToMany(targetEntity="Activity", mappedBy="supportcenter", cascade="persist", orphanRemoval=true)
     */
    private $activities;

    /**
     * @ORM\OneToMany(targetEntity="Timesheet", mappedBy="supportcenter", orphanRemoval=true)
     */
    private $timesheets;

    public function __construct()
    {
        $this->employees = new ArrayCollection();
        $this->sections = new ArrayCollection();
        $this->members = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->timesheets = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getEmployees()
    {
        return $this->employees;
    }

    public function setEmployees($employees)
    {
        $this->employees = $employees;
        return $this;
    }

    public function addEmployees($employee)
    {
        if (!$this->employees->contains($employee)) {
            $this->employees[] = $employee;
            $employee->setSupportCenter($this);
        }
        return $this;
    }

    public function removeEmployee($employee)
    {
        $this->employees->remove($employee);
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    public function getPostalCode()
    {
        return $this->postalcode;
    }

    public function setPostalCode($postalcode)
    {
        $this->postalcode = $postalcode;
        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function getCoordinator()
    {
        return $this->coordinator;
    }

    public function setCoordinator($coordinator)
    {

        $this->coordinator = $coordinator;
        return $this;
    }


    public function getSections()
    {
        return $this->sections;
    }

    public function setSections($sections)
    {
        $this->sections = $sections;
        return $this;
    }

    public function addSection($section)
    {
        if (!$this->sections->contains($section)) {
            $this->sections[] = $section;
            $section->setSupportCenter($this);
        }
        return $this;
    }

    public function removeSection($section)
    {
        $this->sections->remove($section);
        return $this;
    }

    public function getMembers()
    {
        return $this->members;
    }

    public function setMembers($members)
    {
        $this->members = $members;
        return $this;
    }

    public function addMember($member)
    {
        if (!$this->members->contains($member)) {
            $this->members[] = $member;
            $member->setSupportCenter($this);
        }
        return $this;
    }

    public function removeMember($member)
    {
        $this->members->remove($member);
        return $this;
    }

    public function getActivities()
    {
        return $this->activities;
    }

    public function setActivities($activities)
    {
        $this->activities = $activities;
        return $this;
    }

    public function addActivity($activity)
    {
        if (!$this->activities->contains($activity)) {
            $this->acitivities[] = $activity;
        }
        return $this;
    }

    public function removeActivity($activity)
    {
        $this->activities->remove($activity);
        return $this;
    }

    public function getTimesheets()
    {
        return $this->timesheets;
    }

    public function setTimesheets($timesheets)
    {
        $this->timesheets = $timesheets;
        return $this;
    }

    public function addTimesheet($timesheet)
    {
        if (!$this->timesheets->contains($timesheet)) {
            $this->timesheets[] = $timesheet;
        }
        return $this;
    }

    public function removeTimesheet($timesheet)
    {
        $this->timesheets->remove($timesheet);
        return $this;
    }

    public function hasTimesheet($timesheet)
    {
        return $this->timesheets->contains($timesheet);
    }
}
