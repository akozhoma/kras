<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Entity
 * @UniqueEntity({"lastname", "firstname"})
 * @ORM\HasLifecycleCallbacks
 * @ExclusionPolicy("all")
 */
class Member
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Expose
     */
    private $firstname;

    /**
     * @ORM\Column(type="string")
     * @Expose
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $gender;

    /**
     * @ORM\ManyToOne(targetEntity="SupportCenter", inversedBy="members", cascade="persist")
     */
    private $supportcenter;

    /**
     * @ORM\ManyToMany(targetEntity="SupportCenterSection", inversedBy="members", cascade="persist")
     */
    private $sections;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $postalcode;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="array")
     */
    private $phonenumbers;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthdate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $estimated;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $origin;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $language;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $completed;

    /**
     * @ORM\OneToMany(targetEntity="LogEntry", mappedBy="member", cascade="persist", orphanRemoval=true)
     */
    private $logentries;

    /**
     * @ORM\ManyToMany(targetEntity="Activity", mappedBy="members", cascade="persist")
     */
    private $activities;

    /**
     * @ORM\ManyToMany(targetEntity="Activity", mappedBy="volunteers", cascade="persist")
     */
    private $volunteered;

    /**
     * @ORM\ManytoMany(targetEntity="AttendanceSheet", mappedBy="members", cascade="persist")
     */
    private $attendancesheets;

    /**
     * @ORM\Column(type="boolean")
     */
    private $newcomer;

    /**
     * @ORM\OneToMany(targetEntity="ExtendedTimeFragment", mappedBy="linkedmember", orphanRemoval=true)
     */
    private $extendedtimefragments;

    /**
     * @ORM\Column(type="string")
     */
    private $label;

    public function __construct()
    {
        $this->sections = new ArrayCollection();
        $this->phonenumbers = array();
        $this->createdat = new \DateTime();
        $this->completed = false;
        $this->newcomer = false;
        $this->logentries = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->volunteered = new ArrayCollection();
        $this->attendancesheets = new ArrayCollection();
        $this->extendedtimefragments = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->label;
    }

    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function setLabelValue()
    {
        $this->label = ($this->firstname ? $this->firstname . ' ' : '') . $this->lastname;
    }

    public function getSectionLabel()
    {
        $sections = array();
        foreach ($this->sections as $section) {
            $sections[] = (string) $section;
        }
        $sections = implode(', ', $sections);

        return '['.$sections.'] '.$this->label;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFirstName()
    {
        return $this->firstname;
    }

    public function setFirstName($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    public function getLastName()
    {
        return $this->lastname;
    }

    public function setLastName($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    public function getSupportCenter()
    {
        return $this->supportcenter;
    }

    public function setSupportCenter($supportcenter)
    {
        $this->supportcenter = $supportcenter;
        return $this;
    }

    public function getSections()
    {
        return $this->sections;
    }

    public function setSections($sections)
    {
        $this->sections = $sections;
        return $this;
    }

    public function addSection($section)
    {
        if (!$this->sections->contains($section)) {
            $this->sections[] = $section;
        }
        return $this;
    }

    public function removeSection($section)
    {
        $this->sections->remove($section);
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    public function getPostalCode()
    {
        return $this->postalcode;
    }

    public function setPostalCode($postalcode)
    {
        $this->postalcode = $postalcode;
        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPhoneNumbers()
    {
        return $this->phonenumbers;
    }

    public function setPhoneNumbers($phonenumbers)
    {
        $this->phonenumbers = $phonenumbers;
        return $this;
    }

    public function getBirthDate()
    {
        return $this->birthdate;
    }

    public function setBirthDate($birthdate)
    {
        $this->birthdate = $birthdate;
        return $this;
    }

    public function getEstimated()
    {
        return $this->estimated;
    }

    public function setEstimated($estimated)
    {
        $this->estimated = $estimated;
        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    public function getOrigin()
    {
        return $this->origin;
    }

    public function setOrigin($origin)
    {
        $this->origin = $origin;
        return $this;
    }

    public function getLanguage()
    {
        return $this->language;
    }

    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdat;
    }

    public function setCreatedAt($createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }

    public function getNotes()
    {
        return $this->notes;
    }

    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    public function getCompleted()
    {
        return $this->completed;
    }

    public function setCompleted($completed)
    {
        $this->completed = $completed;
        return $this;
    }

    public function getLogEntries()
    {
        return $this->logentries;
    }

    public function setLogEntries($logentries)
    {
        $this->logentries = $logentries;
        return $this;
    }

    public function addLogEntry($logentry)
    {
        if (!$this->logentries->contains($logentry)) {
            $this->logentries[] = $logentry;
        }
        return $this;
    }

    public function removeLogEntry($logentry)
    {
        $this->logentries->remove($logentry);
        return $this;
    }

    public function getActivities()
    {
        return $this->activities;
    }

    public function setActivities($activities)
    {
        $this->activities = $activities;
        return $this;
    }

    public function addActivity($activity)
    {
        if (!$this->activities->contains($activity)) {
            $this->acitivities[] = $activity;
        }
        return $this;
    }

    public function removeActivity($activity)
    {
        $this->activities->remove($activity);
        return $this;
    }

    public function getVolunteered()
    {
        return $this->volunteered;
    }

    public function setVolunteered($volunteered)
    {
        $this->volunteered = $volunteered;
        return $this;
    }

    public function addVolunteered($volunteered)
    {
        if (!$this->voluntereed->contains($volunteered)) {
            $this->volunteered[] = $volunteered;
        }
        return $this;
    }

    public function removeVolunteered($volunteered)
    {
        $this->volunteered->remove($volunteered);
        return $this;
    }

    public function getAttendanceSheets()
    {
        return $this->attendancesheets;
    }

    public function setAttendanceSheets($attendancesheets)
    {
        $this->attendancesheets = $attendancesheets;
        return $this;
    }

    public function addAttendanceSheet($attendancesheet)
    {
        if (!$this->attendancesheets->contains($attendancesheet)) {
            $this->attendancesheets[] = $attendancesheet;
        }
        return $this;
    }

    public function removeAttendanceSheet($attendancesheets)
    {
        $this->attendancesheets->remove($attendancesheet);
        return $this;
    }

    public function getNewcomer()
    {
        return $this->newcomer;
    }

    public function setNewcomer($newcomer)
    {
        $this->newcomer = $newcomer;
        return $this;
    }

    public function getExtendedTimeFragments()
    {
        return $this->extendedtimefragments;
    }

    public function setExtendedTimeFragments($extendedtimefragments)
    {
        $this->extendedtimefragments = $extendedtimefragments;
        return $this;
    }

    public function addExtendedTimeFragment($extendedtimefragment)
    {
        if (!$this->extendedtimefragments->contains($extendedtimefragment)) {
            $this->extendedtimefragments[] = $extendedtimefragment;
        }
        return $this;
    }

    public function removeExtendedTimeFragment($extendedtimefragment)
    {
        $this->extendedtimefragments->remove($extendedtimefragment);
        return $this;
    }

    public function hasExtendedTimeFragment($extendedtimefragment)
    {
        return $this->extendedtimefragments->contains($extendedtimefragment);
    }

    /**
     * Checks whether or not this member is a newcomer
     * Newcomers are those who have the $newcomer var set to true and
     * were registered less than 3 years ago
     */
    public function isNewcomer()
    {
        if(!$this->createdat) return null;

        $dt = new \DateTime();

        $diff = $this->createdat->diff($dt);

        return $diff->y < 3 && $this->newcomer;
    }

    public function getAge()
    {
        if(!$this->birthdate) return null;

        $dt = new \DateTime();

        $diff = $this->birthdate->diff($dt);

        return $diff->y;
    }
}
