<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class LogEntry
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\ManyToOne(targetEntity="LogEntryTheme", inversedBy="logentries", cascade="persist")
     */
    private $theme;

    /**
     * @ORM\ManyToOne(targetEntity="Kras\UserBundle\Entity\User", inversedBy="logentries", cascade="persist")
     */
    private $employee;

    /**
     * @ORM\ManyToOne(targetEntity="Member", inversedBy="logentries", cascade="persist")
     */
    private $member;

    public function __construct()
    {
        $this->createdat = new \DateTime();
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getTheme()
    {
        return $this->theme;
    }

    public function setTheme($theme)
    {
        $this->theme = $theme;
        return $this;
    }

    public function getEmployee()
    {
        return $this->employee;
    }

    public function setEmployee($employee)
    {
        $this->employee = $employee;
        return $this;
    }

    public function getMember()
    {
        return $this->member;
    }

    public function setMember($member)
    {
        $this->member = $member;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdat;
    }

    public function setCreatedAt($createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
}