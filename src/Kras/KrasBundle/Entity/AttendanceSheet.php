<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class AttendanceSheet
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Activity", inversedBy="attendancesheet", cascade="persist")
     */
    private $activity;

    /**
     * @ORM\ManyToMany(targetEntity="Member", inversedBy="attendancesheets", cascade="persist")
     */
    private $members;

    /**
     * @ORM\Column(type="integer")
     */
    private $strangers;

    /**
     * @ORM\Column(type="integer")
     */
    private $visitors;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\Column(name="attendee_count", type="string", length=50, nullable=true)
     */
    private $attendeeCount;

    public function __construct()
    {
        $this->members = new ArrayCollection();
        $this->strangers = 0;
        $this->visitors = 0;
    }

    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function setAttendeeCountValue()
    {
        $attendeeCount = sprintf(
            '%s (+ %s) (+ %s)',
            $this->members->count(),
            $this->strangers,
            $this->visitors
        );
        $this->setAttendeeCount($attendeeCount);
    }

    public function __toString()
    {
        return $this->activity;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getActivity()
    {
        return $this->activity;
    }

    public function setActivity($activity)
    {
        $this->activity = $activity;
    }

    public function getMembers()
    {
        return $this->members;
    }

    public function setMembers($members)
    {
        $this->members = $members;
        return $this;
    }

    public function addMember($member)
    {
        if (!$this->members->contains($member)) {
            $this->members[] = $member;
            $member->setSupportCenter($this);
        }
        return $this;
    }

    public function removeMember($member)
    {
        $this->members->remove($member);
        return $this;
    }

    public function getStrangers()
    {
        return $this->strangers;
    }

    public function setStrangers($strangers)
    {
        $this->strangers = $strangers;
        return $this;
    }

    public function getVisitors()
    {
        return $this->visitors;
    }

    public function setVisitors($visitors)
    {
        $this->visitors = $visitors;
        return $this;
    }

    public function getNotes()
    {
        return $this->notes;
    }

    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    public function getAttendeeCount()
    {
        return $this->attendeeCount;
    }

    public function setAttendeeCount($attendeeCount)
    {
        $this->attendeeCount = $attendeeCount;
        return $this;
    }
}