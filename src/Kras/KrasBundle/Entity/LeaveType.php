<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\Validator\ExecutionContext;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ExclusionPolicy("all")
 * @UniqueEntity({"name"})
 */
class LeaveType
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Expose
     */
    private $name;

    /**
     * @ORM\Column(name="public_holiday", type="boolean")
     * @Expose
     */
    private $publicHoliday;

    /**
     * @ORM\Column(name="overtime", type="boolean")
     * @Expose
     */
    private $overtime;

    /**
     * @ORM\Column(type="boolean")
     * @Expose
     */
    private $sickness;

    /**
     * @ORM\OneToMany(targetEntity="AllowedLeave", mappedBy="leavetype", orphanRemoval=true)
     */
    private $allowedleaves;

    /**
     * @ORM\OneToMany(targetEntity="UserLeave", mappedBy="leavetype", orphanRemoval=true)
     */
    private $leaves;

    /**
     * @ORM\Column(name="plannable_by_employee", type="boolean")
     */
    private $plannableByEmployee;

    public function __construct()
    {
        $this->publicHoliday = false;
        $this->overtime = false;
        $this->sickness = false;
        $this->plannableByEmployee = false;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getAllowedLeaves()
    {
        return $this->allowedleaves;
    }

    public function setAllowedLeaves($allowedleaves)
    {
        $this->allowedleaves = $allowedleaves;
        return $this;
    }

    public function addAllowedLeave($allowedleave)
    {
        if (!$this->allowedleaves->contains($allowedleave)) {
            $this->allowedleaves[] = $allowedleave;
        }
        return $this;
    }

    public function removeAllowedLeave($allowedleave)
    {
        $this->allowedleaves->remove($allowedleave);
        return $this;
    }

    public function hasAllowedLeave($allowedleave)
    {
        return $this->allowedleaves->contains($allowedleave);
    }

    public function getLeaves()
    {
        return $this->leaves;
    }

    public function setLeaves($leaves)
    {
        $this->leaves = $leaves;
        return $this;
    }

    public function addLeave($leave)
    {
        if (!$this->leaves->contains($leave)) {
            $this->leaves[] = $leave;
        }
        return $this;
    }

    public function removeLeave($leave)
    {
        $this->leaves->remove($leave);
        return $this;
    }

    public function hasLeave($leave)
    {
        return $this->leaves->contains($leave);
    }

    public function getPublicHoliday()
    {
        return $this->publicHoliday;
    }

    public function setPublicHoliday($publicHoliday)
    {
        $this->publicHoliday = $publicHoliday;
        return $this;
    }

    public function getOvertime()
    {
        return $this->overtime;
    }

    public function setOvertime($overtime)
    {
        $this->overtime = $overtime;
        return $this;
    }

    public function getSickness()
    {
        return $this->sickness;
    }

    public function setSickness($sickness)
    {
        $this->sickness = $sickness;
        return $this;
    }

    public function getPlannableByEmployee()
    {
        return $this->plannableByEmployee;
    }

    public function setPlannableByEmployee($plannableByEmployee)
    {
        $this->plannableByEmployee = $plannableByEmployee;
        return $this;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContext $context)
    {
        if ($this->getPlannableByEmployee() && ($this->getPublicHoliday())) {
            $context->addViolationAt(
                'plannableByEmployee',
                'Public holidays  cannot be set to be plannable by employees!'
            );
        }

        $values = array_count_values(array(
            $this->getOvertime() ? 'true' : 'false',
            $this->getPublicHoliday() ? 'true' : 'false',
            $this->getSickness() ? 'true' : 'false',
        ));

        if (array_key_exists('true', $values) && ($values['true'] > 1)) {
            $context->addViolationAt(
                'publicHoliday',
                'A leave type can only be one of public holiday, sickness or overtime.'
            );
        }
    }
}
