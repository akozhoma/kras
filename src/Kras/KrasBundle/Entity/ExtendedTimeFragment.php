<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Entity
 * @ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks
 */
class ExtendedTimeFragment
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Expose
     */
    private $start;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Expose
     */
    private $end;

    /**
     * @ORM\ManyToOne(targetEntity="TimeFragmentType", inversedBy="extendedtimefragments")
     * @Expose
     */
    private $timefragmenttype;

    /**
     *  @ORM\ManyToOne(targetEntity="Training", inversedBy="extendedtimefragments")
     *  @Expose
     */
    private $linkedtraining;

    /**
     * @ORM\ManyToOne(targetEntity="Activity", inversedBy="extendedtimefragments")
     * @Expose
     */
    private $linkedactivity;

    /**
     * @ORM\ManyToOne(targetEntity="Member", inversedBy="extendedtimefragments")
     * @Expose
     */
    private $linkedmember;

    /**
     * @ORM\Column(type="text", nullable=true, length=100)
     * @Expose
     */
    private $notes;

    /**
     * @ORM\ManyToOne(targetEntity="Timesheet", inversedBy="extendedtimefragments")
     */
    private $timesheet;

    /**
     * @ORM\ManyToOne(targetEntity="TimesheetTemplate", inversedBy="extendedtimefragments")
     */
    private $timesheettemplate;

    public function getId()
    {
        return $this->id;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function setStart($start)
    {
        $this->start = $start;
        $this->start->modify("-{$this->start->format('s')} seconds");

        return $this;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function setEnd($end)
    {
        $this->end = $end;
        $this->end->modify("-{$this->end->format('s')} seconds");

        return $this;
    }

    public function getTimesheet()
    {
        return $this->timesheet;
    }

    public function setTimesheet($timesheet)
    {
        $this->timesheet = $timesheet;
        $this->timesheettemplate = null;
        return $this;
    }

    public function getTimesheetTemplate()
    {
        return $this->timesheettemplate;
    }

    public function setTimesheetTemplate($timesheettemplate)
    {
        $this->timesheettemplate = $timesheettemplate;
        $this->timesheet = null;
        return $this;
    }

    public function getTimeFragmentType()
    {
        return $this->timefragmenttype;
    }

    public function setTimeFragmentType($timefragmenttype)
    {
        $this->timefragmenttype = $timefragmenttype;
        return $this;
    }

    public function getNotes()
    {
        return $this->notes;
    }

    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    public function getLinkedTraining()
    {
        return $this->linkedtraining;
    }

    public function setLinkedTraining($linkedtraining)
    {
        $this->linkedtraining = $linkedtraining;
        return $this;
    }

    public function getLinkedActivity()
    {
        return $this->linkedactivity;
    }

    public function setLinkedActivity($linkedactivity)
    {
        $this->linkedactivity = $linkedactivity;
        return $this;
    }

    public function getLinkedMember()
    {
        return $this->linkedmember;
    }

    public function setLinkedMember($linkedmember)
    {
        $this->linkedmember = $linkedmember;
        return $this;
    }
}
