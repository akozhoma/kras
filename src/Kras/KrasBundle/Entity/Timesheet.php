<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity({"employee", "year", "week"})
 * @ExclusionPolicy("all")
 */
class Timesheet
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Kras\UserBundle\Entity\User", inversedBy="timesheets")
     */
    private $employee;

    /**
     * @ORM\ManyToOne(targetEntity="SupportCenter", inversedBy="timesheets")
     */
    private $supportcenter;

    /**
     * @ORM\OneToMany(targetEntity="ExtendedTimeFragment", mappedBy="timesheet", orphanRemoval=true, cascade={"persist"})
     * @ORM\OrderBy({"start" = "ASC"})
     * @Expose
     */
    private $extendedtimefragments;

    /**
     * @ORM\Column(type="integer", length=4)
     */
    private $year;

    /**
     * @ORM\Column(type="integer")
     */
    private $week;

    /**
     * @ORM\Column(name="approved_co", type="boolean")
     */
    private $approvedCo;

    /**
     * @ORM\Column(name="approved_de", type="boolean")
     * @var [type]
     */
    private $approvedDi;

    /**
     * @ORM\Column(name="approved_hr", type="boolean")
     */
    private $approvedHr;

    /**
     * @ORM\Column(type="boolean")
     */
    private $submitted;

    /**
     * @ORM\Column(name="worked_hours", type="float", precision=2)
     * @Expose
     */
    private $workedHours;

    /**
     * @ORM\Column(name="required_hours", type="float", precision=2)
     * @Expose
     */
    private $requiredHours;

    /**
     * @ORM\Column(name="overtime_hours", type="float", precision=2)
     * @Expose
     */
    private $overtimeHours;

    /**
     * @ORM\Column(name="remaining_overtime_hours", type="float", precision=2)
     * @Expose
     */
    private $remainingOvertimeHours;

    /**
     * @ORM\Column(name="used_overtime_hours", type="float", precision=2)
     * @Expose
     */
    private $usedOvertimeHours;

    /**
     * @ORM\Column(name="start_date", type="date")
     * @Expose
     */
    private $startDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Expose
     */
    private $comments;

    /**
     * @ORM\Column(type="boolean")
     * @Expose
     */
    private $rejected;

    /**
     * @ORM\ManyToOne(targetEntity="TimesheetTemplate", inversedBy="timesheets")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $timesheettemplate;

    public function __construct()
    {
        $this->workedHours = 0;
        $this->requiredHours = 0;
        $this->overtimeHours = 0;
        $this->remainingOvertimeHours = 0;
        $this->usedOvertimeHours = 0;
        $this->year = date('Y');
        $this->week = date('W');
        $this->approvedCo = false;
        $this->approvedHr = false;
        $this->approvedDi = false;
        $this->submitted = false;
        $this->rejected = false;
        $this->extendedtimefragments = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setStartDateValue()
    {
        $week_start = new \DateTime();
        $week_start->setISODate($this->year, $this->week);
        $this->startDate = $week_start;
    }

    public function __toString()
    {
        return ((string) $this->employee).' - '. $this->year . ' - ' . $this->week;
    }

    public function getTimesheetTemplate()
    {
        return $this->timesheettemplate;
    }

    public function setTimesheetTemplate($timesheettemplate)
    {
        $this->timesheettemplate = $timesheettemplate;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEmployee()
    {
        return $this->employee;
    }

    public function setEmployee($employee)
    {
        $this->employee = $employee;
        return $this;
    }

    public function getExtendedTimeFragments()
    {
        return $this->extendedtimefragments;
    }

    public function setExtendedTimeFragments($extendedtimefragments)
    {
        $this->extendedtimefragments = $extendedtimefragments;
        return $this;
    }

    public function addExtendedTimeFragment($extendedtimefragment)
    {
        if (!$this->extendedtimefragments->contains($extendedtimefragment)) {
            $this->extendedtimefragments[] = $extendedtimefragment;
        }
        return $this;
    }

    public function removeExtendedtimeFragment($extendedtimefragment)
    {
        $this->extendedtimefragments->remove($extendedtimefragment);
        return $this;
    }

    public function hasExtendedTimeFragment($extendedtimefragment)
    {
        return $this->extendedtimefragments->contains($extendedtimefragment);
    }

    public function getSubmitted()
    {
        return $this->submitted;
    }

    public function setSubmitted($submitted)
    {
        $this->submitted = $submitted;
        return $this;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    public function getWeek()
    {
        return $this->week;
    }

    public function setWeek($week)
    {
        $this->week = $week;
        return $this;
    }

    public function getSupportCenter()
    {
        return $this->supportcenter;
    }

    public function setSupportCenter($supportcenter)
    {
        $this->supportcenter = $supportcenter;
        return $this;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    public function getApprovedCo()
    {
        return $this->approvedCo;
    }

    public function setApprovedCo($approvedCo)
    {
        $this->approvedCo = $approvedCo;
        return $this;
    }

    public function getApprovedDi()
    {
        return $this->approvedDi;
    }

    public function setApprovedDi($approvedDi)
    {
        $this->approvedDi = $approvedDi;
        return $this;
    }

    public function getApprovedHr()
    {
        return $this->approvedHr;
    }

    public function setApprovedHr($approvedHr)
    {
        $this->approvedHr = $approvedHr;
        return $this;
    }

    public function getWorkedHours()
    {
        return $this->workedHours;
    }

    public function setWorkedHours($workedHours)
    {
        $this->workedHours = $workedHours;
        return $this;
    }

    public function getRequiredHours()
    {
        return $this->requiredHours;
    }

    public function setRequiredHours($requiredHours)
    {
        $this->requiredHours = $requiredHours;
        return $this;
    }

    public function getOvertimeHours()
    {
        return $this->overtimeHours;
    }

    public function setOvertimeHours($overtimeHours)
    {
        $this->overtimeHours = $overtimeHours;
        return $this;
    }

    public function getUsedOvertimeHours()
    {
        return $this->usedOvertimeHours;
    }

    public function setUsedOvertimeHours($usedOvertimeHours)
    {
        $this->usedOvertimeHours = $usedOvertimeHours;
        return $this;
    }

    public function getRemainingOvertimeHours()
    {
        return $this->remainingOvertimeHours;
    }

    public function setRemainingOvertimeHours($remainingOvertimeHours)
    {
        $this->remainingOvertimeHours = $remainingOvertimeHours;
        return $this;
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function setComments($comments)
    {
        $this->comments = $comments;
        return $this;
    }

    public function getRejected()
    {
        return $this->rejected;
    }

    public function setRejected($rejected)
    {
        $this->rejected = $rejected;
        return $this;
    }

    public function isSubmittable()
    {
        return $this->getWorkedHours() >= $this->getRequiredHours();
    }
}
