<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks
 */
class AllowedLeave
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="Kras\UserBundle\Entity\User", inversedBy="allowedleaves")
     */
    private $employees;

    /**
     * @ORM\ManyToOne(targetEntity="LeaveType", inversedBy="allowedleaves")
     * @Expose
     */
    private $leavetype;

    /**
     * @ORM\Column(type="float", precision=2)
     * @Expose
     */
    private $amount;

    /**
     * @ORM\Column(type="integer", length=4)
     * @Expose
     */
    private $year;

    public function __construct()
    {
        $this->amount = -1;
        $this->year = date('Y');
        $this->employees = new ArrayCollection();
    }

    public function __toString()
    {
        $names = array();

        foreach ($this->employees as $employee) {
            $names[] = (string) $employee;
        }

        $names = implode(', ', $names);

        return $this->year.' - '.($this->employees->isEmpty() ? 'All' : $names).' - '.((string) $this->leavetype);
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setAmountValue()
    {
        if (($this->amount < 0) && ($this->amount != -1)) {
            $this->amount = -1;
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLeaveType()
    {
        return $this->leavetype;
    }

    public function setLeaveType($leavetype)
    {
        $this->leavetype = $leavetype;
        return $this;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    public function getEmployees()
    {
        return $this->employees;
    }

    public function setEmployees($employees)
    {
        $this->employees = $employees;
        return $this;
    }

    public function addEmployee($employee)
    {
        if (!$this->employees->contains($employee)) {
            $this->employees[] = $employee;
        }
        return $this;
    }

    public function removeEmployee($employee)
    {
        $this->employees->remove($employee);
        return $this;
    }

    public function hasEmployee($employee)
    {
        return $this->employees->contains($employee);
    }
}
