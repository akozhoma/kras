<?php

namespace Kras\KrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Entity
 * @UniqueEntity({"name"})
 * @ExclusionPolicy("all")
 */
class TimeFragmentType
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Expose
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=7)
     * @Expose
     */
    private $color;

    /**
     * @ORM\Column(name="link_activity", type="boolean")
     * @Expose
     */
    private $linkActivity;

    /**
     * @ORM\Column(name="link_member", type="boolean")
     * @Expose
     */
    private $linkMember;

    /**
     * @ORM\Column(name="link_training", type="boolean")
     * @Expose
     */
    private $linkTraining;

    /**
     * @ORM\OneToMany(targetEntity="ExtendedTimeFragment", mappedBy="timefragmenttype", orphanRemoval=true)
     */
    private $extendedtimefragments;

    public function __toString()
    {
        return $this->name;
    }

    public function __construct()
    {
        $this->extendedtimefragments = new ArrayCollection();
        $this->color = '#CDD7B6';
        $this->linkActivity = false;
        $this->linkMember = false;
        $this->linkTraining = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getExtendedTimeFragments()
    {
        return $this->extendedtimefragments;
    }

    public function setExtendedTimeFragments($extendedtimefragments)
    {
        $this->extendedtimefragments = $extendedtimefragments;
        return $this;
    }

    public function addExtendedTimeFragment($extendedtimefragment)
    {
        if (!$this->extendedtimefragments->contains($extendedtimefragment)) {
            $this->extendedtimefragments[] = $extendedtimefragment;
        }
        return $this;
    }

    public function removeExtendedTimeFragment($extendedtimefragment)
    {
        $this->extendedtimefragments->remove($extendedtimefragment);
        return $this;
    }

    public function hasExtendedTimeFragment($extendedTimeFragment)
    {
        return $this->extendedtimefragments->contains($extendedtimefragment);
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    public function getLinkActivity()
    {
        return $this->linkActivity;
    }

    public function setLinkActivity($linkActivity)
    {
        $this->linkActivity = $linkActivity;
        return $this;
    }

    public function getLinkMember()
    {
        return $this->linkMember;
    }

    public function setLinkMember($linkMember)
    {
        $this->linkMember = $linkMember;
        return $this;
    }

    public function getLinkTraining()
    {
        return $this->linkTraining;
    }

    public function setLinkTraining($linkTraining)
    {
        $this->linkTraining = $linkTraining;
        return $this;
    }
}
