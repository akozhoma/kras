<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Kras\KrasBundle\Form\SupportCenterSectionType;
use Kras\KrasBundle\Form\EventListener\SupportCenterCoordinatorSubscriber;

class SupportCenterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address')
            ->add('postalcode', null, array('label' => 'Postal code', 'attr' => array('class' => 'postalcode-input')))
            ->add('city', null, array('attr' => array('class' => 'city-input')));

        $builder->addEventSubscriber(new SupportCenterCoordinatorSubscriber());

        $builder->add('sections', 'collection', array(
                'type' => new SupportCenterSectionType(),
                'allow_add' => true,
                'allow_delete' => true,
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\SupportCenter'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_supportcentertype';
    }
}
