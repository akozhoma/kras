<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Kras\KrasBundle\Form\EventListener\AttendanceSheetMemberListener;

class AttendanceSheetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber(new AttendanceSheetMemberListener());

        $builder
            ->add('strangers', null, array('label' => 'Strangers', 'required' => false));

        $builder
            ->add('visitors', null, array('label' => 'Visitors', 'required' => false));

        $builder
            ->add('notes', 'textarea', array('label' => 'Notes', 'required' => false));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\AttendanceSheet'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_attendancesheettype';
    }
}
