<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TimesheetTemplateExtendedTimeFragmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('timefragmenttype', null, array(
                'label'       => 'Type',
                'required'    => true,
                'empty_value' => 'Select a type',
                'attr'        => array(
                    'class'       => 'fragment_type',
                ),
            ))
            ->add('day_of_week', 'hidden', array(
                'mapped'       => false,
                'attr'         => array(
                    'class'        => 'timesheet_day_of_week',
                ),
                'pattern'      => '[1-7]',
            ))
            ->add('start_time', 'time', array(
                'input'        => 'array',
                'widget'       => 'choice',
                'with_seconds' => false,
                'minutes'      => range(0, 59),
                'mapped'       => false,
                'attr'         => array(
                    'class'        => 'time-dropdown time-start-dropdown',
                ),
            ))
            ->add('end_time', 'time', array(
                'input'        => 'array',
                'widget'       => 'choice',
                'with_seconds' => false,
                'minutes'      => range(0, 59),
                'mapped'       => false,
                'attr'         => array(
                    'class'        => 'time-dropdown time-end-dropdown',
                ),
            ))
            ->add('notes', null, array(
                'attr'         => array(
                    'class'        => 'event-notes',
                ),
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\ExtendedTimeFragment'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_timesheettemplateextendedtimefragmenttype';
    }
}
