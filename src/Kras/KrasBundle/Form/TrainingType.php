<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TrainingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('price', null, array(
                'data'     => '0.00',
                'required' => true,
            ))
            ->add('organizer', 'genemu_jqueryautocomplete_text', array(
                'route_name' => 'kras_training_get_organizers_ajax'
            ))
            ->add('employee', null, array(
                'attr'     => array(
                    'class'    => 'chosen-select',
                ),
                'required' => true,
                'empty_value' => false,
            ))
            ->add('approved', null, array(
                'required' => false,
                'label'    => 'Approved?',
            ))
            ->add('trainingDates', 'collection', array(
                'by_reference' => false,
                'allow_add'    => true,
                'allow_delete' => true,
                'required'     => true,
                'label'        => false,
                'type'         => new TrainingDateType(),
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\Training'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_trainingtype';
    }
}
