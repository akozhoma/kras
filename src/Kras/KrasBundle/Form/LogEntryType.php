<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LogEntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array(
                'label' => 'Title',
                'attr'  => array(
                    'class' => 'logentry-title',
                ),
            ))
            ->add('description', null, array(
                'label'    => 'Description',
                'required' => false,
            ))
            ->add('member', null, array(
                'label'    => 'Member',
                'required' => true,
            ))
            ->add('employee', null, array(
                'label'    => 'Employee',
                'required' => true,
            ))
            ->add('theme', null, array(
                'label'    => 'Theme',
                'required' => true,
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\LogEntry'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_logentrytype';
    }
}
