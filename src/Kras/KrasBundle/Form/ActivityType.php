<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Kras\KrasBundle\Form\EventListener\ActivityDatesSubscriber;
use Kras\KrasBundle\Form\EventListener\ActivityVolunteerSubscriber;
use Kras\KrasBundle\Form\EventListener\ActivityEmployeeSubscriber;
use Kras\KrasBundle\Form\EventListener\ActivitySectionSubscriber;
use Kras\KrasBundle\Form\EventListener\ActivityAddressSubscriber;

class ActivityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array(
                'label' => 'Name',
            ))
            ->add('description', 'textarea', array(
                'label'    => 'Description',
                'required' => false,
            ))
            ->add('type', null, array(
                'label'    => 'Type',
                'required' => true,
                'attr'     => array(
                    'class'    => 'chosen-select',
                ),
            ));

        $builder->addEventSubscriber(new ActivityDatesSubscriber());

        $builder->addEventSubscriber(new ActivitySectionSubscriber());

        $builder->addEventSubscriber(new ActivityEmployeeSubscriber());

        $builder->addEventSubscriber(new ActivityVolunteerSubscriber());

        $builder->addEventSubscriber(new ActivityAddressSubscriber());

        $builder
            ->add('notes', 'textarea', array(
                'label'    => 'Notes',
                'required' => false,
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\Activity'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_activitytype';
    }
}
