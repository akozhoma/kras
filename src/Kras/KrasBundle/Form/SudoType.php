<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class SudoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userpassword = new UserPassword();
        $userpassword->message = 'This value should be your current password.';

        $builder->add('currentpassword', 'password', array(
            'label'       =>'Current password',
            'mapped'      => false,
            'constraints' => $userpassword,
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\UserBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_sudotype';
    }
}
