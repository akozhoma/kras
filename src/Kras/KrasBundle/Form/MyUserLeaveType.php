<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Kras\KrasBundle\Form\EventListener\MyUserLeaveSubscriber;

class MyUserLeaveType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subLeaves', 'collection', array(
                'type'         => new LeaveSubLeaveType(),
                'by_reference' => false,
                'allow_add'    => true,
                'allow_delete' => true,
                'required'     => true,
                'label'        => 'Dates & Times',
                'mapped'       => false,
            ))
        ;

        $builder
            ->addEventSubscriber(new MyUserLeaveSubscriber())
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\UserLeave'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_myuserleavetype';
    }
}
