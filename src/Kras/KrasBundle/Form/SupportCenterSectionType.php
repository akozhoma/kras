<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SupportCenterSectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description', 'textarea', array('required' => false))
            ->add('minimumage', null, array('label' => 'Minimum age', 'required' => false))
            ->add('maximumage', null, array('label' => 'Maximum age', 'required' => false))
            ->add('color', 'choice', array(
                'choices'   => array(
                    'FBB829' => 'Gold',
                    '1693A5' => 'Teal',
                    'C7F464' => 'Green',
                    'FF6600' => 'Orange',
                    '9D646E' => 'Purple',
                    'FF614D' => 'Red',
                    '327CCB' => 'Blue',
                    'F5A07B' => 'Peach',
                    'D5D6CB' => 'Gray',
                    'FB5262' => 'Pink',
                )
            ))
            ->add('enabled', null, array('label' => 'Enabled?', 'required' => false))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\SupportCenterSection'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_supportcentersectiontype';
    }
}
