<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Kras\UserBundle\Form\EventListener\PasswordRequirementSubscriber;
use Kras\KrasBundle\Form\DataTransformer\StringToArrayTransformer;

class EmployeeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new StringToArrayTransformer();

        $builder
            ->add('firstname', null, array('label' => 'First name', 'required' => false))
            ->add('lastname', null, array('label' => 'Last name', 'required' => false))
            ->add('username', null, array('label' => 'Username'))
            ->add('email', 'email', array('label' => 'Email address'))
            ->add($builder->create('roles', 'choice', array(
                    'choices' => array('ROLE_INTERN' => 'Intern', 'ROLE_EMPLOYEE' => 'Employee'),
                    'multiple' => false,
                    'expanded' => true,
                    'label' => 'Role'
            ))->addModelTransformer($transformer))
            ->add('enabled', 'checkbox', array('required' => false, 'label' => 'Enable account?'))
            ->add('expiresat', 'date', array('label' => 'Expires at', 'widget' => 'single_text', 'attr' => array('class' => 'datepicker'), 'required' => false))
            ->add('startedWorking', 'date', array(
                'widget'   => 'single_text',
                'attr'     => array(
                    'class'    => 'datepicker',
                ),
                'required' => true,
            ))
            ->addEventSubscriber(new PasswordRequirementSubscriber())
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\UserBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_employeetype';
    }
}
