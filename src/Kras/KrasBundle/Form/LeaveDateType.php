<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LeaveDateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', null, array(
                'required'    => true,
                'empty_value' => false,
                'label'       => 'Date',
                'attr'        => array(
                    'class'       => 'datepicker',
                ),
                'widget'      => 'single_text',
                'format'      => 'yyyy-MM-dd',
                'read_only'   => true,
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\LeaveDate'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_leavedatetype';
    }
}
