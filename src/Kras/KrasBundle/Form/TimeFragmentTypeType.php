<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TimeFragmentTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('color', null, array(
                'attr' => array(
                    'class' => 'color-picker',
                ),
            ))
            ->add('linkActivity', null, array(
                'label'    => 'Link activity?',
                'required' => false,
            ))
            ->add('linkMember', null, array(
                'label'    => 'Link member?',
                'required' => false,
            ))
            ->add('linkTraining', null, array(
                'label'    => 'Link training?',
                'required' => false,
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\TimeFragmentType'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_timefragmenttypetype';
    }
}
