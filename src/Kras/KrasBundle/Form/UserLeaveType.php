<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserLeaveType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('leavetype', null, array(
                'label'       => 'Leave type',
                'required'    => true,
                'empty_value' => false,
                'attr'        => array(
                    'class'       => 'chosen-select leave-type',
                ),
            ))
            ->add('employees', null, array(
                'required'    => false,
                'empty_value' => '-- All employees --',
                'attr'        => array(
                    'class'       => 'chosen-select',
                ),
                'query_builder' => function($er) {
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.label', 'ASC');
                }
            ))
            ->add('startTime', null, array(
                'widget'       => 'choice',
                'with_seconds' => false,
                'minutes'      => array(0, 15, 30, 45),
                'required'     => false,
                'empty_value'  => false,
                'attr'         => array(
                    'class'        => 'time-dropdown time-start-dropdown',
                ),
            ))
            ->add('endTime', null, array(
                'widget'       => 'choice',
                'with_seconds' => false,
                'required'     => false,
                'empty_value'  => false,
                'minutes'      => array(0, 15, 30, 45),
                'attr'         => array(
                    'class'        => 'time-dropdown time-end-dropdown',
                ),
            ))
            ->add('leavedates', 'collection', array(
                'by_reference' => false,
                'allow_add'    => true,
                'allow_delete' => true,
                'required'     => true,
                'label'        => 'Date(s)',
                'type'         => new LeaveDateType(),
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\UserLeave'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_userleavetype';
    }
}
