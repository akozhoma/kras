<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Kras\KrasBundle\Form\EventListener\MemberNewcomerSubscriber;

class MemberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', null, array('label' => 'First name', 'attr' => array('class' => 'member-firstname', 'autocomplete' => 'off')))
            ->add('lastname', null, array('label' => 'Last name', 'attr' => array('class' => 'member-lastname', 'autocomplete' => 'off')))
            ->add('gender', 'choice', array(
                'choices' => array(
                    'GENDER_MALE'   => 'GENDER_MALE',
                    'GENDER_FEMALE' => 'GENDER_FEMALE'
                ),
                    'multiple' => false,
                    'expanded' => true,
                    'required' => true,
            ))
            ->add('supportcenter', null, array(
                'label'    => 'Support center',
                'required' => true,
                'attr'     => array(
                    'class'    => 'chosen-select',
                ),
            ))
            ->add('sections', null, array(
                'required' => true,
                'multiple' => true,
                'attr'     => array(
                    'class'    => 'chosen-select',
                ),
            ))
            ->add('type', 'choice', array(
                'label'    => 'Member type',
                'required' => true,
                'choices'  => array(
                    'MEMBER_NORMAL'    => 'MEMBER_NORMAL',
                    'MEMBER_VOLUNTEER' => 'MEMBER_VOLUNTEER'
                ),
                'attr' => array(
                    'class' => 'chosen-select',
                ),
            ))
            ->add('address', null, array(
                    'label' => 'Address',
                    'required' => false
                )
            )
            ->add('postalcode', null, array(
                    'label' => 'Postal code',
                    'required' => false,
                    'attr' => array(
                        'class' => 'postalcode-input'
                    )
                )
            )
            ->add('city', null, array(
                    'label' => 'City',
                    'required' => false,
                    'attr' => array(
                        'class' => 'city-input'
                    )
                )
            )
            ->add('email', 'email', array(
                    'label' => 'Email address',
                    'required' => false
                )
            )
            ->add('phonenumbers', 'collection', array(
                'type' => 'text',
                'allow_add' => true,
                'allow_delete' => true,
                'required' => false,
                'options' => array('required' => false)
            ))
            ->add('birthdate', 'date', array(
                    'widget'    => 'single_text',
                    'label'     => 'Date of birth',
                    'attr'      => array(
                        'class'     => 'dateofbirthpicker'
                    ),
                    'required'  => false,
                    'read_only' => true,
                )
            )
            ->add('estimated', null, array(
                    'label' => 'Is this date of birth an estimation?',
                    'required' => false
                )
            )
            ->add('country', 'country', array(
                    'label' => 'Nationality',
                    'required' => false,
                    'empty_value' => 'None',
                    'attr' => array(
                        'class' => 'chosen-select'
                    )
                )
            )
            ->add('origin', 'country', array(
                    'label' => 'Country of origin',
                    'required' => false,
                    'empty_value' => 'None',
                    'attr' => array(
                        'class' => 'chosen-select'
                    )
                )
            )
            ->add('language', 'language', array(
                    'label' => 'Mother tongue',
                    'required' => false,
                    'empty_value' => 'None',
                    'attr' => array(
                        'class' => 'chosen-select'
                    )
                )
            );

        $builder->addEventSubscriber(new MemberNewcomerSubscriber());

        $builder
            ->add('notes', 'textarea', array('label' => 'Notes', 'required' => false));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\Member'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_membertype';
    }
}
