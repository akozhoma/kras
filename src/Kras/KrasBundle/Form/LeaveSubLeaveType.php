<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LeaveSubLeaveType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startTime', 'time', array(
                'widget'       => 'choice',
                'with_seconds' => false,
                'required'     => false,
                'empty_value'  => false,
                'attr'         => array(
                    'class'        => 'time-dropdown time-start-dropdown',
                ),
            ))
            ->add('endTime', 'time', array(
                'widget'       => 'choice',
                'with_seconds' => false,
                'required'     => false,
                'empty_value'  => false,
                'attr'         => array(
                    'class'        => 'time-dropdown time-end-dropdown',
                ),
            ))
            ->add('leavedate', new LeaveDateType(), array(
                'by_reference' => false,
                'required'     => true,
                'label'        => false,
            ))
        ;
    }

    public function getName()
    {
        return 'kras_krasbundle_subleavetype';
    }
}
