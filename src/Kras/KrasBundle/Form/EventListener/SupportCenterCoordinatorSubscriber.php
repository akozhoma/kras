<?php
namespace Kras\KrasBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Doctrine\ORM\EntityRepository;

class SupportCenterCoordinatorSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $is_new = !$data || !$data->getId();

        if(!$is_new) {

            $form->add('coordinator', 'entity', array(
                'class'=> 'Kras\UserBundle\Entity\User',
                'query_builder' => function (EntityRepository $er) use ($data) {
                    return $er->createQueryBuilder('u')
                        ->where('u.supportcenter = ?1')
                        ->andWhere('u.roles LIKE :roles')
                        ->setParameter('roles', '%' . 'ROLE_COORDINATOR' . '%')
                        ->setParameter(1, $data->getId());
                },
                'empty_value' => 'None',
                'required'    => false,
                'attr'        => array(
                    'class'       => 'chosen-select',
                ),
            ));
        }
    }
}
