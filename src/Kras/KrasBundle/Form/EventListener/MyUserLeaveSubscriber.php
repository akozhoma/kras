<?php
namespace Kras\KrasBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Doctrine\ORM\EntityRepository;

class MyUserLeaveSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $form
            ->add('leavetype', null, array(
                'label'         => 'Leave type',
                'required'      => true,
                'empty_value'   => false,
                'attr'          => array(
                    'class'            => 'chosen-select leave-type',
                ),
                'query_builder' => function($er) use ($data) {
                    return $er->createQueryBuilder('lt')
                        ->leftJoin('lt.allowedleaves', 'al')
                        ->leftJoin('al.employees', 'e')
                        ->where('(((e.id = ?1 OR e IS NULL) AND (al.year >= ?2)) OR (lt.overtime = TRUE))')
                        ->andWhere('(lt.plannableByEmployee = TRUE)')
                        ->setParameters(array(
                            1 => $data->getEmployees()->first(),
                            2 => strval(intval(date('Y')) - 1),
                        ));
                }
            ))
        ;
    }
}
