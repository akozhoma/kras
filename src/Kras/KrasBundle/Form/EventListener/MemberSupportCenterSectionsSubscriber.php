<?php
namespace Kras\KrasBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Doctrine\ORM\EntityRepository;

class MemberSupportCenterSectionsSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $is_new = !$data || !$data->getId();

        $form
            ->add('sections', null, array(
                'required'      => true,
                'empty_value'   => false,
                'multiple'      => true,
                'attr'          => array(
                    'class'         => 'chosen-select',
                ),
                'query_builder' => function($er) use ($data) {
                    return $er->createQueryBuilder('scs')
                        ->join('scs.supportcenter', 's')
                        ->where('scs.enabled = 1')
                        ->andWhere('s.id = :section')
                        ->setParameter('section', $data->getSupportCenter());
                }
        ));
    }
}