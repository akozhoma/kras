<?php
namespace Kras\KrasBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Doctrine\ORM\EntityRepository;

class ActivityEmployeeSubscriber implements EventSubscriberInterface
{

    private $supportcenter;

    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function __construct($supportcenter = null)
    {
        $this->supportcenter = $supportcenter;
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
        $supportcenter = $this->supportcenter;

        $form->add('employees',
            'entity',
            array(
                'class' => 'Kras\UserBundle\Entity\User',
                'query_builder' => function (EntityRepository $er) use ($supportcenter, $data)
                {
                    if ($supportcenter) {
                        return $er->createQueryBuilder('u')
                            ->where('u.supportcenter = :supportcenter')
                            /*->andWhere('u.roles LIKE :role')*/
                            /*->setParameter('role', '%' . 'ROLE_EMPLOYEE' . '%')*/
                            ->setParameter('supportcenter', $supportcenter->getId());
                    } else {
                        return $er->createQueryBuilder('u')
                            /*->where('u.roles LIKE :role')*/
                            /*->setParameter('role', '%' . 'ROLE_EMPLOYEE' . '%')*/;
                    }
                },
                'required' => true,
                'multiple' => true,
                'attr' => array(
                    'class' => 'chosen-select'
                ),
                'label' => 'Employees'
            )
        );
    }
}