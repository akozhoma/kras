<?php
namespace Kras\KrasBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Doctrine\ORM\EntityRepository;

class MemberNewcomerSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $is_new = !$data || !$data->getId();

        $form->add('newcomer', null, array(
            'label' => 'Is this member a newcomer?',
            'required' => false,
            'data' => $data->isNewcomer()
        ));
    }
}