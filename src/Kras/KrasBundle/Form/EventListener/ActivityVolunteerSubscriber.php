<?php
namespace Kras\KrasBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Doctrine\ORM\EntityRepository;

class ActivityVolunteerSubscriber implements EventSubscriberInterface
{

    private $supportcenter;

    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function __construct($supportcenter = null)
    {
        $this->supportcenter = $supportcenter;
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
        $supportcenter = $this->supportcenter;

        $form->add('volunteers',
            'entity',
            array(
                'class' => 'Kras\KrasBundle\Entity\Member',
                'query_builder' => function (EntityRepository $er) use ($supportcenter, $data)
                {
                    if ($supportcenter) {
                        return $er->createQueryBuilder('m')
                            ->where('m.supportcenter = :supportcenter')
                            ->andWhere('m.type LIKE :type')
                            ->setParameter('type', '%' . 'MEMBER_VOLUNTEER' . '%')
                            ->setParameter('supportcenter', $supportcenter->getId());
                    } else {
                        return $er->createQueryBuilder('m')
                            ->where('m.type LIKE :type')
                            ->setParameter('type', '%' . 'MEMBER_VOLUNTEER' . '%');
                    }
                },
                'required' => false,
                'multiple' => true,
                'attr' => array(
                    'class' => 'chosen-select'
                ),
                'label' => 'Volunteers'
            )
        );
    }
}