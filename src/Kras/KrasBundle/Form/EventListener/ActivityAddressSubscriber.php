<?php
namespace Kras\KrasBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Doctrine\ORM\EntityRepository;

class ActivityAddressSubscriber implements EventSubscriberInterface
{

    private $supportcenter;

    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function __construct($supportcenter = null)
    {
        $this->supportcenter = $supportcenter;
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
        $supportcenter = $this->supportcenter;

        $form->add('address',
            null,
            array(
                'required' => true,
                'label' => 'Address',
                'data' => isset($data) && $data->getAddress() ? $data->getAddress() : (isset($supportcenter) ? $supportcenter->getAddress() : null)
            )
        );

        $form->add('postalcode',
            null,
            array(
                'required' => true,
                'label' => 'Postal code',
                'attr' => array(
                    'class' => 'postalcode-input'
                ),
                'data' => isset($data) && $data->getPostalCode() ? $data->getPostalCode() : (isset($supportcenter) ? $supportcenter->getPostalCode() : null)
            )
        );

        $form->add('city',
            null,
            array(
                'required' => true,
                'label' => 'City',
                'attr' => array(
                    'class' => 'city-input'
                ),
                'data' => isset($data) && $data->getCity() ? $data->getCity() : (isset($supportcenter) ? $supportcenter->getCity() : null)
            )
        );
    }
}