<?php
namespace Kras\KrasBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityRepository;
use Kras\KrasBundle\Form\TrainingType;

class ExtendedTimeFragmentSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (!$data->getTimesheet()) {
            return;
        }

        $supportcenter = $data->getTimesheet()->getEmployee()->getSupportCenter();
        $employee = $data->getTimesheet()->getEmployee();

        $form
            ->add('activity', 'entity', array(
                'class'       => 'Kras\KrasBundle\Entity\Activity',
                'mapped'      => false,
                'required'    => false,
                'empty_value' => 'None',
                'attr'        => array(
                    'class'       => 'fragment_addon link_activity',
                ),
                'label_attr'  => array(
                    'class'       => 'fragment_addon link_activity',
                ),
                'query_builder' => function ($er) use ($supportcenter) {
                    $dt = new \DateTime('-6 months');
                    $dtwo = new \DateTime();

                    $qb = $er
                        ->createQueryBuilder('a')
                        ->where('a.start >= ?1')
                        ->orderBy('abs(?2 - a.start)', 'desc')
                        ->setParameter(1, $dt)
                        ->setParameter(2, $dtwo);

                    if ($supportcenter) {
                        $qb
                            ->andWhere('a.supportcenter = ?3')
                            ->setParameter(3, $supportcenter);
                    }

                    return $qb;
                },
            ))
            ->add('member', 'entity', array(
                'class'       => 'Kras\KrasBundle\Entity\Member',
                'mapped'      => false,
                'required'    => false,
                'empty_value' => 'None',
                'attr'        => array(
                    'class'       => 'fragment_addon link_member',
                ),
                'label_attr'  => array(
                    'class'       => 'fragment_addon link_member',
                ),
                'query_builder' => function ($er) use ($supportcenter) {
                    $qb = $er
                        ->createQueryBuilder('m');

                    if ($supportcenter) {
                        $qb
                            ->where('m.supportcenter = ?1')
                            ->setParameter(1, $supportcenter);
                    }

                    return $qb;
                },
            ))
            ->add('training', 'entity', array(
                'class'       => 'Kras\KrasBundle\Entity\Training',
                'mapped'      => false,
                'required'    => true,
                'attr'        => array(
                    'class'       => 'fragment_addon link_training'
                ),
                'label_attr'  => array(
                    'class'       => 'fragment_addon link_training',
                ),
                'query_builder' => function ($er) use ($employee) {
                    $qb = $er
                        ->createQueryBuilder('t');

                    if ($employee) {
                        $qb
                            ->where('t.employee = ?1')
                            ->andWhere('t.approved = TRUE')
                            ->setParameter(1, $employee);
                    }

                    return $qb;
                },
            ))
        ;
    }
}
