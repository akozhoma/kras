<?php
namespace Kras\KrasBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Doctrine\ORM\EntityRepository;

class ActivitySectionSubscriber implements EventSubscriberInterface
{

    private $supportcenter;

    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function __construct($supportcenter = null)
    {
        $this->supportcenter = $supportcenter;
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
        $supportcenter = $this->supportcenter;

        $form->add('sections',
            'entity',
            array(
                'class' => 'Kras\KrasBundle\Entity\SupportCenterSection',
                'query_builder' => function (EntityRepository $er) use ($supportcenter, $data)
                {
                    if ($supportcenter) {
                        return $er->createQueryBuilder('scs')
                            ->where('scs.supportcenter = :supportcenter')
                            ->andWhere('scs.enabled = 1')
                            ->setParameter('supportcenter', $supportcenter->getId());
                    } else {
                        return $er->createQueryBuilder('scs');
                    }
                },
                'required' => true,
                'multiple' => true,
                'attr' => array(
                    'class' => 'chosen-select'
                ),
                'label' => 'Sections'
            )
        );
    }
}