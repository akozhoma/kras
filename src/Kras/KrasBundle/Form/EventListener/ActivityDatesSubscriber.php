<?php
namespace Kras\KrasBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Doctrine\ORM\EntityRepository;

class ActivityDatesSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $is_new = !$data || !$data->getId();

        if(!$is_new) {
            $form->add('start', 'datetime', array(
                    'label' => 'Start',
                    'required' => true,
                    'widget' => 'single_text',
                    'attr' => array(
                        'class' => 'datetimepicker'
                    ),
                    'format' => 'YYYY/MM/dd HH:mm',
                    'read_only' => true,
            ));

            $form->add('end', 'datetime', array(
                    'label' => 'End',
                    'required' => true,
                    'widget' => 'single_text',
                    'attr' => array(
                        'class' => 'datetimepicker'
                    ),
                    'format' => 'YYYY/MM/dd HH:mm',
                    'read_only' => true,
            ));
        } else {
            $form->add('starts', 'collection', array(
                    'mapped' => false,
                    'label' => 'Start',
                    'required' => true,
                    'type' => 'datetime',
                    'allow_add' => true,
                    'allow_delete' => true,
                    'options' => array(
                        'widget' => 'single_text',
                        'attr' => array(
                            'class' => 'datetimepicker'
                        ),
                        'read_only' => true,
                    ),
                ))
            ;

            $form->add('ends', 'collection', array(
                    'mapped' => false,
                    'label' => 'End',
                    'required' => true,
                    'type' => 'datetime',
                    'allow_add' => true,
                    'allow_delete' => true,
                    'options' => array(
                        'widget' => 'single_text',
                        'attr' => array(
                            'class' => 'datetimepicker'
                        ),
                        'read_only' => true,
                    ),
                ))
            ;
        }
    }
}
