<?php
namespace Kras\KrasBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Doctrine\ORM\EntityRepository;

class AttendanceSheetMemberListener implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $sections = $data->getActivity()->getSections();

        $form->add('members',
            'entity',
            array(
                'class' => 'Kras\KrasBundle\Entity\Member',
                'query_builder' => function (EntityRepository $er) use ($sections, $data)
                {
                    $qb = $er->createQueryBuilder('m');
                    $qb->join('m.sections', 's');
                    foreach($sections as $section) {
                        $qb->orWhere('s.id = :section' . $section->getId())
                        ->setParameter('section' . $section->getId(), $section);
                    }
                    return $qb;
                },
                'required' => true,
                'multiple' => true,
                'expanded' => true,
                'label'    => 'Attendees',
                'property' => 'SectionLabel',
            )
        );
    }
}