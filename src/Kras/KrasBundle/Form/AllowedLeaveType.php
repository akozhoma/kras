<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AllowedLeaveType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('leavetype', null, array(
                'required'    => true,
                'empty_value' => false,
                'label'       => 'Leave type',
                'attr'        => array(
                    'class'       => 'chosen-select',
                ),
                'query_builder' => function($er) {
                    return $er->createQueryBuilder('lt')
                        ->where('lt.overtime = FALSE')
                        ->orderBy('lt.name', 'ASC');
                },
            ))
            ->add('employees', null, array(
                'required'    => false,
                'empty_value' => '-- All employees --',
                'attr'        => array(
                    'class'       => 'chosen-select',
                ),
                'query_builder' => function($er) {
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.label', 'ASC');
                }
            ))
            ->add('year')
            ->add('amount', null, array(
                'label' => 'Amount (hours; -1 for unlimited)',
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\AllowedLeave'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_allowedleavetype';
    }
}
