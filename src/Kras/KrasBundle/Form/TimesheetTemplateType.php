<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TimesheetTemplateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('employee', null, array(
                'required'    => false,
                'empty_value' => '--- ALL EMPLOYEES ---',
                'attr'        => array(
                    'class'       => 'chosen-select',
                ),
            ))
            ->add('name')
            ->add('start', null, array(
                'widget'   => 'single_text',
                'required' => true,
                'attr'     => array(
                    'class'    => 'datepicker',
                ),
            ))
            ->add('end', null, array(
                'widget'   => 'single_text',
                'attr'     => array(
                    'class'    => 'datepicker',
                ),
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\TimesheetTemplate'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_timesheettemplatetype';
    }
}
