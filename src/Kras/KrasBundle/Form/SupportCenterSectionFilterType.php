<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SupportCenterSectionFilterType extends AbstractType
{
    private $supportcenter;

    public function __construct($supportcenter)
    {
        $this->supportcenter = $supportcenter;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('supportcentersection', 'entity', array(
                'mapped'                => false,
                'choices'               => $this->supportcenter->getSections(),
                'class'                 => 'Kras\KrasBundle\Entity\SupportCenterSection',
                'empty_value'           => 'Filter by section..',
          //      'label'                 => 'Filter by Section',
                'label'                 => false,
                'required'              => false,
                'attr'                  => array(
                    'class'                 => 'section-filter',
                )
            ))
        ;
    }

    public function getName()
    {
        return 'sections';
    }
}
