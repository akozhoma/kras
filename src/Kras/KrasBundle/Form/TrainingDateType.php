<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TrainingDateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('start', 'time', array(
                'widget'       => 'choice',
                'with_seconds' => false,
                'required'     => true,
                'empty_value'  => false,
                'minutes'      => array(0, 15, 30, 45),
                'attr'         => array(
                    'class'        => 'time-dropdown time-start-dropdown',
                ),
            ))
            ->add('end', 'time', array(
                'widget'       => 'choice',
                'with_seconds' => false,
                'required'     => true,
                'empty_value'  => false,
                'minutes'      => array(0, 15, 30, 45),
                'attr'         => array(
                    'class'        => 'time-dropdown time-end-dropdown',
                ),
            ))
            ->add('date', 'date', array(
                'by_reference' => false,
                'required'     => true,
                'widget'       => 'single_text',
                'format'       => 'yyyy-MM-dd',
                'read_only'    => true,
                'empty_value'  => false,
                'attr'         => array(
                    'class'        => 'datepicker',
                ),
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\TrainingDate'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_trainingdatetype';
    }
}
