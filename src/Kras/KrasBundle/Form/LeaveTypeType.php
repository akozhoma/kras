<?php

namespace Kras\KrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LeaveTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('publicHoliday', null, array(
                'required' => false,
                'label'    => 'Is public holiday?',
            ))
            ->add('overtime', null, array(
                'required' => false,
                'label'    => 'Is overtime?',
            ))
            ->add('sickness', null, array(
                'required' => false,
                'label'    => 'Is sickness?',
            ))
            ->add('plannableByEmployee', null, array(
                'required' => false,
                'label'    => 'Is plannable by employee?',
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kras\KrasBundle\Entity\LeaveType'
        ));
    }

    public function getName()
    {
        return 'kras_krasbundle_leavetypetype';
    }
}
