<?php
namespace Kras\KrasBundle\EventListener;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Kras\KrasBundle\Service\EmployeeService;

class GenericListener
{
    private $session;
    private $employeeService;
    private $securityContext;

    public function setSession(Session $session)
    {
        $this->session = $session;
    }

    public function setEmployeeService(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    public function setSecurityContext(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    public function processRequest(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        $this->setLocale($event);
    }

    public function setLocale(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $locale = $request->getSession()->get('_locale');

        if(!empty($locale)) {
            $request->setLocale($request->getSession()->get('_locale'));
        }
    }
}
