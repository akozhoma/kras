<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\Member;
use Kras\KrasBundle\Form\MyMemberType;
use Kras\KrasBundle\Entity\LogEntry;
use Kras\KrasBundle\Form\MyLogEntryType;
use Kras\KrasBundle\Form\SupportCenterSectionFilterType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("/member")
 */
class MyMemberController extends Controller
{
    /**
     * @Route("/", name="kras_mymember_index")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function indexAction()
    {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $scs = $this->getRequest()->get('sections');
        $scs = intval($scs['supportcentersection']);

        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:Member');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);

            $datatable->addWhereBuilderCallback(function($qb) use ($datatable, $sc, $scs) {
                $andExpr = $qb->expr()->andX();
                $qb->innerJoin('Member.supportcenter', 'sc');
                $andExpr->add($qb->expr()->eq('sc.id', $sc->getId()));
                $qb->andWhere($andExpr);

                if ($scs > 0) {
                    $andExpr = $qb->expr()->andX();
                    $qb->innerJoin('Member.sections', 'scs');
                    $andExpr->add($qb->expr()->eq('scs.id', $scs));
                    $qb->andWhere($andExpr);
                }
            });

            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);

            array_walk($response['aaData'], function(&$data) {
                $data['attendancesheets']['id'] = empty($data['attendancesheets']['id'])
                    ? 0 : (is_array($data['attendancesheets']['id'])
                    ? count($data['attendancesheets']['id']) : 1);
            });

            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }

        $filterForm = $this->createForm(new SupportCenterSectionFilterType($sc));

        return array('supportcenter' => $sc, 'filterForm' => $filterForm->createView());
    }

    /**
     * @Route("/{id}/show", name="kras_mymember_show")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function showAction($id)
    {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();

        $dql = 'SELECT m, a, aa, s, sc
            FROM KrasKrasBundle:Member m
            JOIN m.supportcenter sc
            LEFT JOIN m.sections s
            LEFT JOIN m.attendancesheets a
            LEFT JOIN a.activity aa
            WHERE m.id = ?1
            AND sc = ?2';
        $query = $em->createQuery($dql);
        $entity = $query
            ->setParameter(1, $id)
            ->setParameter(2, $sc)
            ->getOneOrNullResult();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Member entity.');
        }

        $dql   = "SELECT le FROM KrasKrasBundle:LogEntry le WHERE le.member = " . $entity->getId() . " ORDER BY le.id DESC";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $logentries = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1)/*page number*/,
            5/*limit per page*/
        );

        $logentry = new LogEntry();
        $logentry_form = $this->createForm(new MyLogEntryType(), $logentry);

        return array('entity' => $entity, 'logentries' => $logentries, 'logentry_form' => $logentry_form->createView());
    }

    /**
     * @Route("/new", name="kras_mymember_new")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function newAction()
    {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $entity = new Member();
        $entity->setSupportCenter($sc);

        $form = $this->createForm(new MyMemberType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/create", name="kras_mymember_create")
     * @Method("POST")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template("KrasKrasBundle:MyMember:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $entity = new Member();
        $entity->setSupportCenter($sc);

        $form = $this->createForm(new MyMemberType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            $entity->setSections(new ArrayCollection());
            foreach($form->get('sections')->getData() as $section) {
                $entity->addSection($section);
            }

            $this->get('kras.member')->handleRequired($entity);

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // Log
            $this->get('kras.logging')->log($entity, 'add');

            return $this->redirect($this->generateUrl('kras_mymember_show', array('id' => $entity->getId())));
        }

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="kras_mymember_edit")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function editAction($id)
    {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Member')->findOneBy(array(
            'id' => $id,
            'supportcenter' => $sc
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Member entity.');
        }

        $editForm = $this->createForm(new MyMemberType(), $entity);

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/update", name="kras_mymember_update")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("POST")
     * @Template("KrasKrasBundle:MyMember:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Member')->findOneBy(array(
            'id' => $id,
            'supportcenter' => $sc
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Member entity.');
        }

        $editForm = $this->createForm(new MyMemberType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {

            $entity->setSections(new ArrayCollection());
            foreach($editForm->get('sections')->getData() as $section) {
                $entity->addSection($section);
            }

            $this->get('kras.member')->handleRequired($entity);

            $em->persist($entity);
            $em->flush();

            // Log
            $this->get('kras.logging')->log($entity, 'update');

            return $this->redirect($this->generateUrl('kras_mymember_show', array('id' => $id)));
        }

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/delete", name="kras_mymember_delete")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Member')->findOneBy(array(
            'id' => $id,
            'supportcenter' => $sc
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Member entity.');
        }

        // Log
        $this->get('kras.logging')->log($entity, 'delete');

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('kras_mymember_index'));
    }

    /**
     * @Route("/json_name_list", name="kras_mymember_json_name_list")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("GET")
     */
    public function jsonNameListAction() {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb->select('m.firstname', 'm.lastname');
        $qb->from('KrasKrasBundle:Member', 'm');

        $qb->where('m.supportcenter = :sc');

        $qb->setParameter(':sc', $sc);

        $result = $qb->getQuery()->getResult();

        $response = $result;

        $response = json_encode($response);
        return new Response($response, 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/map", name="kras_mymember_map")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function mapAction() {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        return array('supportcenter' => $sc);
    }

    /**
     * @Route("/json_address_list", name="kras_mymember_json_address_list")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("GET")
     */
    public function jsonAddressListAction() {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb->select('m.firstname', 'm.lastname', 'm.address', 'm.postalcode', 'm.city', 'scs.id AS section_id');
        $qb->from('KrasKrasBundle:Member', 'm');
        $qb->join('m.sections', 'scs');
        $qb->where('m.address IS NOT NULL');
        $qb->andWhere('m.postalcode IS NOT NULL');
        $qb->andWhere('m.city IS NOT NULL');
        $qb->andWhere('m.supportcenter = :sc');
        $qb->setParameter(':sc', $sc);

        $members = $qb->getQuery()->getResult();

        $supportcenter = array(
            'name' => $sc->getName(),
            'address' => $sc->getAddress(),
            'postalcode' => $sc->getPostalCode(),
            'city' => $sc->getCity()
        );

        $response = array(
            'members' => $members,
            'supportcenter' => $supportcenter
        );

        $response = json_encode($response);
        return new Response($response, 200, array('Content-Type' => 'application/json'));
    }
}
