<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\AllowedLeave;
use Kras\KrasBundle\Form\AllowedLeaveType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;

/**
 * @Route("/allowedleaves")
 */
class AllowedLeaveController extends Controller
{
    /**
     * @Route("/", name="kras_allowedleave_index")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:AllowedLeave');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);
            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }
        return array();
    }

    /**
     * @Route("/{id}/show", name="kras_allowedleave_show")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:AllowedLeave')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AllowedLeave entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/new", name="kras_allowedleave_new")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function newAction()
    {
        $entity = new AllowedLeave();
        $form = $this->createForm(new AllowedLeaveType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/create", name="kras_allowedleave_create")
     * @Method("POST")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template("KrasKrasBundle:AllowedLeave:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new AllowedLeave();
        $form = $this->createForm(new AllowedLeaveType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('kras_allowedleave_show', array('id' => $entity->getId())));
        }

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="kras_allowedleave_edit")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:AllowedLeave')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AllowedLeave entity.');
        }

        $editForm = $this->createForm(new AllowedLeaveType(), $entity);

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/update", name="kras_allowedleave_update")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST")
     * @Template("KrasKrasBundle:AllowedLeave:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:AllowedLeave')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find allowedLeave entity.');
        }

        $editForm = $this->createForm(new AllowedLeaveType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('kras_allowedleave_show', array('id' => $id)));
        }

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/delete", name="kras_allowedleave_delete")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:AllowedLeave')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AllowedLeave entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('kras_allowedleave_index'));
    }
}
