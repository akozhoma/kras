<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;

/**
 * @Route("/leaveoverview")
 */
class MyLeaveOverviewController extends Controller
{
    /**
     * @Route("/", name="kras_myleaveoverview_index")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function indexAction()
    {
        $data = $this->get('kras.employee')->getLeaveStatus($this->getUser());
        $overtime = $this->get('kras.employee')->getOvertimeStatus($this->getUser());

        return array(
            'data'     => $data,
            'overtime' => $overtime,
        );
    }
}
