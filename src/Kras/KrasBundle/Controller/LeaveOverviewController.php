<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;

/**
 * @Route("/leaveoverviews")
 */
class LeaveOverviewController extends Controller
{
    /**
     * @Route("/", name="kras_leaveoverview_index")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasUserBundle:User');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);
            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }

        return array();
    }

    /**
     * @Route("/{id}/show", name="kras_leaveoverview_show")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $data = $this->get('kras.employee')->getLeaveStatus($entity);
        $overtime = $this->get('kras.employee')->getOvertimeStatus($entity);

        return array(
            'data'     => $data,
            'entity'   => $entity,
            'overtime' => $overtime,
        );
    }
}
