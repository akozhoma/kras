<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\Training;
use Kras\KrasBundle\Form\MyTrainingType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("/training")
 */
class MyTrainingController extends Controller
{
    /**
     * @Route("/", name="kras_mytraining_index")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:Training');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);

            $user = $this->getUser();
            $datatable->addWhereBuilderCallback(function($qb) use ($datatable, $user) {
                $andExpr = $qb->expr()->andX();
                $andExpr->add($qb->expr()->eq('Training.employee', $user->getId()));
                $qb->andWhere($andExpr);
            });

            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }

        return array();
    }

    /**
     * @Route("/{id}/show", name="kras_mytraining_show")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Training')->findOneBy(array(
            'id'       => $id,
            'employee' => $this->getUser(),
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Training entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/new", name="kras_mytraining_new")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Training();
        $entity->setEmployee($this->getUser());

        $form = $this->createForm(new MyTrainingType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/create", name="kras_mytraining_create")
     * @Method("POST")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template("KrasKrasBundle:MyTraining:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Training();
        $entity->setEmployee($this->getUser());

        $form = $this->createForm(new MyTrainingType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($form->get('trainingDates')->getData()->isEmpty()) {
                $request->getSession()->getFlashBag()->add(
                    'error',
                    $this->get('translator')->trans('You can\'t register training without choosing a date and a duration of at least an hour.')
                );

                return array('entity' => $entity, 'form' => $form->createView());
            }

            foreach ($form->get('trainingDates')->getData() as $trainingDate) {
                if (!$trainingDate->getDate() || (($trainingDate->getEnd()->format('U') - $trainingDate->getStart()->format('U')) < 3600)) {
                    $request->getSession()->getFlashBag()->add(
                        'error',
                        $this->get('translator')->trans('You can\'t register training without choosing a date and a duration of at least an hour.')
                    );

                    return array('entity' => $entity, 'form' => $form->createView());
                }
            }

            $em->persist($entity);
            $em->flush();

            // Log
            $this->get('kras.logging')->log($entity, 'add');

            return $this->redirect($this->generateUrl('kras_mytraining_show', array('id' => $entity->getId())));
        }

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="kras_mytraining_edit")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->createQuery('SELECT t FROM KrasKrasBundle:Training t WHERE t.id = ?1 AND t.employee = ?2 AND t.approved IS NULL')
            ->setParameter(1, $id)->setParameter(2, $this->getUser())->getOneOrNullResult();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Training entity.');
        }

        $editForm = $this->createForm(new MyTrainingType(), $entity);

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/update", name="kras_mytraining_update")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("POST")
     * @Template("KrasKrasBundle:MyTraining:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->createQuery('SELECT t FROM KrasKrasBundle:Training t WHERE t.id = ?1 AND t.employee = ?2 AND t.approved IS NULL')
            ->setParameter(1, $id)->setParameter(2, $this->getUser())->getOneOrNullResult();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Training entity.');
        }

        $editForm = $this->createForm(new MyTrainingType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            if ($editForm->get('trainingDates')->getData()->isEmpty()) {
                $request->getSession()->getFlashBag()->add(
                    'error',
                    $this->get('translator')->trans('You can\'t register training without choosing a date and a duration of at least an hour.')
                );

                return array('entity' => $entity, 'form' => $editForm->createView());
            }

            foreach ($editForm->get('trainingDates')->getData() as $trainingDate) {
                if (!$trainingDate->getDate() || (($trainingDate->getEnd()->format('U') - $trainingDate->getStart()->format('U')) < 3600)) {
                    $request->getSession()->getFlashBag()->add(
                        'error',
                        $this->get('translator')->trans('You can\'t register training without choosing a date and a duration of at least an hour.')
                    );

                    return array('entity' => $entity, 'form' => $editForm->createView());
                }
            }

            $em->persist($entity);
            $em->flush();

            // Log
            $this->get('kras.logging')->log($entity, 'update');

            return $this->redirect($this->generateUrl('kras_mytraining_show', array('id' => $id)));
        }

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/delete", name="kras_mytraining_delete")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->createQuery('SELECT t FROM KrasKrasBundle:Training t WHERE t.id = ?1 AND t.employee = ?2 AND t.approved IS NULL')
            ->setParameter(1, $id)->setParameter(2, $this->getUser())->getOneOrNullResult();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Training entity.');
        }

        // Log
        $this->get('kras.logging')->log($entity, 'delete');

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('kras_mytraining_index'));
    }


    /**
     * @Route("/overview", name="kras_mytraining_overview")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template("KrasKrasBundle:MyTraining:overview.html.twig")
     * @Method("POST|GET")
     */
    public function overviewAction(Request $request)
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:Training');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);

            $user = $this->getUser();
            $datatable->addWhereBuilderCallback(function($qb) use ($datatable, $user) {
                    $qb->innerJoin('Training.employee', 'Employee');
                    $qb->innerJoin('Training.trainingDates', 'TrainingDates');
                    $andExpr = $qb->expr()->andX();
                    $andExpr->add($qb->expr()->eq('Employee.supportcenter', $user->getSupportcenter()->getId()));
                    $qb->andWhere($andExpr);
                });

            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }

        return array();
    }

}
