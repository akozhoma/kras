<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\Member;
use Kras\KrasBundle\Form\MemberType;
use Kras\KrasBundle\Entity\LogEntry;
use Kras\KrasBundle\Form\LogEntryType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("/members")
 */
class MemberController extends Controller
{
    /**
     * @Route("/", name="kras_member_index")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:Member');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);
            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);

            array_walk($response['aaData'], function(&$data) {
                $data['attendancesheets']['id'] = empty($data['attendancesheets']['id'])
                    ? 0 : (is_array($data['attendancesheets']['id'])
                    ? count($data['attendancesheets']['id']) : 1);
            });

            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }
        return array();
    }

    /**
     * @Route("/{id}/show", name="kras_member_show")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $dql = 'SELECT m, a, aa, s, sc
            FROM KrasKrasBundle:Member m
            LEFT JOIN m.supportcenter sc
            LEFT JOIN m.sections s
            LEFT JOIN m.attendancesheets a
            LEFT JOIN a.activity aa
            WHERE m.id = ?1';
        $query = $em->createQuery($dql);
        $entity = $query->setParameter(1, $id)->getOneOrNullResult();
        //$entity = $em->getRepository('KrasKrasBundle:Member')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Member entity.');
        }

        $dql = "SELECT le FROM KrasKrasBundle:LogEntry le WHERE le.member = " . $entity->getId() . " ORDER BY le.id DESC";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $logentries = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1)/*page number*/,
            5/*limit per page*/
        );

        $logentry = new LogEntry();
        $logentry_form = $this->createForm(new LogEntryType(), $logentry);

        return array('entity' => $entity, 'logentries' => $logentries, 'logentry_form' => $logentry_form->createView());
    }

    /**
     * @Route("/new", name="kras_member_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Member();
        $form = $this->createForm(new MemberType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/create", name="kras_member_create")
     * @Method("POST")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("KrasKrasBundle:Member:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Member();
        $form = $this->createForm(new MemberType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            $entity->setSections(new ArrayCollection());
            foreach($form->get('sections')->getData() as $section) {
                $entity->addSection($section);
            }

            $this->get('kras.member')->handleRequired($entity);

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // Log
            $this->get('kras.logging')->log($entity, 'add');

            return $this->redirect($this->generateUrl('kras_member_show', array('id' => $entity->getId())));
        }

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="kras_member_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Member')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Member entity.');
        }

        $editForm = $this->createForm(new MemberType(), $entity);

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/update", name="kras_member_update")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("KrasKrasBundle:Member:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Member')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Member entity.');
        }

        $editForm = $this->createForm(new MemberType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {

            $entity->setSections(new ArrayCollection());
            foreach($editForm->get('sections')->getData() as $section) {
                $entity->addSection($section);
            }

            $this->get('kras.member')->handleRequired($entity);

            $em->persist($entity);
            $em->flush();

            // Log
            $this->get('kras.logging')->log($entity, 'update');

            return $this->redirect($this->generateUrl('kras_member_show', array('id' => $id)));
        }

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/delete", name="kras_member_delete")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Member')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Member entity.');
        }

        // Log
        $this->get('kras.logging')->log($entity, 'delete');

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('kras_member_index'));
    }

    /**
     * @Route("/json_name_list", name="kras_member_json_name_list")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     */
    public function jsonNameListAction() {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb->select('m.firstname', 'm.lastname');
        $qb->from('KrasKrasBundle:Member', 'm');

        $result = $qb->getQuery()->getResult();

        $response = $result;

        $response = json_encode($response);
        return new Response($response, 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/map", name="kras_member_map")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function mapAction() {
        return array();
    }

    /**
     * @Route("/json_address_list", name="kras_member_json_address_list")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     */
    public function jsonAddressListAction() {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb->select('m.firstname', 'm.lastname', 'm.address', 'm.postalcode', 'm.city');
        $qb->from('KrasKrasBundle:Member', 'm');
        $qb->where('m.address IS NOT NULL');
        $qb->andWhere('m.postalcode IS NOT NULL');
        $qb->andWhere('m.city IS NOT NULL');

        $members = $qb->getQuery()->getResult();

        $qb = $em->createQueryBuilder();
        $qb->select('sc.name', 'sc.address', 'sc.postalcode', 'sc.city');
        $qb->from('KrasKrasBundle:SupportCenter', 'sc');
        $qb->where('sc.address IS NOT NULL');
        $qb->andWhere('sc.postalcode IS NOT NULL');
        $qb->andWhere('sc.city IS NOT NULL');

        $supportcenters = $qb->getQuery()->getResult();

        $response = array(
            'members' => $members,
            'supportcenters' => $supportcenters
        );

        $response = json_encode($response);
        return new Response($response, 200, array('Content-Type' => 'application/json'));
    }
}