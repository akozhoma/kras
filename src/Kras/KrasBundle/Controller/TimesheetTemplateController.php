<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\TimesheetTemplate;
use Kras\KrasBundle\Form\TimesheetTemplateType;
use Kras\KrasBundle\Entity\ExtendedTimeFragment;
use Kras\KrasBundle\Form\TimesheetTemplateExtendedTimeFragmentType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;

/**
 * @Route("/timesheettemplates")
 */
class TimesheetTemplateController extends Controller
{
    /**
     * @Route("/", name="kras_timesheettemplate_index")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $user = $this->getUser();

            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:TimesheetTemplate');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);
            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }

        $timesheettemplate = $this->get('kras.employee')->getActiveTimesheetTemplate();

        return array('timesheettemplate' => $timesheettemplate);
    }

    /**
     * @Route("/{id}/show", name="kras_timesheettemplate_show")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:TimesheetTemplate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TimesheetTemplate entity.');
        }

        $frag = new ExtendedTimeFragment();
        $form = $this->createForm(new TimesheetTemplateExtendedTimeFragmentType(), $frag);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/new", name="kras_timesheettemplate_new")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function newAction()
    {
        $entity = new TimesheetTemplate();

        $form = $this->createForm(new TimesheetTemplateType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/create", name="kras_timesheettemplate_create")
     * @Method("POST")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template("KrasKrasBundle:TimesheetTemplate:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new TimesheetTemplate();

        $form = $this->createForm(new TimesheetTemplateType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $dql = 'SELECT etf FROM KrasKrasBundle:ExtendedTimeFragment etf INNER JOIN etf.timesheettemplate tt WHERE tt.employee IS NULL';
            $query = $em->createQuery($dql);
            $tfs = $query->getResult();

            foreach ($tfs as $tf) {
                $tf = clone $tf;
                $tf->setTimesheetTemplate($entity);
                $entity->addExtendedTimeFragment($tf);
            }

            $em->persist($entity);
            $em->flush();

            // Log
            $this->get('kras.logging')->log($entity, 'add');

            return $this->redirect($this->generateUrl('kras_timesheettemplate_show', array('id' => $entity->getId())));
        }

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/{id}/delete", name="kras_timesheettemplate_delete")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $dql = 'SELECT tt FROM KrasKrasBundle:TimesheetTemplate tt WHERE tt.id = ?1 AND tt.employee IS NOT NULL';
        $query = $em->createQuery($dql);
        $query
            ->setParameter(1, $id);
        $entity = $query->getOneOrNullResult();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TimesheetTemplate entity.');
        }

        if ($entity->getEmployee() == null) {
            return $this->redirect($this->generateUrl('kras_timesheettemplate_index'));
        }

        // Log
        $this->get('kras.logging')->log($entity, 'delete');

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('kras_timesheettemplate_index'));
    }

    /**
     * @Route("/{id}/events", name="kras_timesheettemplate_event_list")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST|GET")
     */
    public function eventListAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:TimesheetTemplate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TimesheetTemplate entity.');
        }

        $dql = 'SELECT etf FROM
            KrasKrasBundle:ExtendedTimeFragment etf
            WHERE etf.timesheettemplate = ?1
            ORDER BY etf.start ASC';
        $query = $em->createQuery($dql);
        $entities = $query
            ->setParameter(1, $id)
            ->getResult();

        $output = $this->get('jms_serializer')->serialize($entities, 'json');

        return new Response($output, 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/events/create", name="kras_timesheettemplate_event_create")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST|GET")
     */
    public function eventCreateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:TimesheetTemplate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TimesheetTemplate entity.');
        }

        $fragment = new ExtendedTimeFragment();
        $form = $this->createForm(new TimesheetTemplateExtendedTimeFragmentType(), $fragment);

        $form->bind($request);

        if (!$form->isValid()) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $fragment->setTimesheetTemplate($entity);

        $start = new \DateTime();
        while($start->format('N') != $form->get('day_of_week')->getData()) {
            $start->modify('-1 day');
        }

        $start_time_data = $form->get('start_time')->getData();
        $start->setTime($start_time_data['hour'], $start_time_data['minute'], 0);

        $end = clone $start;
        $end_time_data = $form->get('end_time')->getData();
        $end->setTime($end_time_data['hour'], $end_time_data['minute'], 0);

        if ($end->format('U') <= $start->format('U')) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $fragment
            ->setStart($start)
            ->setEnd($end);

        $em->persist($fragment);
        $em->flush();

        $em->refresh($entity);
        $entity->calculateRequiredHours();
        $em->persist($entity);
        $em->flush();

        return new Response('OK', 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/events/{event_id}/delete", name="kras_timesheettemplate_event_delete")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST|GET")
     */
    public function eventDeleteAction(Request $request, $id, $event_id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:ExtendedTimeFragment')->findOneBy(array(
            'id'        => $event_id,
            'timesheettemplate' => $id,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ExtendedTimeFragment entity.');
        }

        $tt = $entity->getTimesheetTemplate();

        $em->remove($entity);
        $em->flush();

        $tt->calculateRequiredHours();
        $em->persist($tt);
        $em->flush();

        return new Response('OK', 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/approve", name="kras_timesheettemplate_approve")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST|GET")
     */
    public function approveAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:TimesheetTemplate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TimesheetTemplate entity.');
        }

        $entity->setApproved(true);

        $em->persist($entity);
        $em->flush();

        $request->getSession()->getFlashBag()->add(
            'success',
            $this->get('translator')->trans('This timesheet template has been APPROVED successfully.')
        );

        return $this->redirect($this->generateUrl('kras_timesheettemplate_show', array('id' => $entity->getId())));
    }

    /**
     * @Route("/{id}/deny", name="kras_timesheettemplate_deny")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST|GET")
     */
    public function denyAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:TimesheetTemplate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TimesheetTemplate entity.');
        }

        $entity->setApproved(false);

        $em->persist($entity);
        $em->flush();

        $request->getSession()->getFlashBag()->add(
            'success',
            $this->get('translator')->trans('This timesheet template has been DENIED successfully.')
        );

        return $this->redirect($this->generateUrl('kras_timesheettemplate_show', array('id' => $entity->getId())));
    }
}
