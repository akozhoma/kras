<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\Training;
use Kras\KrasBundle\Form\TrainingType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("/trainings")
 */
class TrainingController extends Controller
{
    /**
     * @Route("/", name="kras_training_index")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:Training');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);
            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }

        return array();
    }

    /**
     * @Route("/{id}/show", name="kras_training_show")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Training')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Training entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/new", name="kras_training_new")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Training();
        $entity->setEmployee($this->getUser());

        $form = $this->createForm(new TrainingType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/create", name="kras_training_create")
     * @Method("POST")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template("KrasKrasBundle:Training:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Training();
        $entity->setEmployee($this->getUser());

        $form = $this->createForm(new TrainingType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($form->get('trainingDates')->getData()->isEmpty()) {
                $request->getSession()->getFlashBag()->add(
                    'error',
                    $this->get('translator')->trans('You can\'t register training without choosing a date and a duration of at  least an hour.')
                );

                return array('entity' => $entity, 'form' => $form->createView());
            }

            foreach ($form->get('trainingDates')->getData() as $trainingDate) {
                if (!$trainingDate->getDate() || (($trainingDate->getEnd()->format('U') - $trainingDate->getStart()->format('U')) < 3600)) {
                    $request->getSession()->getFlashBag()->add(
                        'error',
                        $this->get('translator')->trans('You can\'t register training without choosing a date and a duration of at least an hour.')
                    );

                    return array('entity' => $entity, 'form' => $form->createView());
                }
            }

            $em->persist($entity);
            $em->flush();

            // Log
            $this->get('kras.logging')->log($entity, 'add');

            return $this->redirect($this->generateUrl('kras_training_show', array('id' => $entity->getId())));
        }

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="kras_training_edit")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Training')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Training entity.');
        }

        $editForm = $this->createForm(new TrainingType(), $entity);

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/update", name="kras_training_update")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST")
     * @Template("KrasKrasBundle:Training:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Training')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Training entity.');
        }

        $editForm = $this->createForm(new TrainingType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            if ($editForm->get('trainingDates')->getData()->isEmpty()) {
                $request->getSession()->getFlashBag()->add(
                    'error',
                    $this->get('translator')->trans('You can\'t register training without choosing a date and a duration of at least an hour.')
                );

                return array('entity' => $entity, 'form' => $editForm->createView());
            }

            foreach ($editForm->get('trainingDates')->getData() as $trainingDate) {
                if (!$trainingDate->getDate() || (($trainingDate->getEnd()->format('U') - $trainingDate->getStart()->format('U')) < 3600)) {
                    $request->getSession()->getFlashBag()->add(
                        'error',
                        $this->get('translator')->trans('You can\'t register training without choosing a date and a duration of at least an hour.')
                    );

                    return array('entity' => $entity, 'form' => $editForm->createView());
                }
            }

            $em->persist($entity);
            $em->flush();

            // Log
            $this->get('kras.logging')->log($entity, 'update');

            return $this->redirect($this->generateUrl('kras_training_show', array('id' => $id)));
        }

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/delete", name="kras_training_delete")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Training')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Training entity.');
        }

        // Log
        $this->get('kras.logging')->log($entity, 'delete');

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('kras_training_index'));
    }


    /**
     * @Route("/getorganizers", name="kras_training_get_organizers_ajax")
     * @Secure(roles="ROLE_USER")
     */
    public function getOrganizersAjaxAction(Request $request)
    {

        $value = $request->get('term');

        $em = $this->getDoctrine()->getManager();
        $dql = 'SELECT t.organizer
                FROM KrasKrasBundle:Training t
                WHERE t.organizer LIKE ?1
                GROUP BY t.organizer';
        $query = $em->createQuery($dql)->setParameters(
            array(
                1 => '%'.$value."%",
            )
        );
        $search = $query->getResult();


        $searchFinResult = "";

        if(isset($search) and !empty($search)){
            foreach ($search as $singleResult) {

                $searchFinResult[] = $singleResult['organizer'];
            }
        }
        $response = new Response();
        $response->setContent(json_encode($searchFinResult));

        return $response;
    }
}
