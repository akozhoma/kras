<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\SupportCenter;
use Kras\KrasBundle\Form\SupportCenterType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;

/**
 * @Route("/supportcenters")
 */
class SupportCenterController extends Controller
{
    /**
     * @Route("/", name="kras_supportcenter_index")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:SupportCenter');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);
            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }
        return array();
    }

    /**
     * @Route("/{id}/show", name="kras_supportcenter_show")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:SupportCenter')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SupportCenter entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/new", name="kras_supportcenter_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function newAction()
    {
        $entity = new SupportCenter();
        $form = $this->createForm(new SupportCenterType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/create", name="kras_supportcenter_create")
     * @Method("POST")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("KrasKrasBundle:SupportCenter:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new SupportCenter();
        $form = $this->createForm(new SupportCenterType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            foreach($form->get('sections')->getData() as $section) {
                $section->setSupportCenter($entity);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('kras_supportcenter_show', array('id' => $entity->getId())));
        }

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="kras_supportcenter_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:SupportCenter')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SupportCenter entity.');
        }

        $editForm = $this->createForm(new SupportCenterType(), $entity);

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/update", name="kras_supportcenter_update")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("KrasKrasBundle:SupportCenter:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:SupportCenter')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SupportCenter entity.');
        }

        $original_coordinator = $entity->getCoordinator();

        $editForm = $this->createForm(new SupportCenterType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {

            foreach($editForm->get('sections')->getData() as $section) {
                $section->setSupportCenter($entity);
            }

            $new_coordinator = $editForm->get('coordinator')->getData();

            if($original_coordinator !== $new_coordinator) {
                if($original_coordinator) {
                    $original_coordinator->setCoordinating(null);
                    $em->persist($original_coordinator);
                    $em->flush();
                }
                if($new_coordinator) {
                    if($new_coordinator->getCoordinating()) {
                        $new_coordinator->getCoordinating()->setCoordinator(null);
                        $em->persist($new_coordinator->getCoordinating());
                    }
                    $new_coordinator->setCoordinating($entity);
                    $em->persist($new_coordinator);
                }
            }

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('kras_supportcenter_show', array('id' => $id)));
        }

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/delete", name="kras_supportcenter_delete")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:SupportCenter')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SupportCenter entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('kras_supportcenter_index'));
    }
}
