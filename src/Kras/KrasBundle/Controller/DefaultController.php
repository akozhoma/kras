<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="kras_index")
     * @Template()
     */
    public function indexAction()
    {
        $sc = $this->getUser()->getSupportCenter();
        if($sc != null && $this->get('security.context')->isGranted('ROLE_EMPLOYEE')) {
            return $this->redirect($this->generateUrl('kras_mydashboard_index'));
        }
        return array();
    }

    public function localeAction($locale)
    {
        $request = $this->getRequest();

        $this->get('session')->set('_locale', $locale);

        $referer_url = $this->get('request')->headers->get('referer');
        if ($referer_url != null) {
            return $this->redirect($referer_url);
        } else {
            return $this->redirect($this->generateUrl('kras_index'));
        }
    }

    /**
     * @Route("/cache_clear", name="kras_cache_clear")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function cacheClearAction()
    {
        /**
         * This function can be only called by administrators by visiting SYMFONY_ROOT/admin/cache/clear.
         * It'll attempt to delete all subfolders and files of SYMFONY_ROOT/app/cache/ENVIRONMENT.
         */
        function recursiveDelete($str)
        {
            if (is_file($str)) {
                return @unlink($str);
            } elseif (is_dir($str)) {
                $scan = glob(rtrim($str, '/') . '/*');
                foreach ($scan as $index => $path) {
                    recursiveDelete($path);
                }
                return @rmdir($str);
            }
        }

        // Define the path to the cache folder of the current environment
        $cache_folder = $this->get('kernel')->getRootDir() . '/cache/' . $this->container->getParameter('kernel.environment');

        // Deus ex machina goes here
        try {
            recursiveDelete($cache_folder . '/twig');
            recursiveDelete($cache_folder . '/translations');
            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        // Set a flash message to indicate whether or not we succeeded in deleting the cache
        $this->get('session')->getFlashBag()
        ->add($success ? 'notice' : 'error',
                $success ? 'The templating and translation cache folders at ' . $cache_folder . ' have been emptied.' : 'Could not empty templating and translation cache folders at <b>' . $cache_folder . '</b>.');

        // Redirect to previous page or homepage
        $referer_url = $this->get('request')->headers->get('referer');
        return $this->redirect($referer_url != null ? $referer_url : $this->generateUrl('kras_index'));
    }

    /**
     * @Route("/feedback", name="kras_feedback")
     * @Method("POST")
     * @Template()
     */
    public function feedbackAction(Request $request)
    {
        // Redirect to previous page or homepage
        $referer_url = $this->get('request')->headers->get('referer');
        $redir_target = $referer_url != null ? $referer_url : $this->generateUrl('kras_index');

        $message = $request->get('message');

        if (empty($message)) {
            $this->get('session')->getFlashBag()
                ->add('error', 'You cannot submit feedback without a feedback message.');
            return $this->redirect($redir_target);
        }

        $this->get('kras.mailer')->sendFeedbackToSuperAdmins($message, array('referer' => $referer_url));

        $this->get('session')->getFlashBag()
            ->add('success', 'Your feedback has been submitted.');

        return $this->redirect($redir_target);
    }
}
