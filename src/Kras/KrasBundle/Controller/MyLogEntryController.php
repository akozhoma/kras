<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\LogEntry;
use Kras\KrasBundle\Form\MyLogEntryType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;

/**
 * @Route("/logentry")
 */
class MyLogEntryController extends Controller
{
    /**
     * @Route("/create_ajax", name="kras_mylogentry_create_ajax")
     * @Method("POST")
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function ajaxCreateAction(Request $request)
    {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $member_id = $request->get('member_id');

        $em = $this->getDoctrine()->getManager();
        $member = $em->getRepository('KrasKrasBundle:Member')->findOneBy(array(
            'id' => $member_id,
            'supportcenter' => $sc
        ));

        if($member) {

            $entity = new LogEntry();
            $entity->setMember($member);
            $entity->setEmployee($this->getUser());

            $form = $this->createForm(new MyLogEntryType(), $entity);
            $form->bind($request);


            if ($form->isValid()) {

                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                return new Response(json_encode("OK"), 200, array('Content-Type' => 'application/json'));
            }

        }

        return new Response(json_encode("ERROR"), 500, array('Content-Type' => 'application/json'));
    }
}
