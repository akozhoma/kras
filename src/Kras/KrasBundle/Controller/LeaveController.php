<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\UserLeave;
use Kras\KrasBundle\Form\UserLeaveType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;

/**
 * @Route("/leaves")
 */
class LeaveController extends Controller
{
    /**
     * @Route("/", name="kras_leave_index")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:UserLeave');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);
            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }
        return array();
    }

    /**
     * @Route("/{id}/show", name="kras_leave_show")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:UserLeave')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Leave entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/new", name="kras_leave_new")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function newAction()
    {
        $entity = new UserLeave();
        $form = $this->createForm(new UserLeaveType(), $entity);

        $leave_types = $this->getDoctrine()->getManager()->getRepository('KrasKrasBundle:LeaveType')->findAll();

        return array('entity' => $entity, 'form' => $form->createView(), 'leave_types' => $leave_types);
    }

    /**
     * @Route("/create", name="kras_leave_create")
     * @Method("POST")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template("KrasKrasBundle:Leave:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new UserLeave();
        $form = $this->createForm(new UserLeaveType(), $entity);
        $form->submit($request);

        $em = $this->getDoctrine()->getManager();

        $leave_types = $em->getRepository('KrasKrasBundle:LeaveType')->findAll();

        if ($form->isValid()) {

            if ($form->get('leavedates')->getData()->isEmpty()) {
                $request->getSession()->getFlashBag()->add(
                    'error',
                    $this->get('translator')->trans('You can\'t schedule leave without choosing a date.')
                );

                return array('entity' => $entity, 'form' => $form->createView());
            }

            if ($form->get('leavetype')->getData()->getPublicHoliday() || $form->get('leavetype')->getData()->getSickness()) {
                $entity
                    ->setStartTime(null)
                    ->setEndTime(null);
            } else {
                $year = $entity->getLeaveDates()->first()->getDate()->format('Y');

//                if (($form->get('endTime')->getData()->format('U') - $form->get('startTime')->getData()->format('U')) <  3600) {
//                    $request->getSession()->getFlashBag()->add(
//                        'error',
//                        $this->get('translator')->trans('Leave must be scheduled for at least an hour.')
//                    );
//
//                    return array('entity' => $entity, 'form' => $form->createView(), 'leave_types' => $leave_types);
//                }

                foreach ($form->get('employees')->getData() as $employee) {
                    if ((!$form->get('leavetype')->getData()->getOvertime() && ((($form->get('endTime')->getData()->format('U') - $form->get('startTime')->getData()->format('U')) * $form->get('leavedates')->getData()->count()) / 3600) > $this->get('kras.employee')->getRemainingLeave($form->get('leavetype')->getData(), $employee, null, $year))
                        || ($form->get('leavetype')->getData()->getOvertime() && ((($form->get('endTime')->getData()->format('U') - $form->get('startTime')->getData()->format('U')) * $form->get('leavedates')->getData()->count()) / 3600) > $this->get('kras.employee')->getRemainingOvertime($employee))) {
                        $request->getSession()->getFlashBag()->add(
                            'error',
                            $this->get('translator')->trans('The employee "%employee%" is not allowed to take this much leave.', array('%employee%' => $employee))
                        );

                        return array('entity' => $entity, 'form' => $form->createView(), 'leave_types' => $leave_types);
                    }

                    if ($this->get('kras.employee')->hasLeaveScheduled($form->get('leavedates')->getData(), $form->get('startTime')->getData(), $form->get('endTime')->getData(), $employee)) {
                        $request->getSession()->getFlashBag()->add(
                            'error',
                            $this->get('translator')->trans('The employee "%employee%" already has leave scheduled on one of the dates entered.', array('%employee%' => $employee))
                        );

                        return array('entity' => $entity, 'form' => $form->createView(), 'leave_types' => $leave_types);
                    }
                }
            }

            $entity->setApproved(true);

            $em->persist($entity);
            $em->flush();

            $this->get('kras.employee')->recalculateTimesheetWorkedHoursByLeave($entity);
            $em->flush();

            if ($entity->getApproved()) {
                if ($form->get('leavetype')->getData()->getOvertime()) {
                    $em->refresh($entity);

                    foreach ($entity->getEmployees() as $employee) {
                        $this->get('kras.employee')->scheduleOvertime(
                            $employee,
                            ($entity->getDuration() / $entity->getLeaveDates()->count()),
                            $entity->getLeaveDates()->toArray()
                        );
                    }
                }
            }

            return $this->redirect($this->generateUrl('kras_leave_show', array('id' => $entity->getId())));
        }

        return array('entity' => $entity, 'form' => $form->createView(), 'leave_types' => $leave_types);
    }

    /**
     * @Route("/{id}/edit", name="kras_leave_edit")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:UserLeave')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserLeave entity.');
        }

        $editForm = $this->createForm(new UserLeaveType(), $entity);

        $leave_types = $em->getRepository('KrasKrasBundle:LeaveType')->findAll();

        return array('entity' => $entity, 'form' => $editForm->createView(), 'leave_types' => $leave_types);
    }

    /**
     * @Route("/{id}/update", name="kras_leave_update")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST")
     * @Template("KrasKrasBundle:Leave:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:UserLeave')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserLeave entity.');
        }

        $editForm = $this->createForm(new UserLeaveType(), $entity);
        $editForm->submit($request);

        $leave_types = $em->getRepository('KrasKrasBundle:LeaveType')->findAll();

        if ($editForm->isValid()) {
            $year = $entity->getLeaveDates()->first()->getDate()->format('Y');

            if ($editForm->get('leavedates')->getData()->isEmpty()) {
                $request->getSession()->getFlashBag()->add(
                    'error',
                    $this->get('translator')->trans('You can\'t schedule leave without choosing a date.')
                );

                return array('entity' => $entity, 'form' => $editForm->createView(), 'leave_types' => $leave_types);
            }

            if ($editForm->get('leavetype')->getData()->getPublicHoliday() || $editForm->get('leavetype')->getData()->getSickness()) {
                $entity
                    ->setStartTime(null)
                    ->setEndTime(null);
            } else {
                if (($editForm->get('endTime')->getData()->format('U') - $editForm->get('startTime')->getData()->format('U')) <  3600) {
                    $request->getSession()->getFlashBag()->add(
                        'error',
                        $this->get('translator')->trans('Leave must be scheduled for at least an hour.')
                    );

                    return array('entity' => $entity, 'form' => $editForm->createView(), 'leave_types' => $leave_types);
                }

                foreach ($entity->getEmployees() as $employee) {
                    if ((!$editForm->get('leavetype')->getData()->getOvertime() && (((($editForm->get('endTime')->getData()->format('U') - $editForm->get('startTime')->getData()->format('U')) * $editForm->get('leavedates')->getData()->count()) / 3600) > ($this->get('kras.employee')->getRemainingLeave($editForm->get('leavetype')->getData(), $employee, $entity->getId(), $year))))
                        || ($editForm->get('leavetype')->getData()->getOvertime() && (((($editForm->get('endTime')->getData()->format('U') - $editForm->get('startTime')->getData()->format('U')) * $editForm->get('leavedates')->getData()->count()) / 3600) > $this->get('kras.employee')->getRemainingOvertime($employee, $entity->getId())))) {
                        $request->getSession()->getFlashBag()->add(
                            'error',
                            $this->get('translator')->trans('The employee "%employee%" is not allowed to take this much leave.', array('%employee%' => $employee))
                        );

                        return array('entity' => $entity, 'form' => $editForm->createView(), 'leave_types' => $leave_types);
                    }

                    if ($this->get('kras.employee')->hasLeaveScheduled($editForm->get('leavedates')->getData(), $editForm->get('startTime')->getData(), $editForm->get('endTime')->getData(), $employee, $entity->getId())) {
                        $request->getSession()->getFlashBag()->add(
                            'error',
                            $this->get('translator')->trans('The employee "%employee%" already has leave scheduled on one of the dates entered.', array('%employee%' => $employee))
                        );

                        return array('entity' => $entity, 'form' => $editForm->createView(), 'leave_types' => $leave_types);
                    }
                }
            }

            $leave_is_approved_overtime = false;
            if ($entity->getApproved()) {
                $entity2 = $em->getRepository('KrasKrasBundle:UserLeave')->find($id);
                //$em->detach($entity2);

                if ($entity2->getLeaveType()->getOvertime()) {
                    $leave_is_approved_overtime = true;
                    $leave_duration = $entity2->getDuration() / $entity2->getLeaveDates()->count();
                    $leave_dates = $entity2->getLeaveDates()->toArray();
                    $leave_employees = $entity2->getEmployees();
                }
            }

            $em->persist($entity);
            $em->flush();

            $this->get('kras.employee')->recalculateTimesheetWorkedHoursByLeave($entity);
            $em->flush();

            if ($leave_is_approved_overtime) {
                foreach ($leave_employees as $employee) {
                    $this->get('kras.employee')->unscheduleOvertime(
                        $employee,
                        $leave_duration,
                        $leave_dates
                    );
                }
            }

            if ($entity->getApproved() && $editForm->get('leavetype')->getData()->getOvertime()) {
                $em->refresh($entity);

                foreach ($entity->getEmployees() as $employee) {
                    $this->get('kras.employee')->scheduleOvertime(
                        $employee,
                        ($entity->getDuration() / $entity->getLeaveDates()->count()),
                        $entity->getLeaveDates()->toArray()
                    );
                }
            }

            return $this->redirect($this->generateUrl('kras_leave_show', array('id' => $id)));
        }

        return array('entity' => $entity, 'form' => $editForm->createView(), 'leave_types' => $leave_types);
    }

    /**
     * @Route("/{id}/delete", name="kras_leave_delete")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:UserLeave')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserLeave entity.');
        }

        $leave_is_approved_overtime = false;
        if ($entity->getApproved() && $entity->getLeaveType()->getOvertime()) {
            $leave_is_approved_overtime = true;
            $leave_duration = $entity->getDuration() / $entity->getLeaveDates()->count();
            $leave_dates = $entity->getLeaveDates()->toArray();
            $leave_employees = $entity->getEmployees();
        }

        $timesheets = $this->get('kras.employee')->getTimesheetsByLeave($entity);

        $em->remove($entity);
        $em->flush();

        if ($leave_is_approved_overtime) {
            foreach ($leave_employees as $employee) {
                $this->get('kras.employee')->unscheduleOvertime(
                    $employee,
                    $leave_duration,
                    $leave_dates
                );
            }
        }

        $this->get('kras.employee')->recalculateTimesheetsWorkedHours($timesheets);
        $em->flush();

        return $this->redirect($this->generateUrl('kras_leave_index'));
    }

    /**
     * @Route("/{identifier}/approve", name="kras_leave_approve")
     * @Secure(roles="ROLE_COORDINATOR, ROLE_HR_MANAGER")
     * @Method("POST|GET")
     */
    public function approveAction(Request $request, $identifier)
    {
        $em = $this->getDoctrine()->getManager();
        $identifier = $identifier == 'NONE' ? '' : $identifier;

        $entities = $em->getRepository('KrasKrasBundle:UserLeave')->findByIdentifier($identifier);

        if (!count($entities)) {
            throw $this->createNotFoundException('Unable to find UserLeave entity.');
        }

        if (!$this->getUser()->hasSudoMode()) {
            return $this->redirect($this->generateUrl(
                'kras_sudo_form',
                array(
                    'redirect' => $this->generateUrl(
                        $request->get('_route'),
                        $request->get('_route_params')
                    ),
                    'previous' => $request->headers->get('referer'),
                )
            ));
        }

        $redirect_url = $request->headers->get('referer', $this->generateUrl('kras_leave_index'));

        foreach ($entities as $entity) {
            if ($entity->getApproved()) continue;

            if ($entity->getLeaveType()->getPublicHoliday() || $entity->getLeaveType()->getSickness()) {
                $year = $entity->getLeaveDates()->first()->getDate()->format('Y');

                foreach ($entity->getEmployees() as $employee) {
                    if ((!$entity->getLeaveType()->getOvertime() && ((($entity->getEndTime()->format('U') - $entity->getStartTime()->format('U')) * $entity->getLeaveDates()->count()) / 3600) > $this->get('kras.employee')->getRemainingLeave($entity->getLeaveType(), $employee, null, $year))
                        || ($entity->getLeaveType()->getOvertime() && ((($entity->getEndTime()->format('U') - $entity->getStartTime()->format('U')) * $entity->getLeaveDates()->count()) / 3600) > $this->get('kras.employee')->getRemainingOvertime($employee))) {
                        $request->getSession()->getFlashBag()->add(
                            'error',
                            $this->get('translator')->trans('The employee "%employee%" is not allowed to take this much leave.', array('%employee%' => $employee))
                        );

                        return $this->redirect($redirect_url);
                    }

                    if ($this->get('kras.employee')->hasLeaveScheduled($entity->getLeaveDates(), $entity->getStartTime(), $entity->getEndTime(), $employee)) {
                        $request->getSession()->getFlashBag()->add(
                            'error',
                            $this->get('translator')->trans('The employee "%employee%" already has leave scheduled on one of the dates entered.', array('%employee%' => $employee))
                        );

                        return $this->redirect($redirect_url);
                    }
                }
            }
        }

        foreach ($entities as $entity) {
            if ($entity->getApproved()) continue;

            $entity->setApproved(true);

            $em->persist($entity);
            $em->flush();

            $this->get('kras.employee')->deleteDefaultFragments($entity);
            $this->get('kras.employee')->recalculateTimesheetWorkedHoursByLeave($entity);

            if ($entity->getLeaveType()->getOvertime()) {
                foreach ($entity->getEmployees() as $employee) {
                    $this->get('kras.employee')->scheduleOvertime(
                        $employee,
                        ($entity->getDuration() / $entity->getLeaveDates()->count()),
                        $entity->getLeaveDates()->toArray()
                    );
                }
            }
        }

        $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans(
            'This set of leaves has been approved successfully.'
        ));

        return $this->redirect($redirect_url);
    }

    /**
     * @Route("/{identifier}/reject", name="kras_leave_reject")
     * @Secure(roles="ROLE_COORDINATOR, ROLE_HR_MANAGER")
     * @Method("POST|GET")
     */
    public function rejectAction(Request $request, $identifier)
    {
        $em = $this->getDoctrine()->getManager();
        $identifier = $identifier == 'NONE' ? '' : $identifier;

        $entities = $em->getRepository('KrasKrasBundle:UserLeave')->findByIdentifier($identifier);

        if (!count($entities)) {
            throw $this->createNotFoundException('Unable to find UserLeave entity.');
        }

        if (!$this->getUser()->hasSudoMode()) {
            return $this->redirect($this->generateUrl(
                'kras_sudo_form',
                array(
                    'redirect' => $this->generateUrl(
                        $request->get('_route'),
                        $request->get('_route_params')
                    ),
                    'previous' => $request->headers->get('referer'),
                )
            ));
        }

        foreach ($entities as $entity) {
            $leave_is_approved_overtime = false;
            if ($entity->getApproved() && $entity->getLeaveType()->getOvertime()) {
                $leave_is_approved_overtime = true;
                $leave_duration = $entity->getDuration() / $entity->getLeaveDates()->count();
                $leave_dates = $entity->getLeaveDates()->toArray();
                $leave_employees = $entity->getEmployees();
            }

            $entity->setApproved(false);

            $em->persist($entity);
            $em->flush();

            $this->get('kras.employee')->recalculateTimesheetWorkedHoursByLeave($entity);

            if ($leave_is_approved_overtime) {
                foreach ($leave_employees as $employee) {
                    $this->get('kras.employee')->unscheduleOvertime(
                        $employee,
                        $leave_duration,
                        $leave_dates
                    );
                }
            }
        }

        $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans(
            'This set of leave shas been rejected successfully.'
        ));

        $redirect_url = $request->headers->get('referer', $this->generateUrl('kras_leave_index'));
        return $this->redirect($redirect_url);
    }
}
