<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Kras\KrasBundle\Form\SudoType;

class SudoController extends Controller
{
    /**
     * @Route("/sudo", name="kras_sudo_form")
     * @Template()
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function formAction(Request $request)
    {
        $entity = $this->getUser();

        $form = $this->createForm(new SudoType(), $entity);

        if ($request->getMethod() == 'POST') {
            $form->submit($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $entity->setLastSudo(new \DateTime());

                $em->persist($entity);
                $em->flush();

                return $this->redirect(
                    $request->get(
                        'previous',
                        $this->generateUrl('kras_index')
                    )
                );
            }
        }

        return array(
            'form' => $form->createView(),
        );
    }
}
