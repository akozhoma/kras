<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\LogEntryTheme;
use Kras\KrasBundle\Form\LogEntryThemeType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;

/**
 * @Route("/logentrythemes")
 */
class LogEntryThemeController extends Controller
{
    /**
     * @Route("/", name="kras_logentrytheme_index")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:LogEntryTheme');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);
            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }
        return array();
    }

    /**
     * @Route("/{id}/show", name="kras_logentrytheme_show")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:LogEntryTheme')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LogEntryTheme entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/new", name="kras_logentrytheme_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function newAction()
    {
        $entity = new LogEntryTheme();
        $form = $this->createForm(new LogEntryThemeType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/create", name="kras_logentrytheme_create")
     * @Method("POST")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("KrasKrasBundle:LogEntryTheme:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new LogEntryTheme();
        $form = $this->createForm(new LogEntryThemeType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('kras_logentrytheme_show', array('id' => $entity->getId())));
        }

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="kras_logentrytheme_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:LogEntryTheme')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LogEntryTheme entity.');
        }

        $editForm = $this->createForm(new LogEntryThemeType(), $entity);

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/update", name="kras_logentrytheme_update")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("KrasKrasBundle:LogEntryTheme:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:LogEntryTheme')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LogEntryTheme entity.');
        }

        $editForm = $this->createForm(new LogEntryThemeType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('kras_logentrytheme_show', array('id' => $id)));
        }

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/delete", name="kras_logentrytheme_delete")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:LogEntryTheme')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LogEntryTheme entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('kras_logentrytheme_index'));
    }
}
