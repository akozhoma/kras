<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;
use JMS\Serializer\SerializationContext;

/**
 * @Route("/center_leaveoverview")
 */
class CenterLeaveOverviewController extends Controller
{
    /**
     * @Route("/", name="kras_center_leaveoverview_index")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function indexAction()
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasUserBundle:User');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);

            $datatable->addWhereBuilderCallback(function($qb) use ($datatable, $sc) {
                $andExpr = $qb->expr()->andX();
                $andExpr->add($qb->expr()->eq('FosUser.supportcenter', $sc->getId()));
                $qb->andWhere($andExpr);
            });

            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }

        return array('supportcenter' => $sc);
    }

    /**
     * @Route("/{id}/show", name="kras_center_leaveoverview_show")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function showAction($id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasUserBundle:User')->findOneBy(array(
            'id'            => $id,
            'supportcenter' => $sc,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $data = $this->get('kras.employee')->getLeaveStatus($entity);
        $overtime = $this->get('kras.employee')->getOvertimeStatus($entity);

        return array(
            'data'     => $data,
            'entity'   => $entity,
            'overtime' => $overtime,
        );
    }
}
