<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\UserLeave;
use Kras\KrasBundle\Form\MyUserLeaveType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("/leave")
 */
class MyLeaveController extends Controller
{
    /**
     * @Route("/", name="kras_myleave_index")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:UserLeave');
            $user = $this->getUser();

            $datatable->addWhereBuilderCallback(function($qb) use ($datatable, $user) {
                $andExpr = $qb->expr()->andX();
                $qb->leftJoin('UserLeave.employees', 'Employee');
                $andExpr->add($qb->expr()->eq('Employee.id', $user->getId()));

                $qb->andWhere("Employee IS NULL OR $andExpr");
            });

            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);

            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }

        return array();
    }

    /**
     * @Route("/{id}/show", name="kras_myleave_show")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = 'SELECT l FROM KrasKrasBundle:UserLeave l
            LEFT JOIN l.employees e
            WHERE l.id = ?1
            AND (e IS NULL OR e.id = ?2)';
        $entity = $em->createQuery($dql)->setParameters(array(
            1 => $id,
            2 => $this->getUser()
        ))->getOneOrNullResult();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserLeave entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/new", name="kras_myleave_new")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function newAction()
    {
        $entity = new UserLeave();
        $entity->setEmployees(new ArrayCollection(array($this->getUser())));
        $form = $this->createForm(new MyUserLeaveType(), $entity);

        $leave_types = $this->getDoctrine()->getManager()->getRepository('KrasKrasBundle:LeaveType')->findAll();

        return array('entity' => $entity, 'form' => $form->createView(), 'leave_types' => $leave_types);
    }

    /**
     * @Route("/create", name="kras_myleave_create")
     * @Method("POST")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template("KrasKrasBundle:MyLeave:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new UserLeave();
        $entity->setEmployees(new ArrayCollection(array($this->getUser())));
        $form = $this->createForm(new MyUserLeaveType(), $entity);
        $form->submit($request);

        $em = $this->getDoctrine()->getManager();
        $leave_types = $em->getRepository('KrasKrasBundle:LeaveType')->findAll();

        if ($form->isValid()) {
            $has_date = false;
            foreach ($form->get('subLeaves')->getData() as $subLeave) {
                if ($subLeave['leavedate']->getDate()) {
                    $has_date = true;
                    break;
                }
            }

            if (!$has_date) {
                $request->getSession()->getFlashBag()->add(
                    'error',
                    $this->get('translator')->trans('You can\'t schedule leave without choosing a date and a time.')
                );

                return array('entity' => $entity, 'form' => $form->createView(), 'leave_types' => $leave_types);
            }

            if ($form->get('leavetype')->getData()->getPublicHoliday() || $form->get('leavetype')->getData()->getSickness()) {
                $entity
                    ->setStartTime(null)
                    ->setEndTime(null);
            } else {
                $year = current($form->get('subLeaves')->getData());
                $year = $year['leavedate']->getDate()->format('Y');
                $durations = array();

                foreach ($form->get('subLeaves')->getData() as $subLeave) {
                    $durations[] = $subLeave['endTime']->format('U') - $subLeave['startTime']->format('U');
                }

//                if (min($durations) <  3600) {
//                    $request->getSession()->getFlashBag()->add(
//                        'error',
//                        $this->get('translator')->trans('Leave must be scheduled for at least an hour.')
//                    );
//
//                    return array('entity' => $entity, 'form' => $form->createView(), 'leave_types' => $leave_types);
//                }

                if ((!$form->get('leavetype')->getData()->getOvertime() && ((array_sum($durations) / 3600) > $this->get('kras.employee')->getRemainingLeave($form->get('leavetype')->getData(), $entity->getEmployees()->first(), null, $year)))
                    || ($form->get('leavetype')->getData()->getOvertime() && ((array_sum($durations) / 3600) > $this->get('kras.employee')->getRemainingOvertime($entity->getEmployees()->first())))) {
                    $request->getSession()->getFlashBag()->add(
                        'error',
                        $this->get('translator')->trans('You are not allowed to take this much leave.')
                    );

                    return array('entity' => $entity, 'form' => $form->createView(), 'leave_types' => $leave_types);
                }

                 /*
                 * start check not rejected leaves
                 */
                $employee = $entity->getEmployees()->first();

                if ($this->get('kras.employee')->checkNotApprovedLeaves($employee, $form->get('leavetype')->getData(), $durations, $year)) {
                    $request->getSession()->getFlashBag()->add(
                        'error',
                        $this->get('translator')->trans('You are not allowed to take this much leave. Check not approved')
                    );

                    return array('entity' => $entity, 'form' => $form->createView(), 'leave_types' => $leave_types);
                }
                /*
                 * end check not rejected leaves
                 */

                foreach ($form->get('subLeaves')->getData() as $subLeave) {
                    if ($this->get('kras.employee')->hasLeaveScheduled(array($subLeave['leavedate']), $subLeave['startTime'], $subLeave['endTime'], $entity->getEmployees()->first())) {
                        $request->getSession()->getFlashBag()->add(
                            'error',
                            $this->get('translator')->trans('You already have leave scheduled on one of the dates entered.')
                        );

                        return array('entity' => $entity, 'form' => $form->createView(), 'leave_types' => $leave_types);
                    }
                }
            }

            foreach ($form->get('subLeaves')->getData() as $subLeave) {
                $leave = clone $entity;

                $leave->setStartTime($subLeave['startTime']);
                $leave->setEndTime($subLeave['endTime']);

                $leave->setLeaveDates(new ArrayCollection());
                $leave->addLeaveDate($subLeave['leavedate']);

                $em->persist($leave);
                $em->flush();

                $this->get('kras.employee')->recalculateTimesheetWorkedHoursByLeave($leave);
                $this->get('kras.logging')->log($leave, 'add');
            }

            return $this->redirect($this->generateUrl('kras_myleave_index'));
        }

        return array('entity' => $entity, 'form' => $form->createView(), 'leave_types' => $leave_types);
    }

    /**
     * @Route("/{id}/delete", name="kras_myleave_delete")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = 'SELECT l FROM KrasKrasBundle:UserLeave l
            LEFT JOIN l.employees e
            WHERE l.id = ?1
            AND e.id = ?2
            AND l.approved IS NULL';
        $entity = $em->createQuery($dql)->setParameters(array(
            1 => $id,
            2 => $this->getUser()
        ))->getOneOrNullResult();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserLeave entity.');
        }

        $timesheets = $this->get('kras.employee')->getTimesheetsByLeave($entity);

        $em->remove($entity);
        $em->flush();

        $this->get('kras.employee')->recalculateTimesheetsWorkedHours($timesheets);

        return $this->redirect($this->generateUrl('kras_myleave_index'));
    }
}
