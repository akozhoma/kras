<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\Activity;
use Kras\KrasBundle\Form\ActivityType;
use Kras\KrasBundle\Entity\AttendanceSheet;
use Kras\KrasBundle\Form\AttendanceSheetType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;

/**
 * @Route("/activities")
 */
class ActivityController extends Controller
{
    /**
     * @Route("/", name="kras_activity_index")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:Activity');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);

            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }
        return array();
    }

    /**
     * @Route("/{id}/show", name="kras_activity_show")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/new", name="kras_activity_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Activity();
        $form = $this->createForm(new ActivityType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/create", name="kras_activity_create")
     * @Method("POST")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("KrasKrasBundle:Activity:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Activity();
        $form = $this->createForm(new ActivityType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            $failed = false;

            $starts_count = $form->get('starts')->getData() == null ? 0 : count($form->get('starts')->getData());
            $ends_count = $form->get('ends')->getData() == null ? 0 : count($form->get('ends')->getData());
            $date_count = $starts_count < $ends_count ? $starts_count : $ends_count;

            if ($date_count == 0) {
                $this->get('session')->getFlashBag()->add('error', 'You must specify at least one pair of dates for an activity.');
                $failed = true;
            }


            $em = $this->getDoctrine()->getManager();

            // Make sure all selected sections have the same support center
            $entity->setSupportCenter($entity->getSections()->first()->getSupportCenter());
            foreach($entity->getSections() as $section) {
                if ($section->getSupportCenter()->getId() != $entity->getSupportCenter()->getId()) {
                    $this->get('session')->getFlashBag()->add('error', 'All of the sections you select must be part of the same support center.');
                    $failed = true;
                }
            }

            // Make sure all volunteers are part of the same support center
            foreach($entity->getVolunteers() as $volunteer) {
                if ($volunteer->getSupportCenter()->getId() != $entity->getSupportCenter()->getId()) {
                    $this->get('session')->getFlashBag()->add('error', 'All of the volunteers you select must be part of the same support center.');
                    $failed = true;
                }
            }

            // Make sure all employees are part of the same support center
            foreach($entity->getEmployees() as $employee) {
                if (!$employee->getSupportCenter() || $employee->getSupportCenter()->getId() != $entity->getSupportCenter()->getId()) {
                    $this->get('session')->getFlashBag()->add('error', 'All of the employees you select must be part of the same support center.');
                    $failed = true;
                }
            }

            if (!$failed) {
                foreach(range(0, $date_count -1) as $i) {
                    $start = $form->get('starts')->getData();
                    $start = $start[$i];

                    $end = $form->get('ends')->getData();
                    $end = $end[$i];

                    $entity = clone $entity;
                    $entity->setStart($start);
                    $entity->setEnd($end);

                    $em->persist($entity);
                }

                $em->flush();

                // Log
                $this->get('kras.logging')->log($entity, 'add');

                if ($request->get('submit') == 'attendance') {
                    return $this->redirect($this->generateUrl('kras_activity_attendance', array('id' => $entity->getId())));
                }

                return $this->redirect($this->generateUrl('kras_activity_index'));
            }
        }

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="kras_activity_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        $editForm = $this->createForm(new ActivityType(), $entity);

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/update", name="kras_activity_update")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("KrasKrasBundle:Activity:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        $editForm = $this->createForm(new ActivityType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {

            $failed = false;

            // Make sure all selected sections have the same support center
            $entity->setSupportCenter($entity->getSections()->first()->getSupportCenter());
            foreach($entity->getSections() as $section) {
                if ($section->getSupportCenter()->getId() != $entity->getSupportCenter()->getId()) {
                    $this->get('session')->getFlashBag()->add('error', 'All of the sections you select must be part of the same support center.');
                    $failed = true;
                }
            }

            // Make sure all volunteers are part of the same support center
            foreach($entity->getVolunteers() as $volunteer) {
                if ($volunteer->getSupportCenter()->getId() != $entity->getSupportCenter()->getId()) {
                    $this->get('session')->getFlashBag()->add('error', 'All of the volunteers you select must be part of the same support center.');
                    $failed = true;
                }
            }

            // Make sure all employees are part of the same support center
            foreach($entity->getEmployees() as $employee) {
                if ($employee->getSupportCenter()->getId() != $entity->getSupportCenter()->getId()) {
                    $this->get('session')->getFlashBag()->add('error', 'All of the employees you select must be part of the same support center.');
                    $failed = true;
                }
            }

            if (!$failed) {

                $em->persist($entity);
                $em->flush();

                // Log
                $this->get('kras.logging')->log($entity, 'update');

                return $this->redirect($this->generateUrl('kras_activity_show', array('id' => $entity->getId())));
            }
        }

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/delete", name="kras_activity_delete")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        // Log
        $this->get('kras.logging')->log($entity, 'delete');

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('kras_activity_index'));
    }

    /**
     * @Route("/{id}/attendance", name="kras_activity_attendance")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function attendanceAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        $attendance_sheet = $entity->getAttendanceSheet();
        if ($attendance_sheet == null) {
            $attendance_sheet = new AttendanceSheet();
            $attendance_sheet->setActivity($entity);
        }

        $form = $this->createForm(new AttendanceSheetType(), $attendance_sheet);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/{id}/attendance/update", name="kras_activity_attendance_update")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("KrasKrasBundle:Activity:attendance.html.twig")
     */
    public function attendanceUpdateAction(Request $request, $id)
    {
       $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        $attendance_sheet = $entity->getAttendanceSheet();
        if ($attendance_sheet == null) {
            $attendance_sheet = new AttendanceSheet();
            $attendance_sheet->setActivity($entity);
        }

        $form = $this->createForm(new AttendanceSheetType(), $attendance_sheet);
        $form->bind($request);

        if ($form->isValid()) {

            $em->persist($attendance_sheet);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'The attendance sheet has been saved.');

            return $this->redirect($this->generateUrl('kras_activity_attendance', array('id' => $entity->getId())));
        }

        return array('entity' => $entity, 'form' => $form->createView());
    }


    /**
     * @Route("/{id}/map", name="kras_activity_map")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function mapAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/{id}/json_address_list", name="kras_activity_json_address_list")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     */
    public function jsonAddressListAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        $activity = array(
            'name' => $entity->getName(),
            'description' => $entity->getDescription(),
            'address' => $entity->getAddress(),
            'postalcode' => $entity->getPostalCode(),
            'city' => $entity->getCity()
        );

        $supportcenter= array(
            'name' => $entity->getSupportCenter()->getName(),
            'address' => $entity->getSupportCenter()->getAddress(),
            'postalcode' => $entity->getSupportCenter()->getPostalCode(),
            'city' => $entity->getSupportCenter()->getCity()
        );

        $members = array();
        if ($entity->getAttendanceSheet()) {
            foreach($entity->getAttendanceSheet()->getMembers() as $member) {
                if ($member->getAddress()
                    && $member->getPostalCode()
                    && $member->getCity()) {
                    $members[] = array(
                        'name' => (string) $member,
                        'address' => $member->getAddress(),
                        'postalcode' => $member->getPostalCode(),
                        'city' => $member->getCity()
                    );
                }
            }
        }

        $response = array(
            'activity' => $activity,
            'members' => $members,
            'supportcenter' => $supportcenter
        );

        $response = json_encode($response);
        return new Response($response, 200, array('Content-Type' => 'application/json'));
    }
}
