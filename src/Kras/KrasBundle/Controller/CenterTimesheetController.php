<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\Timesheet;
use Kras\KrasBundle\Entity\ExtendedTimeFragment;
use Kras\KrasBundle\Form\ExtendedTimeFragmentType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;
use JMS\Serializer\SerializationContext;

/**
 * @Route("/center_timesheet")
 */
class CenterTimesheetController extends Controller
{
    /**
     * @Route("/", name="kras_center_timesheet_index")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function indexAction()
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:Timesheet');

            $datatable->addWhereBuilderCallback(function($qb) use ($datatable, $sc) {
                $andExpr = $qb->expr()->andX();
                $andExpr->add($qb->expr()->eq('Timesheet.supportcenter', $sc->getId()));
                $qb->andWhere($andExpr);
            });

            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);

            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }

        return array('supportcenter' => $sc);
    }

    /**
     * @Route("/{id}/show", name="kras_center_timesheet_show")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function showAction($id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'            => $id,
            'supportcenter' => $sc,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/{id}/data", name="kras_center_timesheet_data")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Method("POST|GET")
     */
    public function dataAction(Request $request, $id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'            => $id,
            'supportcenter' => $sc,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $output = $this->get('jms_serializer')->serialize($entity, 'json');

        return new Response($output, 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/comments", name="kras_center_timesheet_comments")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST")
     */
    public function commentsAction(Request $request, $id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'             => $id,
            'supportcenter'  => $sc,
            'approvedHr'     => false,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $comments = $request->get('comments');
        $entity->setComments($comments);

        $em->persist($entity);
        $em->flush();

        return new Response('OK', 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/leaves", name="kras_center_timesheet_leaves")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function leavesAction(Request $request, $id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'            => $id,
            'supportcenter' => $sc,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $end_date = clone $entity->getStartDate();
        $end_date->setTime(23, 59, 59);
        $end_date->modify('+6 days');

        $dql = 'SELECT l, lt, ld FROM
            KrasKrasBundle:UserLeave l
            JOIN l.leavetype lt
            JOIN l.leavedates ld
            LEFT JOIN l.employees e
            WHERE (e.id = ?1 OR e IS NULL)
            AND (ld.date BETWEEN ?2 AND ?3)
            ORDER BY l.startTime ASC';
        $query = $em->createQuery($dql);
        $entities = $query
            ->setParameters(array(
                1 => $entity->getEmployee(),
                2 => $entity->getStartDate(),
                3 => $end_date,
            ))
            ->getResult();

        $output = $this->get('jms_serializer')->serialize($entities, 'json');

        return new Response($output, 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/approve", name="kras_center_timesheet_approve")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Method("POST|GET")
     */
    public function approveAction(Request $request, $id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'             => $id,
            'supportcenter'  => $sc,
            'approvedHr'     => false,
        ));

        if (!$entity || ($entity->getEmployee()->getId() == $this->getUser()->getId())) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        if (!$this->getUser()->hasSudoMode()) {
            return $this->redirect($this->generateUrl(
                'kras_sudo_form',
                array(
                    'redirect' => $this->generateUrl(
                        $request->get('_route'),
                        $request->get('_route_params')
                    ),
                    'previous' => $request->headers->get('referer'),
                )
            ));
        }

        $entity
            ->setApprovedCo(true)
            ->setApprovedDi(true)
            ->setSubmitted(true)
            ->setRejected(false);

        $em->persist($entity);
        $em->flush();

        $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans(
            'This timesheet has been approved successfully.'
        ));

        return $this->redirect($this->generateUrl('kras_center_timesheet_show', array('id' => $entity->getId())));
    }

    /**
     * @Route("/{id}/reject", name="kras_center_timesheet_reject")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Method("POST|GET")
     */
    public function rejectAction(Request $request, $id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'             => $id,
            'supportcenter'  => $sc,
            'approvedHr'     => false,
        ));

        if (!$entity || ($entity->getEmployee()->getId() == $this->getUser()->getId())) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        if (!$this->getUser()->hasSudoMode()) {
            return $this->redirect($this->generateUrl(
                'kras_sudo_form',
                array(
                    'redirect' => $this->generateUrl(
                        $request->get('_route'),
                        $request->get('_route_params')
                    ),
                    'previous' => $request->headers->get('referer'),
                )
            ));
        }

        $entity
            ->setApprovedCo(false)
            ->setApprovedDi(false)
            ->setSubmitted(false)
            ->setRejected(true);

        $em->persist($entity);
        $em->flush();

        $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans(
            'This timesheet has been rejected successfully.'
        ));

        return $this->redirect($this->generateUrl('kras_center_timesheet_show', array('id' => $entity->getId())));
    }
}
