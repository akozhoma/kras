<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\Timesheet;
use Kras\KrasBundle\Entity\Overtime;
use Kras\KrasBundle\Entity\ExtendedTimeFragment;
use Kras\KrasBundle\Form\ExtendedTimeFragmentType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;
use JMS\Serializer\SerializationContext;

/**
 * @Route("/timesheets")
 */
class TimesheetController extends Controller
{
    /**
     * @Route("/", name="kras_timesheets_index")
     * @Secure(roles={"ROLE_HR_MANAGER", "ROLE_DIRECTOR"})
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:Timesheet');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);

            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }

        return array();
    }

    /**
     * @Route("/{id}/show", name="kras_timesheets_show")
     * @Secure(roles={"ROLE_HR_MANAGER", "ROLE_DIRECTOR"})
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $frag = new ExtendedTimeFragment();
        $frag->setTimesheet($entity);
        $form = $this->createForm(new ExtendedTimeFragmentType(), $frag);

        $dql = 'SELECT tft FROM KrasKrasBundle:TimeFragmentType tft';
        $query = $em->createQuery($dql);
        $types = $query->getResult();

        return array(
            'entity'         => $entity,
            'form'           => $form->createView(),
            'fragment_types' => $types,
        );
    }

    /**
     * @Route("/{id}/data", name="kras_timesheets_data")
     * @Secure(roles={"ROLE_HR_MANAGER", "ROLE_DIRECTOR"})
     * @Method("POST|GET")
     */
    public function dataAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $output = $this->get('jms_serializer')->serialize($entity, 'json');

        return new Response($output, 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/comments", name="kras_timesheets_comments")
     * @Secure(roles={"ROLE_HR_MANAGER", "ROLE_DIRECTOR"})
     * @Method("POST")
     */
    public function commentsAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $comments = $request->get('comments');
        $entity->setComments($comments);

        $em->persist($entity);
        $em->flush();

        return new Response('OK', 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/leaves", name="kras_timesheets_leaves")
     * @Secure(roles={"ROLE_HR_MANAGER", "ROLE_DIRECTOR"})
     * @Template()
     */
    public function leavesAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $entities = $this->get('kras.employee')->getLeavesByTimesheetAndEmploe($entity);

        $output = $this->get('jms_serializer')->serialize($entities, 'json');

        return new Response($output, 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/events/create", name="kras_timesheets_event_create")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST|GET")
     */
    public function eventCreateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $extended_fragment = new ExtendedTimeFragment();
        $extended_fragment->setTimesheet($entity);
        $form = $this->createForm(new ExtendedTimeFragmentType(), $extended_fragment);

        $form->submit($request);

        if (!$form->isValid()) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $start = clone ($entity->getStartDate());
        while($start->format('N') != $form->get('day_of_week')->getData()) {
            $start->modify('+1 day');
        }

        $start_time_data = $form->get('start_time')->getData();
        $start->setTime($start_time_data['hour'], $start_time_data['minute'], 0);

        $end = clone $start;
        $end_time_data = $form->get('end_time')->getData();
        $end->setTime($end_time_data['hour'], $end_time_data['minute'], 0);

        if ($end->format('U') <= $start->format('U')) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $dql = 'SELECT count(etf.id) FROM KrasKrasBundle:ExtendedTimeFragment etf
            JOIN etf.timesheet ts
            WHERE ts.employee = ?1
            AND (etf.start <  ?3 AND etf.end > ?2)
            AND ts.id = ?4';
        $query = $em->createQuery($dql)->setParameters(
            array(
                1 => $this->getUser(),
                2 => $start,
                3 => $end,
                4 => $entity->getId(),
            )
        );
        $hasTimeFragment = $query->getSingleScalarResult();

        if ($hasTimeFragment) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $dql = 'SELECT count(ul.id) FROM KrasKrasBundle:UserLeave ul
            JOIN ul.leavedates ld
            LEFT JOIN ul.employees e
            WHERE ld.date = ?1
            AND (e IS NULL OR e.id = ?2)
            AND (ul.startTime < ?4 AND ul.endTime > ?3)';
        $query = $em->createQuery($dql)->setParameters(array(
            1 => $start->format('Y-m-d'),
            2 => $entity->getEmployee(),
            3 => $start,
            4 => $end,
        ));
        $hasLeave = $query->getSingleScalarResult();

        if ($hasLeave) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $extended_fragment
            ->setStart($start)
            ->setEnd($end);

        if ($extended_fragment->getTimeFragmentType()->getLinkActivity()) {
            $extended_fragment->setLinkedActivity($form->get('activity')->getData());
        }

        if ($extended_fragment->getTimeFragmentType()->getLinkMember()) {
            $extended_fragment->setLinkedMember($form->get('member')->getData());
        }

        if ($extended_fragment->getTimeFragmentType()->getLinkTraining()) {
            $extended_fragment->setLinkedTraining($form->get('training')->getData());
        }

        $em->persist($extended_fragment);
        $em->flush();

        $em->refresh($entity);
        $this->get('kras.employee')->recalculateTimesheetWorkedHours($entity);

        $em->persist($entity);
        $em->flush();

        return new Response('OK', 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/events/{event_id}/update", name="kras_timesheets_event_update")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST|GET")
     */
    public function eventUpdateAction(Request $request, $id, $event_id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $extended_fragment = $em->getRepository('KrasKrasBundle:ExtendedTimeFragment')->findOneBy(array(
            'id'        => $event_id,
            'timesheet' => $id,
        ));

        if (!$extended_fragment) {
            throw $this->createNotFoundException('Unable to find ExtendedTimeFragment entity.');
        }

        $form = $this->createForm(new ExtendedTimeFragmentType(), $extended_fragment);
        $form->submit($request);

        if (!$form->isValid()) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $start = clone ($entity->getStartDate());
        while($start->format('N') != $form->get('day_of_week')->getData()) {
            $start->modify('+1 day');
        }

        $start_time_data = $form->get('start_time')->getData();
        $start->setTime($start_time_data['hour'], $start_time_data['minute'], 0);

        $end = clone $start;
        $end_time_data = $form->get('end_time')->getData();
        $end->setTime($end_time_data['hour'], $end_time_data['minute'], 0);

        if ($end->format('U') <= $start->format('U')) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $dql = 'SELECT count(etf.id) FROM KrasKrasBundle:ExtendedTimeFragment etf
            JOIN etf.timesheet ts
            WHERE ts.employee = ?1
            AND (etf.start <  ?3 AND etf.end > ?2)
            AND etf.id != ?4
            AND ts.id = ?5';
        $query = $em->createQuery($dql)->setParameters(
            array(
                1 => $entity->getEmployee(),
                2 => $start,
                3 => $end,
                4 => $extended_fragment->getId(),
                5 => $entity->getId(),
            )
        );
        $hasTimeFragment = $query->getSingleScalarResult();

        if ($hasTimeFragment) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $dql = 'SELECT count(ul.id) FROM KrasKrasBundle:UserLeave ul
            JOIN ul.leavedates ld
            LEFT JOIN ul.employees e
            WHERE ld.date = ?1
            AND (e IS NULL OR e.id = ?2)
            AND (ul.startTime < ?4 AND ul.endTime > ?3)';
        $query = $em->createQuery($dql)->setParameters(array(
            1 => $start->format('Y-m-d'),
            2 => $entity->getEmployee(),
            3 => $start,
            4 => $end,
        ));
        $hasLeave = $query->getSingleScalarResult();

        if ($hasLeave) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $extended_fragment
            ->setStart($start)
            ->setEnd($end);

        if ($extended_fragment->getTimeFragmentType()->getLinkActivity()) {
            $extended_fragment->setLinkedActivity($form->get('activity')->getData());
        } else {
            $extended_fragment->setLinkedActivity(null);
        }

        if ($extended_fragment->getTimeFragmentType()->getLinkMember()) {
            $extended_fragment->setLinkedMember($form->get('member')->getData());
        } else {
            $extended_fragment->setLinkedMember(null);
        }

        if ($extended_fragment->getTimeFragmentType()->getLinkTraining()) {
            $extended_fragment->setLinkedTraining($form->get('training')->getData());
        } else {
            $extended_fragment->setLinkedTraining(null);
        }

        $em->persist($extended_fragment);
        $em->flush();

        $em->refresh($entity);
        $this->get('kras.employee')->recalculateTimesheetWorkedHours($entity);

        $em->persist($entity);
        $em->flush();

        return new Response('OK', 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/events/{event_id}/delete", name="kras_timesheets_event_delete")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST|GET")
     */
    public function eventDeleteAction(Request $request, $id, $event_id)
    {
        $em = $this->getDoctrine()->getManager();

        $t = $em->getRepository('KrasKrasBundle:Timesheet')->find($id);

        if (!$t) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $entity = $em->getRepository('KrasKrasBundle:ExtendedTimeFragment')->findOneBy(array(
            'id'        => $event_id,
            'timesheet' => $id,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ExtendedTimeFragment entity.');
        }

        $em->remove($entity);
        $em->flush();

        $em->refresh($t);
        $this->get('kras.employee')->recalculateTimesheetWorkedHours($t);

        $em->persist($t);
        $em->flush();

        return new Response('OK', 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/approve", name="kras_timesheets_approve")
     * @Secure(roles={"ROLE_HR_MANAGER", "ROLE_DIRECTOR"})
     * @Method("POST|GET")
     */
    public function approveAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        if (!$this->getUser()->hasSudoMode()) {
            return $this->redirect($this->generateUrl(
                'kras_sudo_form',
                array(
                    'redirect' => $this->generateUrl(
                        $request->get('_route'),
                        $request->get('_route_params')
                    ),
                    'previous' => $request->headers->get('referer'),
                )
            ));
        }

        $entity
            ->setApprovedCo(true)
            ->setSubmitted(true)
            ->setApprovedDi(true)
            ->setRejected(false);

        if ($this->get('security.context')->isGranted('ROLE_HR_MANAGER')) {
            $entity->setApprovedHr(true);
        }

        $em->persist($entity);
        $em->flush();

        $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans(
            'This timesheet has been approved successfully.'
        ));

        return $this->redirect($this->generateUrl('kras_timesheets_show', array('id' => $entity->getId())));
    }

    /**
     * @Route("/{id}/reject", name="kras_timesheets_reject")
     * @Secure(roles={"ROLE_HR_MANAGER", "ROLE_DIRECTOR"})
     * @Method("POST|GET")
     */
    public function rejectAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        if (!$this->getUser()->hasSudoMode()) {
            return $this->redirect($this->generateUrl(
                'kras_sudo_form',
                array(
                    'redirect' => $this->generateUrl(
                        $request->get('_route'),
                        $request->get('_route_params')
                    ),
                    'previous' => $request->headers->get('referer'),
                )
            ));
        }

        $entity
            ->setApprovedCo(false)
            ->setApprovedHr(false)
            ->setApprovedDi(false)
            ->setSubmitted(false)
            ->setRejected(true);

        $em->persist($entity);
        $em->flush();

        $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans(
            'This timesheet has been rejected successfully.'
        ));

        return $this->redirect($this->generateUrl('kras_timesheets_show', array('id' => $entity->getId())));
    }

    /**
     * @Route("/{id}/req_hours", name="kras_timesheets_req_hours")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST")
     */
    public function reqHoursAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $hours = $request->get('hours');
        $entity->setRequiredHours($hours);

        $this->get('kras.employee')->recalculateTimesheetWorkedHours($entity);

        $em->persist($entity);
        $em->flush();

        return new Response('OK', 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/delete", name="kras_timesheets_delete")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST|GET")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneById($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('kras_timesheets_index', array('id' => $entity->getId())));
    }

    /**
     * @Route("/{id}/getHours", name="kras_timesheets_get_hours")
     * @Secure(roles={"ROLE_HR_MANAGER", "ROLE_DIRECTOR"})
     * @Template()
     */
    public function getHoursAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $time['workedHours'] = $entity->getWorkedHours();

        $time['overtime'] = $entity->getOvertimeHours();


        $output = $this->get('jms_serializer')->serialize($time, 'json');

        return new Response($output, 200, array('Content-Type' => 'application/json'));
    }
}
