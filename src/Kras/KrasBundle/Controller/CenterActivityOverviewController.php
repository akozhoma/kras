<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;
use JMS\Serializer\SerializationContext;

/**
 * @Route("/center_activityoverview")
 */
class CenterActivityOverviewController extends Controller
{
    /**
     * @Route("/", name="kras_center_activityoverview_index")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function indexAction()
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:ExtendedTimeFragment');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);

            $datatable->addWhereBuilderCallback(function($qb) use ($datatable, $sc) {
                $qb->innerJoin('ExtendedTimeFragment.timesheet', 'Timesheet');
                $qb->innerJoin('Timesheet.employee', 'Employee');

                $andExpr = $qb->expr()->andX();
                $andExpr->add($qb->expr()->eq('Employee.supportcenter', $sc->getId()));
                $qb->andWhere($andExpr);
            });

            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);

            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }

        return array('supportcenter' => $sc);
    }

    /**
     * @Route("/data", name="kras_center_activityoverview_data")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Method("POST|GET")
     * @Template()
     */
    public function dataAction(Request $request)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();

        $start_date = $request->get('chart-filter-start-date');
        $start_date = $start_date ? \DateTime::createFromFormat('Y-m-d', $start_date) : null;

        $end_date = $request->get('chart-filter-end-date');
        $end_date = $end_date ? \DateTime::createFromFormat('Y-m-d', $end_date) : null;

        $dql = 'SELECT tft.name, SUM(TIMESTAMPDIFF(etf.start, etf.end)) FROM KrasKrasBundle:ExtendedTimeFragment etf
            JOIN etf.timesheet t
            JOIN etf.timefragmenttype tft
            JOIN t.employee e
            WHERE e.supportcenter = ?1';

        $dql2 = 'SELECT lt.name, SUM(ul.duration) FROM KrasKrasBundle:LeaveDate ld
            JOIN ld.leave ul
            JOIN ul.leavetype lt
            LEFT JOIN ul.employees e
            WHERE (e IS NULL OR e.supportcenter = ?1)';

        $dql3 = 'SELECT ld.date, lt.name FROM KrasKrasBundle:LeaveDate ld
            JOIN ld.leave ul
            JOIN ul.leavetype lt
            LEFT JOIN ul.employees e
            WHERE (e IS NULL OR e.supportcenter = ?1)
            AND (lt.publicHoliday = TRUE OR lt.sickness = TRUE)';

        if ($start_date && $end_date) {
            $start_date->setTime(00, 00, 01);
            $end_date->setTime(23, 59, 59);

            $dql .= ' AND (etf.start <= :end AND etf.end >= :start)';
            $dql2 .= ' AND (ld.date <= :end AND ld.date >= :start)';
            $dql3 .= ' AND (ld.date <= :end AND ld.date >= :start)';
        } elseif ($start_date) {
            $start_date->setTime(00, 00, 01);

            $dql .= ' AND etf.start >= :start';
            $dql2 .= ' AND ld.date >= :start';
            $dql3 .= ' AND ld.date >= :start';
        } elseif ($end_date) {
            $end_date->setTime(23, 59, 59);

            $dql .= ' AND etf.end <= :end';
            $dql2 .= ' AND ld.date <= :end';
            $dql3 .= ' AND ld.date <= :end';
        }

        $dql .= ' GROUP BY etf.timefragmenttype';
        $dql2 .= ' GROUP BY ul.leavetype';

        $query = $em->createQuery($dql);
        $query->setParameters(array(
            1 => $sc,
        ));

        $query2 = $em->createQuery($dql2);
        $query2->setParameters(array(
            1 => $sc,
        ));

        $query3 = $em->createquery($dql3);
        $query3->setParameters(array(
            1 => $sc,
        ));

        if ($start_date && is_object($start_date)) {
            $query->setParameter('start', $start_date);
            $query2->setParameter('start', $start_date);
            $query3->setParameter('start', $start_date);
        }

        if ($end_date && is_object($end_date)) {
            $query->setParameter('end', $end_date);
            $query2->setParameter('end', $end_date);
            $query3->setParameter('end', $end_date);
        }

        $result = $query->getScalarResult();
        $result2 = $query2->getScalarResult();
        $result3 = $query3->getScalarResult();

        $result = array_map('array_values', $result);
        foreach ($result as $k => $res) {
            $result[$res[0]] = $res;
            unset($result[$k]);
        }

        $result2 = array_map('array_values', $result2);
        foreach ($result2 as $k => $res) {
            $result2[$res[0]] = $res;
            unset($result2[$k]);
        }

        $result3 = $this->get('kras.employee')->getSupportCentersWorkedTimeByDates(array($sc), $result3);

        foreach ($result2 as $res) {
            if (!array_key_exists($res[0], $result)) {
                $result[$res[0]] = array(
                    $res[0],
                    0,
                    '0.00 hours',
                );
            }

            $result[$res[0]][1] += $res[1];
        }

        foreach ($result3 as $res) {
            if (!array_key_exists($res[0], $result)) {
                $result[$res[0]] = array(
                    $res[0],
                    0,
                    '0.00 hours',
                );
            }

            $result[$res[0]][1] += $res[1];
        }

        foreach ($result as $key => $res) {
            $result[$key][1] = intval($res[1]);
            $result[$key][2] = number_format($res[1] / 3600, 2) . " hours";
        }

        $result = array_values($result);
        $response = json_encode($result);

        return new Response($response, 200, array('Content-Type' => 'application/json'));
    }
}
