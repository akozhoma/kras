<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\UserBundle\Entity\User;
use Kras\KrasBundle\Form\EmployeeType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;

/**
 * @Route("/employees")
 */
class EmployeeController extends Controller
{

    /**
     * @Route("/", name="kras_employee_index")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function indexAction()
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasUserBundle:User');

            $datatable->addWhereBuilderCallback(function($qb) use ($datatable, $sc) {
                $andExpr = $qb->expr()->andX();

                $andExpr->add($qb->expr()->eq('FosUser.supportcenter', $sc->getId()));

                $qb->andWhere($andExpr);
            });

            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);
            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }
        return array('supportcenter' => $sc);
    }

    /**
     * @Route("/{id}/show", name="kras_employee_show")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function showAction($id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasUserBundle:User')->findOneBy(array(
            'id' => $id,
            'supportcenter' => $sc
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/new", name="kras_employee_new")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function newAction()
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $entity = new User();
        $entity->addRole('ROLE_EMPLOYEE');
        $entity->setSupportCenter($sc);

        $form = $this->createForm(new EmployeeType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/create", name="kras_employee_create")
     * @Method("POST")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template("KrasKrasBundle:Employee:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $entity = new User();
        $entity->setSupportCenter($sc);

        $form = $this->createForm(new EmployeeType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // Log
            $this->get('kras.logging')->log($entity, 'add');

            return $this->redirect($this->generateUrl('kras_employee_show', array('id' => $entity->getId())));
        }

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="kras_employee_edit")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function editAction($id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasUserBundle:User')->findOneBy(array(
            'id' => $id,
            'supportcenter' => $sc
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createForm(new EmployeeType(), $entity);

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/update", name="kras_employee_update")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Method("POST")
     * @Template("KrasKrasBundle:Employee:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasUserBundle:User')->findOneBy(array(
            'id' => $id,
            'supportcenter' => $sc
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createForm(new EmployeeType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $pass = $editForm->get('plainPassword')->getData();
            if(!empty($pass)) {
                $encoder = $this->container
                ->get('security.encoder_factory')
                ->getEncoder($entity);

                $entity->setPassword($encoder
                        ->encodePassword($pass, $entity->getSalt()));
            }
            $em->persist($entity);
            $em->flush();

            // Log
            $this->get('kras.logging')->log($entity, 'update');

            return $this->redirect($this->generateUrl('kras_employee_show', array('id' => $id)));
        }

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/delete", name="kras_employee_delete")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasUserBundle:User')->findOneBy(array(
            'id' => $id,
            'supportcenter' => $sc,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $em->createQuery('UPDATE KrasUserBundle:User u SET u.enabled = 0 WHERE u.id = ' . $entity->getId())->execute();

        // Log
        $this->get('kras.logging')->log($entity, 'delete');

        return $this->redirect($this->generateUrl('kras_employee_index'));
    }
}
