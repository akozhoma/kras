<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\TimeFragmentType;
use Kras\KrasBundle\Form\TimeFragmentTypeType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;

/**
 * @Route("/timefragmenttypes")
 */
class TimeFragmentTypeController extends Controller
{
    /**
     * @Route("/", name="kras_timefragmenttype_index")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:TimeFragmentType');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);
            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }
        return array();
    }

    /**
     * @Route("/{id}/show", name="kras_timefragmenttype_show")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:TimeFragmentType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TimeFragmentType entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/new", name="kras_timefragmenttype_new")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function newAction()
    {
        $entity = new TimeFragmentType();
        $form = $this->createForm(new TimeFragmentTypeType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/create", name="kras_timefragmenttype_create")
     * @Method("POST")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template("KrasKrasBundle:TimeFragmentType:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new TimeFragmentType();
        $form = $this->createForm(new TimeFragmentTypeType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('kras_timefragmenttype_show', array('id' => $entity->getId())));
        }

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="kras_timefragmenttype_edit")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:TimeFragmentType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TimeFragmentType entity.');
        }

        $editForm = $this->createForm(new TimeFragmentTypeType(), $entity);

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/update", name="kras_timefragmenttype_update")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST")
     * @Template("KrasKrasBundle:TimeFragmentType:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:TimeFragmentType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TimeFragmentType entity.');
        }

        $editForm = $this->createForm(new TimeFragmentTypeType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('kras_timefragmenttype_show', array('id' => $id)));
        }

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/delete", name="kras_timefragmenttype_delete")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:TimeFragmentType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TimeFragmentType entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('kras_timefragmenttype_index'));
    }
}
