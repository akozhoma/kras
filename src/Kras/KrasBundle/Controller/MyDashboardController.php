<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;

/**
 * @Route("/dashboard")
 */
class MyDashboardController extends Controller
{
    /**
     * @Route("/", name="kras_mydashboard_index")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();

        $paginator  = $this->get('knp_paginator');

        $dql = "SELECT a FROM KrasKrasBundle:Activity a JOIN a.supportcenter sc LEFT JOIN a.attendancesheet s WHERE sc.id = " . $sc->getId() . " AND (s IS NULL OR s.notes IS NULL) ORDER BY a.start DESC";
        $query = $em->createQuery($dql);

        $activities = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page_1', 1),
            5,
            array('pageParameterName' => 'page_1')
        );

        $dql = "SELECT a FROM KrasKrasBundle:Activity a JOIN a.supportcenter sc LEFT JOIN a.attendancesheet s WHERE sc.id = " . $sc->getId() . " AND (s IS NULL OR s.notes IS NULL) ORDER BY a.start ASC";
        $activity_first = $em->createQuery($dql);
        $activity_first->setMaxResults(1);
        $activity_first = $activity_first->getOneOrNullResult();

        $dql = "SELECT t FROM KrasKrasBundle:Timesheet t
                WHERE t.employee = " . $this->getUser()->getId() . "
                AND t.approvedCo = FALSE
                AND t.approvedDi = FALSE
                AND t.approvedHr = FALSE
                AND t.submitted = FALSE
                AND ((t.year < " . date('Y') . ") OR (" . date('Y') . " = t.year AND t.week < " . date('W') . "))";
        $your_timesheets = $em->createQuery($dql);
        $your_timesheets = $your_timesheets->getResult();

        $data = array(
            'entity'          => $sc,
            'activities'      => $activities,
            'activity_first'  => $activity_first,
            'your_timesheets' => $your_timesheets,
        );

        $security = $this->get('security.context');

        if ($security->isGranted('ROLE_COORDINATOR') || $security->isGranted('ROLE_HR_MANAGER') || $security->isGranted('ROLE_DIRECTOR')) {
            $dql = "SELECT t FROM KrasKrasBundle:Timesheet t
                WHERE t.supportcenter = " . $sc->getId() . "
                AND t.approvedDi = FALSE
                AND t.approvedCo = FALSE
                AND t.approvedHr = FALSE
                AND ((t.year < " . date('Y') . ") OR (" . date('Y') . " = t.year AND t.week < " . date('W') . "))";

            if ($security->isGranted('ROLE_DIRECTOR')) {
                $dql = "SELECT t FROM KrasKrasBundle:Timesheet t
                    WHERE t.submitted = TRUE
                    AND t.approvedDi = FALSE
                    AND t.approvedCo = TRUE
                    AND t.approvedHr = FALSE";
            }

            if ($security->isGranted('ROLE_HR_MANAGER')) {
                $dql = "SELECT t FROM KrasKrasBundle:Timesheet t
                    WHERE t.submitted = TRUE
                    AND t.approvedDi = TRUE
                    AND t.approvedCo = TRUE
                    AND t.approvedHr = FALSE";
            }

            $query = $em->createQuery($dql);

            $timesheets = $paginator->paginate(
                $query,
                $this->get('request')->query->get('page_2', 1),
                5,
                array('pageParameterName' => 'page_2')
            );

            $data['timesheets'] = $timesheets;

            $start_date = new \DateTime();
            $start_date->setTime(01, 01, 01);
            $start_date->modify('-6 months');

            $dql = 'SELECT u AS employee, SUM(t.remainingOvertimeHours) AS overtime FROM KrasUserBundle:User u
                LEFT JOIN u.timesheets t
                WHERE t.overtimeHours > 0
                AND t.startDate > ?1
                AND u.supportcenter = ' . $sc->getId() . '
                GROUP BY u';

            if ($security->isGranted('ROLE_HR_MANAGER')) {
                $dql = 'SELECT u AS employee, SUM(t.remainingOvertimeHours) AS overtime FROM KrasUserBundle:User u
                    LEFT JOIN u.timesheets t
                    WHERE t.overtimeHours > 0
                    AND t.startDate > ?1
                    GROUP BY u';
            }

            $query = $em
                ->createQuery($dql)
                ->setParameter(1, $start_date);

            $overtime_employees = $query->getResult();

            foreach ($overtime_employees as $key => $overtime_employee) {
                if ($overtime_employee['overtime'] < 40) {
                    unset($overtime_employees[$key]);
                }
            }

            $data['overtime_employees'] = $overtime_employees;

            $dql = 'SELECT l, lt, ld, e
                FROM KrasKrasBundle:UserLeave l
                LEFT JOIN l.employees e
                JOIN l.leavetype lt
                JOIN l.leavedates ld
                WHERE l.approved IS NULL
                AND e.supportcenter = ' . $sc->getId();

            if ($security->isGranted('ROLE_HR_MANAGER')) {
                $dql = 'SELECT l, lt, ld, e
                    FROM KrasKrasBundle:UserLeave l
                    LEFT JOIN l.employees e
                    JOIN l.leavetype lt
                    JOIN l.leavedates ld
                    WHERE l.approved IS NULL';
            }

            $query = $em->createQuery($dql);
            $leaves = $query->getResult();

            $queued_leaves = array();

            foreach ($leaves as $leave) {

                if (!array_key_exists($leave->getIdentifier(), $queued_leaves)) {
                    $queued_leaves[$leave->getIdentifier()] = array(
                        'employees' => $leave->getEmployees()->toArray(),
                        'data'      => array(),
                        'type'      => $leave->getLeaveType(),
                    );
                }

                $queued_leaves[$leave->getIdentifier()]['data'][] = array(
                    $leave->getLeaveDates()->toArray(),
                    $leave->getStartTime(),
                    $leave->getEndTime(),
                );
            }

            $data['queued_leaves'] = $queued_leaves;
        }

        return $data;
    }

    /**
     * @Route("/json_logentries", name="kras_mydashboard_json_logentries")
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function jsonLogentriesAction(Request $request)
    {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:LogEntry');

        $datatable->addWhereBuilderCallback(function($qb) use ($datatable, $sc) {
            $qb->innerJoin('LogEntry.member', 'Member');
            $qb->innerJoin('Member.supportcenter', 'SupportCenter');

            $andExpr = $qb->expr()->andX();
            $andExpr->add($qb->expr()->eq('SupportCenter.id', $sc->getId()));
            $qb->andWhere($andExpr);

            $qb->orderBy('LogEntry.id', 'desc');
        });

        $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);

        $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
        $response = json_encode($response);

        return new Response($response, 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/json_trainings", name="kras_mydashboard_json_trainings")
     * @Secure(roles="ROLE_COORDINATOR")
     */
    public function jsonTrainingsAction(Request $request)
    {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:Training');

        $datatable->addWhereBuilderCallback(function($qb) use ($datatable, $sc) {
            $qb->innerJoin('Training.employee', 'Employee');
            $qb->innerJoin('Employee.supportcenter', 'SupportCenter');

            $andExpr = $qb->expr()->andX();
            $andExpr->add($qb->expr()->eq('SupportCenter.id', $sc->getId()));
            $qb->andWhere($andExpr);

            $qb->andWhere('Training.approved IS NULL');
        });

        $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);

        $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
        $response = json_encode($response);

        return new Response($response, 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/json_activities", name="kras_mydashboard_json_activities")
     */
    public function jsonActivitiesAction(Request $request)
    {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:Activity');

        $datatable->addWhereBuilderCallback(function($qb) use ($datatable, $sc) {
            $andExpr = $qb->expr()->andX();
            $qb->innerJoin('Activity.supportcenter', 'sc');
            $andExpr->add($qb->expr()->eq('sc.id', $sc->getId()));
            $qb->andWhere($andExpr);

            $week_start = new \DateTime('this week');
            $week_start->setTime(00, 00, 01);
            $week_end = new \DateTime('this week +6 days');
            $week_end->setTime(23, 23, 59);

            $qb->andWhere('(Activity.start >= :start) AND (Activity.end <= :end)');
            $qb->setParameter('start', $week_start);
            $qb->setParameter('end', $week_end);
        });

        $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);

        $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
        $response = json_encode($response);

        return new Response($response, 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/json_data", name="kras_mydashboard_json_data")
     * @Secure(roles="ROLE_EMPLOYEE")
     */
    public function jsonDataAction(Request $request)
    {
        $sc = $this->getUser()->getSupportCenter();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $response = $this->getGraphData($sc);

        $response = json_encode($response);
        return new Response($response, 200, array('Content-Type' => 'application/json'));
    }

    private function getGraphData($supportcenter)
    {
        $em = $this->getDoctrine()->getManager();

        $data = array();

        $end = new \DateTime();
        $end->modify('yesterday');
        $end->modify('next sunday');
        $end->setTime('23', '59', '59');
        $start = clone $end;
        $start->modify('-25 weeks');
        $start->modify('last monday');
        $start->setTime('00', '00', '01');

        $sections = $supportcenter->getSections();

        $dtemp = clone $end;
        $dtemptwo = clone $end;
        $dtemptwo->modify('-25 weeks');

        while ($dtemptwo->format('m-Y') != $dtemp->format('m-Y')) {
            foreach ($sections as $section) {
                $data[$section->getId()][$dtemptwo->format('m-Y')] = array(
                    'average_attendance' => 0,
                    'activity_count'     => 0,
                );
            }

            $dtemptwo->modify('+1 week');
        }

        foreach($sections as $section) {
            $qb = $em->createQueryBuilder();
            $qb->select('a');
            $qb->from('KrasKrasBundle:Activity', 'a');
            $qb->join('a.attendancesheet', 's');
            $qb->join('a.sections', 'se');
            $qb->where('(a.start <= :end) AND (a.end >= :start)');
            $qb->andWhere('se.id = :section');
            $qb->setParameter('section', $section);
            $qb->setParameter('start', $start->format('Y-m-d H:i:s'));
            $qb->setParameter('end', $end->format('Y-m-d H:i:s'));

            $activities = $qb->getQuery()->getResult();

            foreach($activities as $activity) {
                if (!array_key_exists($activity->getStart()->format('m-Y'), $data[$section->getId()])) {
                    $data[$section->getId()][$activity->getStart()->format('m-Y')] = array(
                        'average_attendance' => 0,
                        'activity_count'     => 0,
                    );
                }

                $data[$section->getId()][$activity->getStart()->format('m-Y')]['activity_count']++;
                $data[$section->getId()][$activity->getStart()->format('m-Y')]['average_attendance'] =
                    (($data[$section->getId()][$activity->getStart()->format('m-Y')]['average_attendance']
                     * ($data[$section->getId()][$activity->getStart()->format('m-Y')]['activity_count'] - 1))
                     + $activity->getAttendanceSheet()->getMembers()->count())
                     / $data[$section->getId()][$activity->getStart()->format('m-Y')]['activity_count'];
            }
        }

        return $data;
    }
}
