<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\Timesheet;
use Kras\KrasBundle\Entity\Overtime;
use Kras\KrasBundle\Entity\LeaveDate;
use Kras\KrasBundle\Entity\UserLeave;
use Kras\KrasBundle\Entity\ExtendedTimeFragment;
use Kras\KrasBundle\Form\ExtendedTimeFragmentType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;
use JMS\Serializer\SerializationContext;

/**
 * @Route("/timesheet")
 */
class MyTimesheetController extends Controller
{
    /**
     * @Route("/", name="kras_mytimesheet_index")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $user = $this->getUser();

            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:Timesheet');

            $datatable->addWhereBuilderCallback(function($qb) use ($datatable, $user) {
                $andExpr = $qb->expr()->andX();
                $andExpr->add($qb->expr()->eq('Timesheet.employee', $user->getId()));
                $qb->andWhere($andExpr);
            });

            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);

            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }

        return array();
    }

    /**
     * @Route("/{id}/show", name="kras_mytimesheet_show")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'       => $id,
            'employee' => $this->getUser(),
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $dql = 'SELECT t FROM KrasKrasBundle:Timesheet t WHERE t.employee = ?1 AND t.id > ?2 ORDER BY t.id ASC';
        $query = $em->createQuery($dql);
        $query->setParameters(array(
            1 => $this->getUser(),
            2 => $id,
        ));
        $query->setMaxResults(1);
        $next = $query->getOneOrNullResult();

        $dql = 'SELECT t FROM KrasKrasBundle:Timesheet t WHERE t.employee = ?1 AND t.id < ?2 ORDER BY t.id DESC';
        $query = $em->createQuery($dql);
        $query->setParameters(array(
            1 => $this->getUser(),
            2 => $id,
        ));
        $query->setMaxResults(1);
        $previous = $query->getOneOrNullResult();

        $frag = new ExtendedTimeFragment();
        $frag->setTimesheet($entity);
        $form = $this->createForm(new ExtendedTimeFragmentType(), $frag);

        $dql = 'SELECT tft FROM KrasKrasBundle:TimeFragmentType tft';
        $query = $em->createQuery($dql);
        $types = $query->getResult();

        return array(
            'entity'         => $entity,
            'previous'       => $previous,
            'next'           => $next,
            'form'           => $form->createView(),
            'fragment_types' => $types,
        );
    }

    /**
     * @Route("/new", name="kras_mytimesheet_new")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $entity = $this->get('kras.employee')->getFirstNonExistantTimesheet();

        if (!$entity) {
            $request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('You can\'t create a new timesheet before the start of the next week.'));

            return $this->redirect($this->generateUrl('kras_mytimesheet_index'));
        }

        // Log
        $this->get('kras.logging')->log($entity, 'add');

        return $this->redirect($this->generateUrl('kras_mytimesheet_show', array('id' => $entity->getId())));
    }

    /**
     * @Route("/{id}/leaves", name="kras_mytimesheet_leaves")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Template()
     */
    public function leavesAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getMAnager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'       => $id,
            'employee' => $this->getUser(),
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $entities = $this->get('kras.employee')->getLeavesByTimesheetAndEmploe($entity);

        $output = $this->get('jms_serializer')->serialize($entities, 'json');

        return new Response($output, 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/data", name="kras_mytimesheet_data")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("POST|GET")
     */
    public function eventListAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'       => $id,
            'employee' => $this->getUser(),
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $output = $this->get('jms_serializer')->serialize($entity, 'json');

        return new Response($output, 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/comments", name="kras_mytimesheet_comments")
     * @Secure(roles="ROLE_HR_MANAGER")
     * @Method("POST")
     */
    public function commentsAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'         => $id,
            'employee'   => $this->getUser(),
            'approvedCo' => false,
            'approvedHr' => false,
            'approvedDi' => false,
            'submitted'  => false,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $comments = $request->get('comments');
        $entity->setComments($comments);

        $em->persist($entity);
        $em->flush();

        return new Response('OK', 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/template_events/{id}", name="kras_mytimesheet_template_event_list")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("POST|GET")
     */
    public function templateEventListAction(Request $request, $id)
    {
        $template = $this->get('kras.employee')->getActiveOrDefaultTimesheetTemplate();

        $template = $this->get('kras.employee')->removeExtraFragments($template, $id);

        $output = $this->get('jms_serializer')->serialize($template, 'json');

        return new Response($output, 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/events/create", name="kras_mytimesheet_event_create")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("POST|GET")
     */
    public function eventCreateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'         => $id,
            'employee'   => $this->getUser(),
            'approvedCo' => false,
            'approvedDi' => false,
            'approvedHr' => false,
            'submitted'  => false,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $extended_fragment = new ExtendedTimeFragment();
        $extended_fragment->setTimesheet($entity);
        $form = $this->createForm(new ExtendedTimeFragmentType(), $extended_fragment);

        $form->bind($request);

        if (!$form->isValid()) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $start = clone ($entity->getStartDate());
        while($start->format('N') != $form->get('day_of_week')->getData()) {
            $start->modify('+1 day');
        }

        $start_time_data = $form->get('start_time')->getData();
        $start->setTime($start_time_data['hour'], $start_time_data['minute'], 0);

        $end = clone $start;
        $end_time_data = $form->get('end_time')->getData();
        $end->setTime($end_time_data['hour'], $end_time_data['minute'], 0);

        if ($end->format('U') <= $start->format('U')) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $dql = 'SELECT count(etf.id) FROM KrasKrasBundle:ExtendedTimeFragment etf
            JOIN etf.timesheet ts
            WHERE ts.employee = ?1
            AND (etf.start <  ?3 AND etf.end > ?2)
            AND ts.id = ?4';
        $query = $em->createQuery($dql)->setParameters(
            array(
                1 => $this->getUser(),
                2 => $start,
                3 => $end,
                4 => $entity->getId(),
            )
        );
        $hasTimeFragment = $query->getSingleScalarResult();

        if ($hasTimeFragment) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $dql = 'SELECT count(ul.id) FROM KrasKrasBundle:UserLeave ul
            JOIN ul.leavedates ld
            LEFT JOIN ul.employees e
            WHERE ld.date = ?1
            AND (e IS NULL OR e.id = ?2)
            AND (ul.startTime < ?4 AND ul.endTime > ?3)';
        $query = $em->createQuery($dql)->setParameters(array(
            1 => $start->format('Y-m-d'),
            2 => $this->getUser(),
            3 => $start,
            4 => $end,
        ));
        $hasLeave = $query->getSingleScalarResult();

        if ($hasLeave) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $extended_fragment
            ->setStart($start)
            ->setEnd($end);

        if ($extended_fragment->getTimeFragmentType()->getLinkActivity()) {
            $extended_fragment->setLinkedActivity($form->get('activity')->getData());
        }

        if ($extended_fragment->getTimeFragmentType()->getLinkMember()) {
            $extended_fragment->setLinkedMember($form->get('member')->getData());
        }

        if ($extended_fragment->getTimeFragmentType()->getLinkTraining()) {
            $extended_fragment->setLinkedTraining($form->get('training')->getData());
        }

        $em->persist($extended_fragment);
        $em->flush();

        $em->refresh($entity);
        $this->get('kras.employee')->recalculateTimesheetWorkedHours($entity);

        $em->persist($entity);
        $em->flush();

        return new Response('OK', 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/events/{event_id}/duration_update", name="kras_mytimesheet_event_duration_update")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("POST|GET")
     */
    public function eventDurationUpdateAction(Request $request, $id, $event_id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'         => $id,
            'employee'   => $this->getUser(),
            'approvedCo' => false,
            'approvedDi' => false,
            'approvedHr' => false,
            'submitted'  => false,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $extended_fragment = $em->getRepository('KrasKrasBundle:ExtendedTimeFragment')->findOneBy(array(
            'id'        => $event_id,
            'timesheet' => $id,
        ));

        if (!$extended_fragment) {
            throw $this->createNotFoundException('Unable to find ExtendedTimeFragment entity.');
        }

        if (!in_array(intval($request->get('day_of_week')), range(0, 6))) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $start = clone ($entity->getStartDate());
        while($start->format('N') != intval($request->get('day_of_week'))) {
            $start->modify('+1 day');
        }

        $start_time_data = explode(':', $request->get('start'));
        $start->setTime($start_time_data[0], $start_time_data[1], 0);

        $end = clone $start;
        $end_time_data = explode(':', $request->get('end'));
        $end->setTime($end_time_data[0], $end_time_data[1], 0);

        if ($end->format('U') <= $start->format('U')) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $dql = 'SELECT count(etf.id) FROM KrasKrasBundle:ExtendedTimeFragment etf
            JOIN etf.timesheet ts
            WHERE ts.employee = ?1
            AND (etf.start <  ?3 AND etf.end > ?2)
            AND etf.id != ?4';
        $query = $em->createQuery($dql)->setParameters(array(
            1 => $this->getUser(),
            2 => $start,
            3 => $end,
            4 => $extended_fragment->getId(),
        ));
        $hasTimeFragment = $query->getSingleScalarResult();

        if ($hasTimeFragment) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $dql = 'SELECT count(ul.id) FROM KrasKrasBundle:UserLeave ul
            JOIN ul.leavedates ld
            LEFT JOIN ul.employees e
            WHERE ld.date = ?1
            AND (e IS NULL OR e.id = ?2)
            AND (ul.startTime < ?4 AND ul.endTime > ?3)';
        $query = $em->createQuery($dql)->setParameters(array(
            1 => $start->format('Y-m-d'),
            2 => $this->getUser(),
            3 => $start,
            4 => $end,
        ));
        $hasLeave = $query->getSingleScalarResult();

        if ($hasLeave) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $extended_fragment
            ->setStart($start)
            ->setEnd($end);

        $em->persist($extended_fragment);
        $em->flush();

        $em->refresh($entity);
        $this->get('kras.employee')->recalculateTimesheetWorkedHours($entity);

        $em->persist($entity);
        $em->flush();

        return new Response('OK', 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/events/{event_id}/update", name="kras_mytimesheet_event_update")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("POST|GET")
     */
    public function eventUpdateAction(Request $request, $id, $event_id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'         => $id,
            'employee'   => $this->getUser(),
            'approvedCo' => false,
            'approvedHr' => false,
            'approvedDi' => false,
            'submitted'  => false,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $extended_fragment = $em->getRepository('KrasKrasBundle:ExtendedTimeFragment')->findOneBy(array(
            'id'        => $event_id,
            'timesheet' => $id,
        ));

        if (!$extended_fragment) {
            throw $this->createNotFoundException('Unable to find ExtendedTimeFragment entity.');
        }

        $form = $this->createForm(new ExtendedTimeFragmentType(), $extended_fragment);
        $form->submit($request);

        if (!$form->isValid()) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $start = clone ($entity->getStartDate());
        while($start->format('N') != $form->get('day_of_week')->getData()) {
            $start->modify('+1 day');
        }

        $start_time_data = $form->get('start_time')->getData();
        $start->setTime($start_time_data['hour'], $start_time_data['minute'], 0);

        $end = clone $start;
        $end_time_data = $form->get('end_time')->getData();
        $end->setTime($end_time_data['hour'], $end_time_data['minute'], 0);

        if ($end->format('U') <= $start->format('U')) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $dql = 'SELECT count(etf.id) FROM KrasKrasBundle:ExtendedTimeFragment etf
            JOIN etf.timesheet ts
            WHERE ts.employee = ?1
            AND (etf.start <  ?3 AND etf.end > ?2)
            AND etf.id != ?4
            AND ts.id = ?5';
        $query = $em->createQuery($dql)->setParameters(
            array(
                1 => $this->getUser(),
                2 => $start,
                3 => $end,
                4 => $extended_fragment->getId(),
                5 => $entity->getId(),
            )
        );
        $hasTimeFragment = $query->getSingleScalarResult();

        if ($hasTimeFragment) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $dql = 'SELECT count(ul.id) FROM KrasKrasBundle:UserLeave ul
            JOIN ul.leavedates ld
            LEFT JOIN ul.employees e
            WHERE ld.date = ?1
            AND (e IS NULL OR e.id = ?2)
            AND (ul.startTime < ?4 AND ul.endTime > ?3)';
        $query = $em->createQuery($dql)->setParameters(array(
            1 => $start->format('Y-m-d'),
            2 => $this->getUser(),
            3 => $start,
            4 => $end,
        ));
        $hasLeave = $query->getSingleScalarResult();

        if ($hasLeave) {
            return new Response('INVALID', 200, array('Content-Type' => 'application/json'));
        }

        $extended_fragment
            ->setStart($start)
            ->setEnd($end);

        if ($extended_fragment->getTimeFragmentType()->getLinkActivity()) {
            $extended_fragment->setLinkedActivity($form->get('activity')->getData());
        } else {
            $extended_fragment->setLinkedActivity(null);
        }

        if ($extended_fragment->getTimeFragmentType()->getLinkMember()) {
            $extended_fragment->setLinkedMember($form->get('member')->getData());
        } else {
            $extended_fragment->setLinkedMember(null);
        }

        if ($extended_fragment->getTimeFragmentType()->getLinkTraining()) {
            $extended_fragment->setLinkedTraining($form->get('training')->getData());
        } else {
            $extended_fragment->setLinkedTraining(null);
        }

        $em->persist($extended_fragment);
        $em->flush();

        $em->refresh($entity);
        $this->get('kras.employee')->recalculateTimesheetWorkedHours($entity);

        $em->persist($entity);
        $em->flush();

        return new Response('OK', 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/events/{event_id}/delete", name="kras_mytimesheet_event_delete")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("POST|GET")
     */
    public function eventDeleteAction(Request $request, $id, $event_id)
    {
        $em = $this->getDoctrine()->getManager();

        $t = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'         => $id,
            'employee'   => $this->getUser(),
            'approvedCo' => false,
            'approvedDi' => false,
            'approvedHr' => false,
            'submitted'  => false,
        ));

        if (!$t) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        $entity = $em->getRepository('KrasKrasBundle:ExtendedTimeFragment')->findOneBy(array(
            'id'        => $event_id,
            'timesheet' => $id,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ExtendedTimeFragment entity.');
        }

        $em->remove($entity);
        $em->flush();

        $em->refresh($t);
        $this->get('kras.employee')->recalculateTimesheetWorkedHours($t);

        $em->persist($t);
        $em->flush();

        return new Response('OK', 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/{id}/submit", name="kras_mytimesheet_submit")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("POST|GET")
     */
    public function submitAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'         => $id,
            'employee'   => $this->getUser(),
            'approvedCo' => false,
            'approvedHr' => false,
            'approvedDi' => false,
            'submitted'  => false,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        if (!$this->getUser()->hasSudoMode()) {
            return $this->redirect($this->generateUrl(
                'kras_sudo_form',
                array(
                    'redirect' => $this->generateUrl(
                        $request->get('_route'),
                        $request->get('_route_params')
                    ),
                    'previous' => $request->headers->get('referer'),
                )
            ));
        }

        if (!$entity->isSubmittable()) {
            $request->getSession()->getFlashBag()->add(
                'error',
                $this->get('translator')->trans('You can\'t submit this timesheet because you are missing %hours% hours of activities.', array('%hours%' => abs($entity->getWorkedHours() - $entity->getRequiredHours())))
            );

            return $this->redirect($this->generateUrl('kras_mytimesheet_show', array('id' => $entity->getId())));
        }

        $entity->setSubmitted(true);

        $security = $this->get('security.context');

        if ($security->isGranted('ROLE_COORDINATOR')) {
            $entity
                ->setApprovedCo(true);
        }

        if ($security->isGranted('ROLE_HR_MANAGER')) {
            $entity
                ->setApprovedHr(true);
        }

        if ((!$security->isGranted('ROLE_COORDINATOR') && !$security->isGranted('ROLE_HR_MANAGER')) || $security->isGranted('ROLE_DIRECTOR')) {
            $entity
                ->setApprovedDi(true);
        }

        $em->persist($entity);
        $em->flush();

        $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans(
            'Your timesheet has been submitted and is currently awaiting approval.'
        ));

        return $this->redirect($this->generateUrl('kras_mytimesheet_show', array('id' => $entity->getId())));
    }

    /**
     * @Route("/{id}/autofill", name="kras_mytimesheet_autofill")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("POST|GET")
     */
    public function autofillAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('KrasKrasBundle:Timesheet')->findOneBy(array(
            'id'         => $id,
            'employee'   => $this->getUser(),
            'approvedCo' => false,
            'approvedHr' => false,
            'approvedDi' => false,
            'submitted'  => false,
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timesheet entity.');
        }

        if (($entity->getWorkedHours() - $entity->getRequiredHours()) >= 0) {
            $request->getSession()->getFlashBag()->add(
                'error',
                $this->get('translator')->trans('Can\'t automatically fill a timesheet which is already full.')
            );

            return $this->redirect($this->generateUrl('kras_mytimesheet_show', array('id' => $entity->getId())));
        }

        $duration = abs($entity->getWorkedHours() - $entity->getRequiredHours()) * 3600;
        $duration = $duration > 86400 ? 86400 : $duration;

        if (($duration / 3600) > $this->get('kras.employee')->getRemainingOvertime($this->getUser())) {
            $request->getSession()->getFlashBag()->add(
                'error',
                $this->get('translator')->trans('You are not allowed to take this much leave.')
            );

            return $this->redirect($this->generateUrl('kras_mytimesheet_show', array('id' => $entity->getId())));
        }

        $date = clone ($entity->getStartDate());
        $date->modify('+6 days');

        $leave_date = new LeaveDate();
        $leave_date->setDate($date);

        $dt1 = new \DateTime();
        $dt1->setTime(00,00,00);

        $dt2 = clone $dt1;
        $dt2->modify('+'.$duration.' seconds');

        $leave = new UserLeave();
        $leave->addLeaveDate($leave_date);
        $leave->addEmployee($this->getUser());
        $leave->setLeaveType($em->getRepository('KrasKrasBundle:LeaveType')->findOneByOvertime(TRUE));
        $leave->setStartTime($dt1);
        $leave->setEndTime($dt2);
        $leave->setApproved(true);

        $em->persist($leave);
        $em->flush();

        $em->refresh($leave);

        foreach ($leave->getEmployees() as $employee) {
            $this->get('kras.employee')->scheduleOvertime(
                $employee,
                ($leave->getDuration() / $leave->getLeaveDates()->count()),
                $leave->getLeaveDates()->toArray()
            );
        }

        $this->get('kras.employee')->recalculateTimesheetWorkedHoursByLeave($leave);

        $request->getSession()->getFlashBag()->add('info', $this->get('translator')->trans(
            'Your timesheet has been filled with overtime.'
        ));

        return $this->redirect($this->generateUrl('kras_mytimesheet_show', array('id' => $entity->getId())));
    }
}
