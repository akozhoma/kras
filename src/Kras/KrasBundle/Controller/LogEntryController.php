<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\LogEntry;
use Kras\KrasBundle\Form\LogEntryType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;

/**
 * @Route("/logentries")
 */
class LogEntryController extends Controller
{
    /**
     * @Route("/", name="kras_logentry_index")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:LogEntry');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);
            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }
        return array();
    }

    /**
     * @Route("/{id}/show", name="kras_logentry_show")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:LogEntry')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LogEntry entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/new", name="kras_logentry_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function newAction()
    {
        $entity = new LogEntry();
        $form = $this->createForm(new LogEntryType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/create", name="kras_logentry_create")
     * @Method("POST")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("KrasKrasBundle:LogEntry:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new LogEntry();
        $form = $this->createForm(new LogEntryType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('kras_logentry_show', array('id' => $entity->getId())));
        }

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="kras_logentry_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:LogEntry')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LogEntry entity.');
        }

        $editForm = $this->createForm(new LogEntryType(), $entity);

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/update", name="kras_logentry_update")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("KrasKrasBundle:LogEntry:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:LogEntry')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LogEntry entity.');
        }

        $editForm = $this->createForm(new LogEntryType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('kras_logentry_show', array('id' => $id)));
        }

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/{id}/delete", name="kras_logentry_delete")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KrasKrasBundle:LogEntry')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LogEntry entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('kras_logentry_index'));
    }

    /**
     * @Route("/create_ajax", name="kras_logentry_create_ajax")
     * @Method("POST")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function ajaxCreateAction(Request $request)
    {
        $entity = new LogEntry();
        $form = $this->createForm(new LogEntryType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return new Response(json_encode("OK"), 200, array('Content-Type' => 'application/json'));
        }

        return new Response(json_encode("ERROR"), 500, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/json_title_list", name="kras_logentry_json_title_list")
     * @Secure(roles="ROLE_EMPLOYEE")
     * @Method("GET")
     */
    public function jsonNameListAction() {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb->select('DISTINCT le.title');
        $qb->from('KrasKrasBundle:LogEntry', 'le');
        //$qb->orderBy('le.id', 'DESC');
        //$qb->setMaxResults(50);

        $result = $qb->getQuery()->getScalarResult();

        $grouped = array_map('current', $result);

        $response = $grouped;

        $response = json_encode($response);
        return new Response($response, 200, array('Content-Type' => 'application/json'));
    }
}
