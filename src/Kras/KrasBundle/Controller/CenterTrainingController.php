<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;
use Kras\KrasBundle\Entity\ExtendedTimeFragment;

/**
 * @Route("/center_training")
 */
class CenterTrainingController extends Controller
{
    /**
     * @Route("/", name="kras_center_training_index")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function indexAction()
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        if ($this->getRequest()->isXmlHttpRequest()) {
            $datatable = $this->get('lankit_datatables')->getDatatable('KrasKrasBundle:Training');
            $datatable->setDefaultJoinType(Datatable::JOIN_LEFT);

            $user = $this->getUser();
            $datatable->addWhereBuilderCallback(function($qb) use ($datatable, $sc) {
                $qb->innerJoin('Training.employee', 'Employee');

                $andExpr = $qb->expr()->andX();
                $andExpr->add($qb->expr()->eq('Employee.supportcenter', $sc->getId()));
                $qb->andWhere($andExpr);
            });

            $response = $datatable->getSearchResults(Datatable::RESULT_ARRAY);
            $response = json_encode($response);
            return new Response($response, 200, array('Content-Type' => 'application/json'));
        }

        return array('supportcenter' => $sc);
    }

    /**
     * @Route("/{id}/show", name="kras_center_training_show")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function showAction(Request $request, $id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $dql = 'SELECT t FROM KrasKrasBundle:Training t JOIN t.employee e WHERE t.id = ?1 AND e.supportcenter = ?2';
        $entity = $em->createQuery($dql)->setParameters(array(
            1 => $id,
            2 => $sc,
        ))->getOneOrNullResult();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Training entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/{id}/approve", name="kras_center_training_approve")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function approveAction(Request $request, $id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $dql = 'SELECT t FROM KrasKrasBundle:Training t JOIN t.employee e WHERE t.id = ?1 AND e.supportcenter = ?2';
        $entity = $em->createQuery($dql)->setParameters(array(
            1 => $id,
            2 => $sc,
        ))->getOneOrNullResult();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Training entity.');
        }

        $entity->setApproved(true);
        $request->getSession()->getFlashBag()->add(
            'success',
            $this->get('translator')->trans('The Training has been approved successfully.')
        );

        $em->persist($entity);
        $em->flush();

        $this->get('kras.employee')->scheduleTraining($entity);

        return $this->redirect($this->generateUrl('kras_center_training_show', array('id' => $entity->getId())));
    }

    /**
     * @Route("/{id}/reject", name="kras_center_training_reject")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function rejectAction(Request $request, $id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $dql = 'SELECT t FROM KrasKrasBundle:Training t JOIN t.employee e WHERE t.id = ?1 AND e.supportcenter = ?2';
        $entity = $em->createQuery($dql)->setParameters(array(
            1 => $id,
            2 => $sc,
        ))->getOneOrNullResult();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Training entity.');
        }

        $entity->setApproved(false);
        $request->getSession()->getFlashBag()->add(
            'success',
            $this->get('translator')->trans('The Training has been rejected successfully.')
        );

        $em->persist($entity);
        $em->flush();

        $this->get('kras.employee')->unscheduleTraining($entity);

        return $this->redirect($this->generateUrl('kras_center_training_show', array('id' => $entity->getId())));
    }

    /**
     * @Route("/{id}/delete", name="kras_center_training_delete")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $sc = $this->getUser()->getCoordinating();
        if($sc == null || empty($sc)) {
            $this->get('session')->getFlashBag()->add('error', 'You are not assigned to a support center.');
            return $this->redirect($this->generateUrl('kras_index'));
        }

        $em = $this->getDoctrine()->getManager();
        $dql = 'SELECT t FROM KrasKrasBundle:Training t JOIN t.employee e WHERE t.id = ?1 AND e.supportcenter = ?2';
        $entity = $em->createQuery($dql)->setParameters(array(
            1 => $id,
            2 => $sc,
        ))->getOneOrNullResult();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Training entity.');
        }

        // Log
        $this->get('kras.logging')->log($entity, 'delete');
        $this->get('kras.employee')->unscheduleTraining($entity);

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('kras_center_training_index'));
    }
}
