<?php

namespace Kras\KrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kras\KrasBundle\Entity\SupportCenter;
use Kras\KrasBundle\Form\MySupportCenterType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use LanKit\DatatablesBundle\Datatables\DataTable;

/**
 * @Route("/supportcenter")
 */
class MySupportCenterController extends Controller
{
    /**
     * @Route("/", name="kras_mysupportcenter_index")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function indexAction()
    {
        $entity = $this->getUser()->getCoordinating();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SupportCenter entity.');
        }

        return array('entity' => $entity);
    }

    /**
     * @Route("/edit", name="kras_mysupportcenter_edit")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Template()
     */
    public function editAction()
    {
        $entity = $this->getUser()->getCoordinating();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SupportCenter entity.');
        }

        $editForm = $this->createForm(new MySupportCenterType(), $entity);

        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/update", name="kras_mysupportcenter_update")
     * @Secure(roles="ROLE_COORDINATOR")
     * @Method("POST")
     * @Template("KrasKrasBundle:MySupportCenter:edit.html.twig")
     */
    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getUser()->getCoordinating();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SupportCenter entity.');
        }

        $editForm = $this->createForm(new MySupportCenterType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {

            foreach($editForm->get('sections')->getData() as $section) {
                $section->setSupportCenter($entity);
            }

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('kras_mysupportcenter_index'));
        }

        return array('entity' => $entity, 'form' => $editForm->createView());
    }
}
