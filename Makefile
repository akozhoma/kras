default: update-dev

update:
	app/console doctrine:schema:update --force
	app/console cache:clear --env=prod
	app/console assets:install --symlink web
	app/console assetic:dump --env=prod

update-dev:
	app/console doctrine:schema:update --force
	app/console cache:clear
	app/console assets:install --symlink web
	app/console assetic:dump --env=prod
