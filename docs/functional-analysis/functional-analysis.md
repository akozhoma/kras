% Kras VZW - REX2
% Jeroen Budts - jeroen@inuits.eu

# Introduction
## Organizational structure
### Support Centers & Sections
The organization is divided in different _support centers_:

* Berchem
* Zuid
* Noord
* Sport
* Centraal Secretariaat
* Merksem
* Oud-Borgerhout
* Kiel

Each _support center_ has different _sections_. Examples of such _sections_ are:

* Children
* Teens
* Girls
* Youth
* Circus

Each _support center_ can create their own _sections_ depending on the local needs. Sections are targetted toward an _age_ group and one or both _genders_.

### Employees
The application will be used by the _employees_ of Kras VZW. _Employees_ will have a role with related access permissions so allow/disallow the employee to perform specific operations in the system.  
The following roles will be foreseen:

Administrator
:   Will have access to the entire system, including all the _support centers_ and the _employee_ management.
Coordinator
:   Will have full access to everything related to his assigned _support center_ including _employee_ management (but only for that _support center_)
Employee
:   Will only have access to their own _support center_ and can not access other _support centers_ or the _employee_ management.
Intern
:   Will have limited access to their assigned _support center_.

### Employee management
The following roles have access to the _employee_ management:

* The `admin` role has full access for all the _support centers_.
* The `coordinator` role can only access his assigned _support center_.
* An `employee` and `intern` can only change his/her own password

The following actions can be performed in the _employee_ management:

* Add _employee_.
* Edit _employee_ (details + _support center_ assignation)

An _employee_ has the following properties:

| property       | type | required | comments                              |
|----------------|------|----------|---------------------------------------|
| id             | int  | yes      |                                       |
| name           | text | yes      |                                       |
| firstname      | text | yes      |                                       |
| username       | text | yes      |                                       |
| password       | hash | yes      |                                       |
| support center | int  | yes      | the id of the assigned support center |
| active         | bool | yes      |                                       |
| contract type  | int  | yes      | limited or unlimited in time          |
| end date       | date |          | only for a limited contract           |

**Remark**: when the contract of an employee expires (end date for limited contract is before the current date) the user should be set to inactive.

### Support center management
Kras VZW is divided in several _support centers_. _Support centers_ can be added & edited by the `admin` role. The `coordinator` role can also edit his assigned _support center_.

A _support center_ has the following properties:

| property       | type | required | comments                       |
|----------------|------|----------|--------------------------------|
| id             | int  | yes      |                                |
| name           | text | yes      |                                |
| street         | text |          |                                |
| number         | text |          |                                |
| postal code    | text |          |                                |
| city           | text |          |                                |
| coordinator_id | int  | yes      | employee id of the Coordinator |

Each _support center_ can have one or more _sections_. The sections can be edited by the `admin` role for all the _support centers_, and by the `coordinator` role (only his assigned _Support center_).

A _section_ has the following properties:

| property    | type | required | comments                |
|-------------|------|----------|-------------------------|
| id          | int  | yes      |                         |
| name        | text | yes      |                         |
| minimum age | int  |          |                         |
| maximum age | int  |          |                         |
| target      | id   | yes      | see valid target values |
| description | text |          | free text               |

Valid target values are

| id | description          |
|----|----------------------|
| 1  | Mixed (boys & girls) |
| 2  | Boys                 |
| 3  | Girls                |

# Member & activity management
## Member management
The following roles have access to the _member_ management:

* The `admin` role can access the members for all the _support centers_.
* The `coordinator`, `employee` and `intern` roles can only access the members of their own assigned
_support center_.

A _member_ has the following properties:

| property                          | type    | required | comments                           |
|-----------------------------------|---------|----------|------------------------------------|
| id                                | int     | yes      |                                    |
| firstname                         | text    | yes      |                                    |
| lastname                          | text    | yes      |                                    |
| gender                            | list    | yes      | Male or Female                     |
| member_type                       | list    | yes      | Regular member or Volunteer        |
| address                           | text    | no       |                                    |
| postal_code                       | text    | no       |                                    |
| city                              | text    | no       |                                    |
| email                             | text    | no       |                                    |
| phonenumbers                      | list    | no       |                                    |
| birthday                          | date    | no       |                                    |
| birthday_estimation [^estimation] | boolean | no       |                                    |
| nationality                       | int     | no       | select list from all nationalities |
| country_of_origin                 | int     | no       | select list from all countries     |
| mother_tongue                     | int     | no       | select list from all languages     |
|                                   |         |          |                                    |

[^estimation]: Some people don't know their exact birthday. In this case an estimation is made and this field is checked. 

# Timesheeting

The timesheeting part of the application will be used by employees to register the worked time.

## Global configuration
Each year the HR-manager can enter the list of public holidays for that year.

## Blocks
A working day is split into blocks. The start and end times of each blocks can be configured individually for each employee, with a smallest timeframe of 15 minutes.

The following blocks are available:

* morning
* afternoon
* evening

Each block can have a type and each type can require some extra information:

* activity: link to activity?
* preparation: optional link to activity?
* administration
* individual contact: optional link to person?
* meeting
* training:
    * name
    * date (this is the date on which the timesheet is entered)
    * price
    * organization (organisator of the training)

Blocks should also be highlighted in different configurable colors depending on their type.

## Employee configuration and Templates
For each employee one or more templates can be defined. Each template will be based on a global
template as a starting point. The template defines when the employee normally works during a week
and helps to enter times.  
A template has a start date to indicate the date when that template will become the active one for
the employee.  
The configuration also defines how many hours the employee should work each week and defines the
number of allowed holidays of each type (see below).

## Validation & Acceptance
Each week the employee must fill in his timesheets. After entering the hours for the entire week.
The smallest amount of time which can be entered is 15 minutes.  The emplyee must 'sign' the
timesheet using his password, to make it final. Signing is only possible if there are at minimum the
amount of hours in the timesheet as defined in the template. After this is done the employee can't
modify the timesheet anymore.  
When a timesheet is finalized it should be sent to the coordinator for acceptance. The coordinator
should have an overview of all the timesheets which need validation. The coordinator can either
validate and accept the timesheet (by signing it with his password) or send the timesheet back to
the employee for modification.  
After the coordinator has accepted the timesheet it is sent to the HR-manager so she can review the
timesheets. The HR-manager can always modify timesheets. Note that the coordinators sends their
timesheets directly to the HR-manager.

## Holidays & Leave
The employee can plan his holidays. When planning a holiday the employee can choose the holiday
type from his individual list.
The HR-manager will configure the types of holidays (including which are limited and which unlimited
such as sickness) and will assign to each employee the correct amount of holidays at the beginning
of the year. The holiday types are of one of the following categories:

* unlimited: such as sickness
* limited: such as normal days, in hours
* ADV (recup): for each 3 months and are only 'unlocked' in the correct quarter.

The smallest amount of holiday which can be taken is 1 hour.

For public holidays: each employee can indicate on his timesheet whether he takes the holiday or he
worked that day, in which case he needs to enter the worked hours.

## Overtime
Employees can do overtime. Overtime is calculated on a weekly base. When a user works overtime the
coordincator should be notified, for example on the weekly timesheet.
Overtime can be taken back on the next 6 months, in timeframes of 15 minutes. This means that for each overtime the date should
be kept when it was performed. Calculating the amount of available overtime at any given moment can
be done as follows:

    sum(hours of overtime for today until today - 6 months) - sum(hours of used overtime for today
    until today - 6 months)
    
When an employee reaches more than 40 hours of overtime, a notification should be sent to the
coordinator and the employee. (the limit is 65 hours)

## Reporting
Each employee should have an overview of their timesheets. Each employee should
have an overview of their remaining holidays and overtime + the weeks for which
the timesheet is not filled in on their dashboard.  

The coordinator should be able to see all this information for all the
employees in his support center. The coordinator should also have an overview
on his dashboard of the timesheets which need to be validated + employees who
still need to enter their timsheet (highlighted with a colored background).  
An overview should also be available for the coordinator showing all the employees of the support center
with the date of the last validated timesheet. Employees who still need to enter their timesheets
should be colorcoded. From this list, the coordinator can open the timesheet for a specific employee
by clicking on their name.

### Individual activities report
The coordinator can see the individual activities report for each employee for a specific period.
The report will show percentages of the time spent to each type of activity, holidays and sickness.
Presentation would preferable be a pie-chart. Below the chart a synchronous list of all the
timesheets entries can be found. The coordinator can filter the list on activity type, using
a dropdown, or (if doable) by clicking on the pie-chart. This will help him to understand the time
an employee spends on specific activities and why.

### Sickness
An overview of sickness should be available for:

* individual people. (This is already partially visible in the individual activities report)
* Support Center
* The entire organization

The most flexible system would allow the end user to see some different graph, such as:

* Number of sickness days for each employee of the chosen support center for the chosen timeframe.
(this will give a visual indication which employee has the most sickness)
* Total number of sickness days for the employees of the chosen support center for each
week/month/year in the chosen timeframe. (This will give a visual presentation of when most people
are sick)
* Number of sickness days for each employee of the entire organization for the chosen timeframe.
(this will give a visual indication which employee has the most sickness of the global
organization).
* Total number of sickness days for the employees of the entire organization for each
week/month/year in the chosen timeframe.
* Total number of sickness days for the employees of each support center for the chosen timeframe
(this will make it visually clear which support center has the most sick-days)


## Questions
* Should holidays be accepted by the coordinator?
* Should the HR-manager also accept the timesheet (the coordinator already does this)


%% vim: tw=0 wrap
